# Script to convert repository to master branch
import os
import subprocess

repo_dir = (
	subprocess.Popen(["git", "rev-parse", "--show-toplevel"], stdout=subprocess.PIPE)
	.communicate()[0]
	.rstrip()
	.decode("utf-8")
)


def modifyFile(filename, *args):
	# args should be tuples or lists with 2 values
	with open(os.path.join(repo_dir, filename)) as f:
		contents = f.read()
	for arg in args:
		contents = contents.replace(arg[0], arg[1])
	with open(filename, "w") as f:
		f.write(contents)


print("README.md")
modifyFile("README.md", ("/development/", "/master/"), ('git clone https://aur.archlinux.org/iicalc-beta.git && cd iicalc-beta', 'git clone https://aur.archlinux.org/iicalc.git && cd iicalc'), ('=development', '=master'))

print("config.ini")
modifyFile("config.ini", ("branch = development", "branch = master"))

print('CONTRIBUTING.md')
modifyFile('CONTRIBUTING.md', ('/development/', '/master/'))

print(".gitlab-ci.yml")
modifyFile(
	".gitlab-ci.yml",
	("- development", "- master"),
	(" --beta", ""),
	("--release=beta", "--release=stable"),
	("iicalc-beta", "iicalc"),
	("./installer.sh --make-pkg -b -h", "./installer.sh --make-pkg -h"),
	("ref=development", "ref=master"),
	("/development/", "/master/"),
)

for file in os.listdir(".installer/configDefaults"):
	fullPath = os.path.join(".installer/configDefaults", file)
	print(fullPath)
	modifyFile(fullPath, ("branch = development", "branch = master"))

print()
print("Please double check and make sure that the calculator is fully transitioned to the master branch.")
