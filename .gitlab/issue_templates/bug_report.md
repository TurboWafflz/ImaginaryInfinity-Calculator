## Describe the bug
A clear and concise description of what the bug is.

## To Reproduce
Steps to reproduce the behavior

## Expected behavior
A clear and concise description of what you expected to happen.

## Screenshots
If applicable, add screenshots to help explain your problem.

## Desktop (please complete the following information):
<!-- Delete as appropriate. !-->
- OS: [e.g. Ubuntu]
- OS Version: [e.g. 20.04]
- Python 3 Version: [e.g. 3.8]

## Mobile (please complete the following information):
<!-- Delete as appropriate. !-->
- Device: [e.g. Galaxy S8]
- OS: [e.g. Android 9]
- Termux Version (Found in the app info): [e.g. 0.101]
- Python 3 Version: [e.g. 3.8]


## Priority/Severity:
<!-- Delete as appropriate. The priority and severity assigned may be different to this !-->
- [ ] High (anything that impacts the normal user flow or blocks app usage)
- [ ] Medium (anything that negatively affects the user experience)
- [ ] Low (anything else e.g., typos, missing icons, layout issues, etc.)


## Additional context
Add any other context about the problem here.

/title Bug report

/label ~bug