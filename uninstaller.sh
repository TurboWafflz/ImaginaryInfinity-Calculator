#!/usr/bin/env bash

args=("$@")

if [[ ! "${args[*]}" =~ "--noclear" ]]; then
	clear
fi
DIR=$(dirname "$0")
chmod +x "$DIR"/"${0##*/}"

if [[ ! "${args[*]}" =~ "--silent" ]]; then
	echo "ImaginaryInfinity Calculator Uninstaller"
	echo "If you are having a problem with the calculator, please start an issue at https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator"
	echo "Are you sure you want to uninstall ImaginaryInfinity Calculator? (y/N)"
	read -r yn
	if [ "$yn" != "y" ]
	then
		echo "Cancelled"
		exit
	fi
fi

#Uninstall for MacOS
if [ "$(uname)" == "Darwin" ]
then
	if [ "$(whoami)" != "root" ]
	then
		echo "Restarting the uninstaller as root"
		sudo "$DIR"/"${0##*/}"
		exit
	fi
	echo "The uninstaller has detected that you are using MacOS, is this correct? (Y/n)"
	read -r yn
	if [ "$yn" == "n" ]
	then
		exit
	fi
	systemPath="/usr/local/share/iicalc/"
	binPath="/usr/local/bin/"
	iconPath="/usr/local/share/iicalc/"
	desktopFilePath="/Applications/"
	desktopFile="ImaginaryInfinity Calculator.app"
	installDesktopFile="true"

#Uninstall for Android (Termux)
elif [ "$(echo "$PREFIX" | grep -o 'com.termux')" != "" ]
then
	echo "The uninstaller has detected that you are using Android, is this correct? (Y/n)"
	read -r yn
	if [ "$yn" == "n" ]
	then
		exit
	fi
	systemPath="/data/data/com.termux/files/usr/share/iicalc/"
	binPath="/data/data/com.termux/files/usr/bin/"
	iconPath="/dev/null"
	desktopFilePath="/dev/null"
	desktopFile="iicalc.desktop"
	installDesktopFile="false"
#Uninstall for Linux
elif [ "$(uname)" == "Linux" ]
then
	if [ "$(whoami)" != "root" ]
	then
		echo "Restarting the uninstaller as root"
		sudo "$DIR"/"${0##*/}"
		exit
	fi
	echo "The uninstaller has detected that you are using Linux, is this correct? (Y/n)"
	read -r yn
	if [ "$yn" == "n" ]
	then
		exit
	fi
	systemPath="/usr/share/iicalc/"
	binPath="/usr/bin/"
	iconPath="/usr/share/icons"
	desktopFilePath="/usr/share/applications"
	desktopFile="iicalc.desktop"
	installDesktopFile="true"

#Uninstall for Haiku
elif [ "$(uname)" == "Haiku" ]
then
	echo "The uninstaller has detected that you are using Haiku, is this correct? (Y/n)"
	read -r yn
	if [ "$yn" == "n" ]
	then
		exit
	fi
	systemPath="$HOME/.iicalc/system/"
	binPath="$HOME/config/non-packaged/bin"
	iconPath="$HOME/.iicalc/system"
	desktopFilePath="$HOME/config/settings/deskbar/menu/Applications"
	desktopFile="ImaginaryInfinity Calculator"
	installDesktopFile="true"

#Uninstall for NetBSD
elif [ "$(uname)" == "NetBSD" ]
then
	if [ "$(whoami)" != "root" ]
	then
		echo "Root access is required to uninstall ImaginaryInfinity Calculator."
		sudo "$DIR"/"${0##*/}"
		exit
	fi
	echo "The uninstaller has detected that you are using NetBSD, is this correct? (Y/n)"
	read -r yn
	if [ "$yn" == "n" ]
	then
		exit
	fi
	systemPath="/usr/share/iicalc/"
	binPath="/usr/bin/"
	iconPath="/usr/share/icons"
	desktopFilePath="/usr/share/applications"
	desktopFile=".installer/desktopFiles/iicalc.desktop"
	installDesktopFile="true"

else
	echo "The uninstaller does not currently support your operating system. You can install the calculator by manually specifying the required paths, however this is only recommended for experienced users."
	echo "Would you like to start manual uninstallation (y/N)?"
	read -r yn
	if [ "$yn" != "y" ]
	then
		exit
	fi
	echo "Where are plugins and themes that are installed system wide stored? (Ex. /usr/share/iicalc/) (Warning: This directory will be deleted)"
	read -r systemPath
	echo "Where are the executable files stored? (Ex. /usr/bin/)"
	read -r binPath
	echo "Where are the icons stored?"
	read -r iconPath
	installDesktopFile="false"
fi
cd "$DIR"
echo "Removing launcher..."
rm "$binPath/iicalc"
if [ $installDesktopFile == "true" ]
then
	rm -rf "${desktopFilePath:?}/${desktopFile:?}"
	echo "Removing icons..."
	rm "$iconPath/iicalc.svg"
fi
echo "Removing main Python script.."
rm "$systemPath/iicalc.py"
echo "Removing builtin plugins..."
rm -rf "$systemPath"
printf "\033[38;5;11mWarning: Removing ImaginaryInfinity Calculator does not remove the .iicalc folder in your home directory. If you want to run the portable version of ImaginaryInfinity Calculator again, you will have to delete it.\033[m\n"
