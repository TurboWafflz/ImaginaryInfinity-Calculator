#!/usr/bin/env bash

# exit immediately on errors
set -e
ROOT=".package-build-dir/"
shopt -s expand_aliases

function generate_package_structure () {
	# function to generate package structure of installed files

	printf "::\033[1;34mPreparing the directory structure...\033[0m\n"
	rm -rf "${ROOT}"
	mkdir "${ROOT}"

	BINARY_FOLDER="${ROOT}${BINARY_FOLDER}"
	APPLICATIONS_FOLDER="${ROOT}${APPLICATIONS_FOLDER}"
	ICONS_FOLDER="${ROOT}${ICONS_FOLDER}"
	SYSTEM_FOLDER="${ROOT}${SYSTEM_FOLDER}"

	# binary location
	mkdir -p "$BINARY_FOLDER"

	# desktop file
	if [[ "$INSTALL_DESKTOP_FILE" == "true" ]]; then
		mkdir -p "$APPLICATIONS_FOLDER"
	fi

	# icon
	if [[ "$INSTALL_ICON" == "true" ]]; then
		mkdir -p "$ICONS_FOLDER"
	fi

	# iicalc system folders
	mkdir -p "$SYSTEM_FOLDER"
	mkdir -p "$SYSTEM_FOLDER/systemPlugins"
	mkdir -p "$SYSTEM_FOLDER/themes"
	mkdir -p "$SYSTEM_FOLDER/templates"
	mkdir -p "$SYSTEM_FOLDER/docs"
	mkdir -p "$SYSTEM_FOLDER/locale"


	# install files
	cp "$LAUNCHER_FILE" "$BINARY_FOLDER/iicalc"

	# use find instead of cp -r in order to supress directory ommited error
	# shellcheck disable=SC2033
	find system/systemPlugins -maxdepth 1 -type f -exec cp {} "$SYSTEM_FOLDER/systemPlugins"/ \;
	cp system/themes/* "$SYSTEM_FOLDER/themes"
	cp -r templates "$SYSTEM_FOLDER"
	cp system/docs/* "$SYSTEM_FOLDER/docs"
	cp -r system/locale/* "$SYSTEM_FOLDER/locale"

	cp README.md "$SYSTEM_FOLDER/docs/iicalc.md"
	cp main.py "$SYSTEM_FOLDER/iicalc.py"
	cp requirements.txt "$SYSTEM_FOLDER"
	cp messages.txt "$SYSTEM_FOLDER"
	cp system/version.txt "$SYSTEM_FOLDER"
	cp README.md "$SYSTEM_FOLDER"
	cp "$CONFIG_FILE" "$SYSTEM_FOLDER/config.ini"

	if [[ "$INSTALL_DESKTOP_FILE" == "true" ]]; then
		if [[ "$APPLICATIONS_FOLDER_IS_FULL_PATH" == "true" ]]; then
			cp -a "$DESKTOP_FILE/./" "$APPLICATIONS_FOLDER/$DESKTOP_FILE_FINAL_NAME"
		else
			cp -a "$DESKTOP_FILE" "$APPLICATIONS_FOLDER/$DESKTOP_FILE_FINAL_NAME"
		fi
	fi
	if [[ "$INSTALL_ICON" == "true" ]]; then
		cp "$ICON_FILE" "$ICONS_FOLDER/$ICON_FILE_FINAL_NAME"
	fi

	# shellcheck disable=SC2033
	find "${ROOT}" -type f -exec chmod ug+r {} \;
	chmod +x "$BINARY_FOLDER/iicalc"

}

function detect_install_platform () {

	# Detect platform that user is on and return it in human-readable form

	if [[ "$(echo "$PREFIX" | grep -o 'com.termux')" != "" ]]; then
		echo "Android"
		return 0
	fi

	case "$(uname)" in
		Linux|Haiku|NetBSD|OpenBSD|FreeBSD) uname;;
		Darwin)                             echo "macOS";;
		*)                                  echo "Custom";;
	esac
}

function linux_install () {
	# directory paths
	BINARY_FOLDER="usr/bin"
	APPLICATIONS_FOLDER="usr/share/applications"
	ICONS_FOLDER="usr/share/icons/hicolor/scalable/apps"
	SYSTEM_FOLDER="usr/share/iicalc"

	# choose files to be installed
	LAUNCHER_FILE=".installer/launchers/unix.sh"
	CONFIG_FILE=".installer/configDefaults/unix.ini"
	DESKTOP_FILE=".installer/desktopFiles/iicalc.desktop"
	ICON_FILE="iicalc.svg"

	# flags
	INSTALL_DESKTOP_FILE="true"
	INSTALL_ICON="true"
	generate_package_structure
}

function macos_install () {
	# directory paths
	BINARY_FOLDER="usr/local/bin"
	APPLICATIONS_FOLDER="Applications/ImaginaryInfinity Calculator.app"
	SYSTEM_FOLDER="usr/local/share/iicalc"

	# choose files to be installed
	LAUNCHER_FILE=".installer/launchers/macos.sh"
	CONFIG_FILE=".installer/configDefaults/macos.ini"
	DESKTOP_FILE=".installer/desktopFiles/iicalc.app"

	# flags
	INSTALL_DESKTOP_FILE="true"
	APPLICATIONS_FOLDER_IS_FULL_PATH="true"
	INSTALL_ICON="false"

	# generate macOS icon
	iconSource=iicalc.png
	iconDestination="${DESKTOP_FILE}"
	icon=/tmp/$(basename "$iconSource")
	rsrc=/tmp/icon.rsrc

	# Create icon from the iconSource
	cp "$iconSource" "$icon"

	# Add icon to image file, meaning use itself as the icon
	sips -i "$icon"

	# Take that icon and put it into a rsrc file
	DeRez -only icns "$icon" > "$rsrc"

	# Apply the rsrc file to
	SetFile -a C "$iconDestination"

	touch "$iconDestination"/$'Icon\r'
	Rez -append "$rsrc" -o "$iconDestination"/Icon?
	SetFile -a V "$iconDestination"/Icon?

	rm "$rsrc" "$icon"

	generate_package_structure

	chmod -R 755 "${APPLICATIONS_FOLDER}"
}


function android_install () {
	BINARY_FOLDER="/data/data/com.termux/files/usr/bin/"
	SYSTEM_FOLDER="/data/data/com.termux/files/usr/share/iicalc"

	CONFIG_FILE=".installer/configDefaults/android.ini"
	LAUNCHER_FILE=".installer/launchers/android.sh"

	INSTALL_DESKTOP_FILE="false"
	INSTALL_ICON="false"

	generate_package_structure
}

function free_openbsd_install () {
	BINARY_FOLDER="/usr/bin/"
	APPLICATIONS_FOLDER="/usr/share/applications"
	ICONS_FOLDER="/usr/share/icons"
	SYSTEM_FOLDER="/usr/share/iicalc/"

	CONFIG_FILE=".installer/configDefaults/unix.ini"
	LAUNCHER_FILE=".installer/launchers/unix.sh"
	DESKTOP_FILE=".installer/desktopFiles/iicalc.desktop"
	ICON_FILE="iicalc.svg"

	INSTALL_DESKTOP_FILE="true"
	INSTALL_ICON="true"

	generate_package_structure
}

function netbsd_install () {
	SYSTEM_FOLDER="/usr/share/iicalc/"
	BINARY_FOLDER="/usr/bin/"
	ICONS_FOLDER="/usr/share/icons"
	APPLICATIONS_FOLDER="/usr/share/applications"

	CONFIG_FILE=".installer/configDefaults/unix.ini"
	LAUNCHER_FILE=".installer/launchers/netbsd.sh"
	DESKTOP_FILE=".installer/desktopFiles/iicalc.desktop"
	ICON_FILE="iicalc.svg"

	INSTALL_DESKTOP_FILE="true"
	INSTALL_ICON="true"

	generate_package_structure
}

function haiku_install () {
	prevPath=$(pwd)
	cd .installer/launchers/
	unzip -o haiku.sh.zip
	cd ../desktopFiles/haiku
	unzip -o "ImaginaryInfinity Calculator.zip"
	chmod +x "ImaginaryInfinity Calculator"
	cd "$prevPath"

	BINARY_FOLDER="$HOME/config/non-packaged/bin"
	SYSTEM_FOLDER="$HOME/.iicalc/system/"
	CONFIG_FILE=".installer/configDefaults/haiku.ini"
	LAUNCHER_FILE=".installer/launchers/haiku.sh"
	ICONS_FOLDER="$HOME/.iicalc/system/"
	APPLICATIONS_FOLDER="$HOME/config/settings/deskbar/menu/Applications/"
	DESKTOP_FILE=".installer/desktopFiles/haiku/ImaginaryInfinity Calculator"
	ICON_FILE="iicalc.svg"

	INSTALL_DESKTOP_FILE="true"
	INSTALL_ICON="true"

	generate_package_structure
}

function custom_install () {
	echo "Where should plugins and themes that are installed system wide be stored? (Ex. /usr/share/iicalc/)"
	printf "\033[1;34m:: \033[0m"
	read -r SYSTEM_FOLDER
	echo "Where should executable files be stored? (Ex. /usr/bin/)"
	printf "\033[1;34m:: \033[0m"
	read -r BINARY_FOLDER

	INSTALL_DESKTOP_FILE="false"
	INSTALL_ICON="false"
	cp .installer/launchers/unix.sh .installer/launchers/custom.sh
	LAUNCHER_FILE=".installer/launchers/custom.sh"
	sed -i "s'systemPath=\"/usr/share/iicalc/\"'systemPath=$SYSTEM_FOLDER'" $LAUNCHER_FILE
	CONFIG_FILE=".installer/configDefaults/unix.ini"

	generate_package_structure
}

function install_system_requirements () {
	# function to install OS requirements (dialog, python, etc)
	case "${INSTALLATION_PLATFORM}" in
		Android)
			pkg install dialog python;;
		FreeBSD|OpenBSD)
			printf "::\033[1;34mInstalling required packages from pkgsrc...\033[0m\n"

			if [[ "$(uname)" == "FreeBSD" ]]; then
				pkg install python3
			else
				pkg_add -r python
			fi

			if ! pip3 > /dev/null;
			then
				printf "::\033[1;34mInstalling pip...\033[0m\n"
				if [[ "$(uname)" == "FreeBSD" ]]; then
					pkg install curl
				else
					pkg_add -r curl
				fi
				curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
				python3 get-pip.py
				rm get-pip.py
			fi
			;;
		NetBSD)
			printf "::\033[1;34mInstalling required packages from pkgsrc...\033[0m\n"
			pkg_add python38
			pkg_add py38-expat
			pkg_add readline
			pkg_add py38-readline
			printf "::\033[1;34mInstalling pip...\033[0m\n"
			python3.8 -m ensurepip
			;;
		Haiku)
			if [[ "$DEFAULT_PROMPT_MODE" == "true" ]]; then
				yn="y"
			else
				printf "\033[1;34m::\033[0mWould you like to automatially install Dialog from HaikuDepot? [Y/n] "
				read -r yn
			fi

			if [ ! "$yn" == "n" ]
			then
				printf "::\033[1;34mRefreshing repositories...\033[0m\n"
				pkgman refresh
				# printf "::\033[1;34mInstalling Python...\033[0m\n"
				# echo "y" | pkgman install python39_x86
				# printf "::\033[1;34mInstalling pip...\033[0m\n"
				# echo "y" | pkgman install pip_python39
				printf "::\033[1;34mInstalling dialog...\033[0m\n"
				echo "y" | pkgman install dialog
			fi
			;;
	esac
}

function test_requirements () {
	# function to test for executable and PyPI requirements
	PYTHON_VERSIONS=(python3 python3.9 python3.8 python3.7 python3.6)
	for python in "${PYTHON_VERSIONS[@]}"; do
		if command -v "$python" &> /dev/null; then
			PYTHON_EXECUTABLE=${python}
			break
		fi

		if [[ "${python}" == "${PYTHON_VERSIONS[-1]}" ]]; then
			echo "Python 3 not found. You must install Python 3 before attempting to install the calculator."
			echo "On Debian based operating systems (Ubuntu, Raspbian, Debian, etc.) run: sudo apt install python3"
			echo "On Red Hat based operating systems (Fedora, CentOS, Red Hat Enterprise Linux, etc.) run: sudo dnf install python3"
			echo "On Alpine based operating systems (PostmarketOS, Alpine Linux, etc.) run: sudo apk add python3"
			echo "On Arch based operating systems (Arch Linux, Manjaro, TheShellOS) run: sudo pacman -S python"
			echo "On MacOS, download the latest Python installer from https://www.python.org/downloads/mac-osx/"
			echo "On Android based operating systems (In Termux) run: pkg install python"
			exit 1
		fi
	done

	# test for pip
	if ! "$PYTHON_EXECUTABLE" -m pip --version &> /dev/null; then
		echo ""
		printf "\033[0;31mPip does not seem to be installed. Before running the calculator, please install pip.\033[0m\n"
		echo "On Debian based operating systems (Ubuntu, Raspbian, Debian, etc.) run: sudo apt install python3-pip"
		echo "On Red Hat based operating systems (Fedora, CentOS, Red Hat Enterprise Linux, etc.) run: sudo dnf install python3-pip"
		echo "On Alpine based operating systems (PostmarketOS, Alpine Linux, etc.) run: sudo apk add py3-pip"
		echo "On Arch based operating systems (Arch Linux, Manjaro, TheShellOS) run: sudo pacman -Syu python-pip"
		printf "On MacOS, download the get-pip.py installer: \033[33mcurl https://bootstrap.pypa.io/get-pip.py -o get-pip.py\033[0m and then run: \033[33mpython3 get-pip.py\033[0m\n"
	else
		printf "::\033[1;34mInstalling Python modules...\033[0m\n"
		"$PYTHON_EXECUTABLE" -m pip install -r requirements.txt
	fi

}

function install_files () {
	# install the calculator files into the filesystem
	printf "::\033[1;34mInstalling files...\033[0m\n"
	cp -a "${ROOT}/." /
	printf "::\033[1;34mRemoving old package...\033[0m\n"
	rm -rf "${ROOT}"
}

function base_installation () {

	# base installation for the calculator. scripts arguments should be passed to this function.

	# restart as root since we're installing to user read-only folders
	if [ "$EUID" != 0 ]; then
		if command -v sudo &> /dev/null || command -v doas &> /dev/null; then
			echo "Root access is required to install ImaginaryInfinity Calculator. Restarting with elevated privileges."
			echo ""
			$(command -v sudo || command -v doas) "$0" "$@"
			exit $?
		fi
	fi

	# auto-detect system and install the calculator
	printf "\033[4;33mImaginaryInfinity Calculator Installer\033[0m\n\n"

	SUPPORTED_SYSTEMS=(
		"Linux"
		"macOS"
		"Android"
		"OpenBSD"
		"NetBSD"
		"FreeBSD"
		"Haiku"
		"Custom"
	)

	INSTALLATION_PLATFORM=$(detect_install_platform)

	# check if auto-detection was correct
	printf "Your installation platform has been automatically detected as \033[0;32m%s\033[0m.\n" "$INSTALLATION_PLATFORM"

	if [[ "$DEFAULT_PROMPT_MODE" == "true" ]]; then
		REPLY="n"
	else
		printf "\033[1;34m::\033[0m Is this correct? [Y/n] "
		read -p "" -r
	fi

	echo ""

	# check user response
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		# user would like to change installation system; prompt for change
		select opt in "${SUPPORTED_SYSTEMS[@]}"; do
			# shellcheck disable=SC2076
			if [[ " ${SUPPORTED_SYSTEMS[*]} " =~ " ${opt} " ]]; then
				INSTALLATION_PLATFORM=$opt
				break
			else
				echo "Invalid selection: ${REPLY}"
			fi
		done
	fi

	# install calculator
	case "${INSTALLATION_PLATFORM}" in
		Linux)
			linux_install;;
		macOS)
			macos_install;;
		Android)
			android_install;;
		FreeBSD|OpenBSD)
			free_openbsd_install;;
		NetBSD)
			netbsd_install;;
		Haiku)
			haiku_install;;
		Custom)
			custom_install;;
	esac

	if [[ "$DUMMY_BUILD" != "true" ]]; then

		if [[ "$MINIMAL" != "true" ]]; then
			echo ""

			test_requirements
		fi

		if [[ "$NO_SYS_REQS" != "true" ]]; then
			echo ""

			install_system_requirements
		fi

		echo ""

		install_files
	fi
}

function build_deb_package () {

	if ! command -v dpkg &>/dev/null; then
		echo "dpkg is not installed. Please install the package that provides dpkg for your operating system."
		exit 1
	fi

	SYSTEM_FOLDER="usr/share/iicalc/"
	BINARY_FOLDER="usr/bin"
	APPLICATIONS_FOLDER="usr/share/applications"
	ICONS_FOLDER="usr/share/icons/hicolor/scalable/apps"

	CONFIG_FILE=".installer/configDefaults/deb.ini"
	LAUNCHER_FILE=".installer/launchers/unix.sh"
	DESKTOP_FILE=".installer/desktopFiles/iicalc.desktop"
	ICON_FILE="iicalc.svg"

	INSTALL_DESKTOP_FILE="true"
	INSTALL_ICON="true"

	generate_package_structure

	mkdir -p "${ROOT}/DEBIAN"
	cp .installer/build/deb/* "${ROOT}/DEBIAN"
	chmod +x "${ROOT}/DEBIAN/postinst"
	chmod +x "${ROOT}/DEBIAN/prerm"

	cd "${ROOT}"
	find . -type f ! -regex '.*.hg.*' ! -regex '.*?debian-binary.*' ! -regex '.*?DEBIAN.*' -printf '%P ' | xargs md5sum > "DEBIAN/md5sums"
	cd ..
	#Calculate Size
	sed -i "s'Installed-Size: 0'Installed-Size: $(du -s "${ROOT}" | awk '{print $1}')'" "${ROOT}/DEBIAN/control"
	sed -i "s'Version: 0'Version: $(cat system/version.txt)'" "${ROOT}/DEBIAN/control"
	#Build DEB
	dpkg -b ${ROOT} iicalc.deb

}

build_ppa_package () {
	if [[ "$BETA" == "true" ]]; then
		pkg_name="iicalc-beta"
	else
		pkg_name="iicalc"
	fi

	pkg_ver="$(cat system/version.txt)-0ubuntu1~bionicppa1"
	ROOT="${pkg_name}-${pkg_ver}/"

	SYSTEM_FOLDER="usr/share/iicalc/"
	BINARY_FOLDER="usr/bin"
	APPLICATIONS_FOLDER="usr/share/applications"
	ICONS_FOLDER="usr/share/icons/hicolor/scalable/apps"

	CONFIG_FILE=".installer/configDefaults/ppa.ini"
	LAUNCHER_FILE=".installer/launchers/unix.sh"
	DESKTOP_FILE=".installer/desktopFiles/iicalc.desktop"
	ICON_FILE="iicalc.svg"

	INSTALL_DESKTOP_FILE="true"
	INSTALL_ICON="true"

	generate_package_structure

	mkdir -p "${ROOT}/debian"
	cp -r .installer/build/ppa/* "${ROOT}/debian/"
	cp "README.md" "${ROOT}/debian/README"
	chmod +x "${ROOT}/debian/postinst"
	chmod +x "${ROOT}/debian/prerm"
	chmod +x "${ROOT}/debian/rules"

	cd "${ROOT}"
	echo "${pkg_name} (${pkg_ver}) bionic; urgency=medium

  * $(git log -1 --pretty=%B | xargs)

 -- ImaginaryInfinity <tabulatejarl8@gmail.com>  $(date +'%a, %d %b %Y %H:%M:%S -0400' -d @"$(git log -1 --pretty=format:'%ct' | xargs)")

$(cat debian/changelog)" > debian/changelog

	if [[ "$BETA" == "true" ]]; then
		sed -i 's|Source:.*|Source: https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator/-/archive/development/ImaginaryInfinity-Calculator-development.zip|' debian/copyright

		sed -i 's|Source:.*|Source: iicalc-beta|' debian/control
		sed -i 's|Package:.*|Package: iicalc-beta|' debian/control
		sed -i 's|Conflicts:.*|Conflicts: iicalc|' debian/control

		mv debian/iicalc.install debian/iicalc-beta.install
	fi

}

function build_snap_package () {

	if ! command -v snapcraft &>/dev/null; then
		echo "snapcraft is not installed. Please install it by running \`\`snap install snapcraft --classic\`\`."
		exit 1
	fi

	SYSTEM_FOLDER="src/usr/share/iicalc/"
	BINARY_FOLDER="src/usr/bin"
	ICONS_FOLDER="snap/gui/"
	APPLICATIONS_FOLDER="snap/gui/iicalc.desktop"

	CONFIG_FILE=".installer/configDefaults/snap.ini"
	LAUNCHER_FILE=".installer/launchers/snap.sh"
	DESKTOP_FILE=".installer/desktopFiles/iicalc-snap.desktop"
	ICON_FILE="iicalc.svg"

	INSTALL_DESKTOP_FILE="true"
	INSTALL_ICON="true"

	generate_package_structure

	cp ".installer/build/snap/snapcraft.yaml" "${ROOT}/snap/snapcraft.yaml"
	cd "${ROOT}"

	# Version
	sed -i "s/{{vernum}}/$(cat ../system/version.txt)/g" snap/snapcraft.yaml

	# Grade
	if [ "$BETA" == "true" ]; then
		sed -i "s/{{grade}}/devel/g" snap/snapcraft.yaml
	else
		sed -i "s/{{grade}}/stable/g" snap/snapcraft.yaml
	fi

	snapcraft
}

function build_appimage_package () {
	SYSTEM_FOLDER="usr/share/iicalc/"
	BINARY_FOLDER="usr/bin"
	ICONS_FOLDER=""
	APPLICATIONS_FOLDER=""

	CONFIG_FILE=".installer/configDefaults/appImage.ini"
	LAUNCHER_FILE=".installer/launchers/appImage.sh"
	DESKTOP_FILE=".installer/desktopFiles/iicalc-appImage.desktop"
	ICON_FILE="iicalc.svg"

	INSTALL_DESKTOP_FILE="true"
	INSTALL_ICON="true"

	generate_package_structure

	cp ".installer/build/appImage/AppRun" "${ROOT}"
	chmod +x "${ROOT}/AppRun"

	if [ -f appimagetool-x86_64.AppImage ]; then
		echo "Found appimagetool"
	else
		wget https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage
	fi
	chmod +x ./appimagetool-x86_64.AppImage
	./appimagetool-x86_64.AppImage --appimage-extract
	mv squashfs-root appimagetool
	ARCH=x86_64 ./appimagetool/AppRun ${ROOT}

}

function build_aur_package () {
	cp -f .installer/build/arch/buildscript.sh ./
	bash buildscript.sh "$@"

	rm buildscript.sh
}

function build_rpm_package () {

	if ! command -v rpmbuild; then
		echo "rpmbuild command is not installed. Please install rpmbuild"
		exit 1
	fi

	realdir="$(cd "$(dirname "$DIR")"; pwd)"

	cp -f .installer/build/rhel/iicalc.spec ./
	sed -i "s:{{maindir}}:$realdir:g" iicalc.spec
	version=$(cat system/version.txt)
	versionarr=("${version//-/ }")
	if [ "${#versionarr[@]}" -eq "1" ]; then
	  versionarr+=('1')
	fi

	sed -i "s/{{pkgver}}/${versionarr[0]}/g" iicalc.spec
	sed -i "s/{{pkgrel}}/${versionarr[1]}/g" iicalc.spec

	rpmpath=$(rpmbuild --target noarch -bb iicalc.spec | awk -F': ' '/Wrote: /{print $2}')

	mv "$rpmpath" ./iicalc.rpm
}

function build_flatpak_package () {
	SYSTEM_FOLDER="app/share/iicalc/"
	BINARY_FOLDER="app/bin"
	ICONS_FOLDER="app/share/icons/hicolor/scalable/apps/"
	APPLICATIONS_FOLDER="app/share/applications/"

	CONFIG_FILE=".installer/configDefaults/unix.ini"
	LAUNCHER_FILE=".installer/launchers/unix.sh"
	DESKTOP_FILE=".installer/desktopFiles/iicalc.desktop"
	ICON_FILE="iicalc.png"
	DESKTOP_FILE_FINAL_NAME="org.imaginaryinfinity.Calculator.desktop"
	ICON_FILE_FINAL_NAME="org.imaginaryinfinity.Calculator.png"

	INSTALL_DESKTOP_FILE="true"
	INSTALL_ICON="true"

	generate_package_structure
}

function build_clean () {
	rm -rf iicalc.spec iicalc.deb iicalc-any.pkg.tar.zst *.AppImage
	rm -rf iicalc-arch .package-build-dir iicalc-*ubuntu1*ppa1 *.tar.xz *.dsc
	rm -rf *.build* *.changes *.upload *.snap appimagetool
}

args=("$@")
# main code; run when script is run
while test $# -gt 0; do
	case "$1" in
		-h|--help)
			echo "$0 - Installer and package builder for ImaginaryInfinity Calculator"
			echo ""
			echo "$0 [options]"
			echo ""
			echo "options:"
			echo "    -h, --help                show this message and exit"
			echo "    -v, --verbose             give more output"
			echo "    -d, --default             all applicable prompts will automatically select the default option"
			echo "    -m, --minimal             minimal installation; don't attempt to install requirements"
			echo "    --no-sys-reqs             do not attempt to install system requirements (dialog, python, etc) with you package manager"
			echo "    --dummy-build             build installation directory, but don't install it (for testing)"
			echo ""
			echo "    --build TYPE              build a packaged version of iiCalc. Build \"help\" to get a list of valid options"
			echo "    -b, --beta                To be used alongside --build. Changes package metadata to be for the beta version of iiCalc"
			echo "    --install                 install .package-build-dir contents after building package"
			exit 0
			;;
		-v|--verbose)
			alias mkdir='mkdir -v'
			alias rm='rm -v'
			# shellcheck disable=SC2032
			alias cp='cp -v'
			# shellcheck disable=SC2032
			alias chmod='chmod -v'
			shift
			;;
		-d|--default)
			DEFAULT_PROMPT_MODE="true"
			shift
			;;
		-m|--minimal)
			MINIMAL="true"
			shift
			;;
		--build)
			shift
			BUILD_TARGET="$1"
			shift
			;;
		-b|--beta)
			BETA="true"
			shift
			;;
		--no-sys-reqs)
			NO_SYS_REQS="true"
			shift
			;;
		--dummy-build)
			DUMMY_BUILD="true"
			shift
			;;
		--install)
			INSTALL_AFTER_BUILD="true"
			shift
			;;
		*)
			echo "Unknown option: \"$1\". Run with --help to get a list of valid flags"
			exit 1
	esac
done

if [[ "$BUILD_TARGET" != "" ]]; then
	# to lowercase
	BUILD_TARGET=$(echo "$BUILD_TARGET" | tr '[:upper:]' '[:lower:]')
	case "$BUILD_TARGET" in
		help)
			# build help
			echo "$0 - Installer and package builder for ImaginaryInfinity Calculator"
			echo ""
			echo "$0 --build [TYPE]"
			echo ""
			echo "types:"
			echo "    help                show this message and exit"
			echo "    deb                 build debian (.deb) package"
			echo "    ppa                 create files for upload to PPA"
			echo "    snap                build snap package"
			echo "    appimage            build AppImage package"
			echo "    aur                 create AUR files and build package (pkg.tar.zst)"
			echo "    rpm                 build RPM package"
			echo "    flatpak             build flatpak package"
			echo "    clean               clean build files"
			exit 0
			;;
		deb)
			build_deb_package
			;;
		ppa)
			build_ppa_package
			;;
		snap)
			build_snap_package
			;;
		appimage)
			build_appimage_package
			;;
		aur)
			build_aur_package
			;;
		rpm)
			build_rpm_package
			;;
		flatpak)
			build_flatpak_package
			;;
		clean)
			build_clean
			;;
		*)
			echo "Invalid package format: \"$BUILD_TARGET\". Build \"help\" to get a list of valid formats"
			exit 1
			;;
	esac

	if [[ "$INSTALL_AFTER_BUILD" == "true" ]]; then
		install_files
	fi
else
	# generate_package_structure
	base_installation "${args[@]}"
fi
