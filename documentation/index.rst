Welcome to ImaginaryInfinity Calculator's documentation!
========================================================

Contents:

.. toctree::
   :maxdepth: 2

   main.md
   makingStandardPlugin.md
   addSettings.md
   guidelines.md


* :ref:`search`
