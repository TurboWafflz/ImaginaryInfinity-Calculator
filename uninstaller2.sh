#!/usr/bin/env bash

set -e
shopt -s expand_aliases

function delete_files () {
	printf "::\033[1;34mUninstalling...\033[0m\n"

	rm -rf "$SYSTEM_FOLDER"
	rm -f "$BINARY_PATH"

	if [[ "$UNINSTALL_ICON_FILE" == "true" ]]; then
		rm -f "$ICON_PATH"
	fi

	if [[ "$UNINSTALL_DESKTOP_FILE" == "true" ]]; then
		rm -rf "$DESKTOP_FILE_PATH"
	fi

	printf "\033[0;32mDone\033[0m\n"

}

function detect_install_platform () {

	# Detect platform that user is on and return it in human-readable form

	if [[ "$(echo "$PREFIX" | grep -o 'com.termux')" != "" ]]; then
		echo "Android"
		return 0
	fi

	case "$(uname)" in
		Linux|Haiku|NetBSD|OpenBSD|FreeBSD) uname;;
		Darwin)                             echo "macOS";;
		*)                                  echo "Custom";;
	esac
}

function linux_uninstall () {
	SYSTEM_FOLDER="/usr/share/iicalc/"
	BINARY_PATH="/usr/bin/iicalc"
	ICON_PATH="/usr/share/icons/hicolor/scalable/iicalc.svg"
	DESKTOP_FILE_PATH="/usr/share/applications/iicalc.desktop"

	UNINSTALL_DESKTOP_FILE="true"
	UNINSTALL_ICON_FILE="true"

	delete_files
}

function macos_uninstall () {
	SYSTEM_FOLDER="/usr/local/share/iicalc"
	BINARY_PATH="/usr/local/bin/iicalc"
	DESKTOP_FILE_PATH="/Applications/ImaginaryInfinity Calculator.app"

	UNINSTALL_DESKTOP_FILE="true"
	UNINSTALL_ICON_FILE="false"

	delete_files
}

function android_uninstall () {
	SYSTEM_FOLDER="/data/data/com.termux/files/usr/share/iicalc/"
	BINARY_PATH="/data/data/com.termux/files/usr/bin/iicalc"

	UNINSTALL_ICON_FILE="false"
	UNINSTALL_DESKTOP_FILE="false"

	delete_files
}

function haiku_uninstall () {
	SYSTEM_FOLDER="$HOME/.iicalc/system/"
	BINARY_PATH="$HOME/config/non-packaged/bin/iicalc"
	ICON_PATH="$HOME/.iicalc/system/iicalc.svg"
	DESKTOP_FILE_PATH="$HOME/config/settings/deskbar/menu/Applications/ImaginaryInfinity Calculator"

	UNINSTALL_ICON_FILE="true"
	UNINSTALL_DESKTOP_FILE="true"

	delete_files
}

function custom_uninstall () {
	echo "The uninstaller does not currently support your operating system. You can uninstall the calculator by manually specifying the required paths, however this is only recommended for experienced users."
	printf "Would you like to start manual uninstallation? (y/N) "
	read -r yn
	if [ "$yn" != "y" ]
	then
		exit 0
	fi
	echo "Where are plugins and themes that are installed system wide stored? (Ex. /usr/share/iicalc/) (Warning: This directory will be deleted)"
	printf "\033[1;34m:: \033[0m"
	read -r SYSTEM_FOLDER
	echo "Where are the executable files stored? (Ex. /usr/bin/)"
	printf "\033[1;34m:: \033[0m"
	read -r BINARY_PATH
	BINARY_PATH="${BINARY_PATH}/iicalc"

	UNINSTALL_ICON_FILE="false"
	UNINSTALL_DESKTOP_FILE="false"

	delete_files
}

function uninstall () {
	# uninstaller for the calculator. scripts arguments should be passed to this function.

	# restart as root since we're installing to user read-only folders
	if [ "$EUID" != 0 ]; then
		if command -v sudo &> /dev/null || command -v doas &> /dev/null; then
			echo "Root access is required to uninstall ImaginaryInfinity Calculator. Restarting with elevated privileges."
			echo ""
			$(command -v sudo || command -v doas) "$0" "$@"
			exit $?
		fi
	fi

	# auto-detect system and install the calculator
	printf "\033[4;33mImaginaryInfinity Calculator Uninstaller\033[0m\n\n"
	printf "If you are having a problem with the calculator, please start an issue at \033[4;34mhttps://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator\033[0m\n"

	if [[ "$DEFAULT_PROMPT_MODE" != "true" ]]; then
		printf "\033[1;34m::\033[0m Are you sure you want to uninstall ImaginaryInfinity Calculator? [y/N] "
		read -r yn
		if [ "$yn" != "y" ]
		then
			echo "Cancelled"
			exit 0
		fi
	fi

	echo ""

	local SUPPORTED_SYSTEMS=(
		"Linux"
		"macOS"
		"Android"
		"OpenBSD"
		"NetBSD"
		"FreeBSD"
		"Haiku"
		"Custom"
	)

	local INSTALLATION_PLATFORM
	INSTALLATION_PLATFORM=$(detect_install_platform)

	# check if auto-detection was correct
	printf "Your installation platform has been automatically detected as \033[0;32m%s\033[0m.\n" "$INSTALLATION_PLATFORM"
	if [[ "$DEFAULT_PROMPT_MODE" == "true" ]]; then
		REPLY="n"
	else
		printf "\033[1;34m::\033[0m Is this correct? [Y/n] "
		read -p "" -r
	fi

	echo ""

	# check user response
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		# user would like to change installation system; prompt for change
		select opt in "${SUPPORTED_SYSTEMS[@]}"; do
			# shellcheck disable=SC2076
			if [[ " ${SUPPORTED_SYSTEMS[*]} " =~ " ${opt} " ]]; then
				INSTALLATION_PLATFORM=$opt
				break
			else
				echo "Invalid selection: ${REPLY}"
			fi
		done
	fi

	# install calculator
	case "${INSTALLATION_PLATFORM}" in
		Linux|FreeBSD|OpenBSD|NetBSD)
			linux_uninstall;;
		macOS)
			macos_uninstall;;
		Android)
			android_uninstall;;
		Haiku)
			haiku_uninstall;;
		Custom)
			custom_uninstall;;
	esac

	printf "\n\033[38;5;11mWarning: Removing ImaginaryInfinity Calculator does not remove the .iicalc folder in your home directory. If you want to run the portable version of ImaginaryInfinity Calculator again, you will have to delete it.\033[m\n"
}

while test $# -gt 0; do
	case "$1" in
		-h|--help)
			echo "$0 - Installer and package builder for ImaginaryInfinity Calculator"
			echo ""
			echo "$0 [options]"
			echo ""
			echo "options:"
			echo "    -h, --help                show this message and exit"
			echo "    -v, --verbose             give more output"
			echo "    -d, --default             all applicable prompts will automatically select the default option"
			exit 0
			;;
		-v|--verbose)
			alias rm='rm -v'
			shift
			;;
		-d|--default)
			DEFAULT_PROMPT_MODE="true"
			shift
			;;
	esac
done

uninstall "$@"