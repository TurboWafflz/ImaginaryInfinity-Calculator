#!/usr/bin/env bash

args=("$@")

if [[ ! "${args[*]}" =~ "--noclear" ]]; then
	clear
fi
echo "ImaginaryInfinity Calculator Installer"
DIR=$(dirname "$0")
chmod +x "$DIR"/"${0##*/}"

#Build deb
if [ "$1" == "--make-deb" ]
then
	rm -rf "iicalc-deb"
	mkdir -p "iicalc-deb/DEBIAN"
	mkdir -p "iicalc-deb/usr/bin"
	mkdir -p "iicalc-deb/usr/share"
	mkdir -p "iicalc-deb/usr/share/applications"
	mkdir -p "iicalc-deb/usr/share/icons"
	cp ".installer/build/deb/control" "iicalc-deb/DEBIAN"
	cp ".installer/build/deb/postinst" "iicalc-deb/DEBIAN"
	cp ".installer/build/deb/prerm" "iicalc-deb/DEBIAN"
	cp ".installer/build/deb/copyright" "iicalc-deb/DEBIAN"
	chmod +x "iicalc-deb/DEBIAN/postinst"
	chmod +x "iicalc-deb/DEBIAN/prerm"
	systemPath="iicalc-deb/usr/share/iicalc/"
	binPath="iicalc-deb/usr/bin"
	config=".installer/configDefaults/deb.ini"
	launcher=".installer/launchers/unix.sh"
	iconPath="iicalc-deb/usr/share/icons"
	desktopFilePath="iicalc-deb/usr/share/applications"
	desktopFile=".installer/desktopFiles/iicalc.desktop"
	installDesktopFile="true"
	buildOnly="true"

elif [[ "${args[*]}" =~ "--make-ppa-files" ]]
then
	if [ "$2" == "--beta" ] || [ "$2" == "-b" ]; then
		pkg_name="iicalc-beta"
		beta="true"
	else
		pkg_name="iicalc"
		beta="false"
	fi

	pkg_ver="$(cat system/version.txt)-0ubuntu1~bionicppa1"
	pkg_dir="${pkg_name}-${pkg_ver}"

	rm -rf "${pkg_name}-$(cat system/version.txt)*"
	mkdir -p "${pkg_dir}/debian"
	mkdir -p "${pkg_dir}/usr/bin"
	mkdir -p "${pkg_dir}/usr/share"
	mkdir -p "${pkg_dir}/usr/share/applications"
	mkdir -p "${pkg_dir}/usr/share/icons"
	cp -r .installer/build/ppa/* "${pkg_dir}/debian/"
	cp "README.md" "${pkg_dir}/debian/README"
	chmod +x "${pkg_dir}/debian/postinst"
	chmod +x "${pkg_dir}/debian/prerm"
	chmod +x "${pkg_dir}/debian/rules"
	systemPath="${pkg_dir}/usr/share/iicalc/"
	binPath="${pkg_dir}/usr/bin"
	config=".installer/configDefaults/ppa.ini"
	launcher=".installer/launchers/unix.sh"
	iconPath="${pkg_dir}/usr/share/icons"
	desktopFilePath="${pkg_dir}/usr/share/applications"
	desktopFile=".installer/desktopFiles/iicalc.desktop"
	installDesktopFile="true"
	buildOnly="true"

#Build snap
elif [ "$1" == "--make-snap" ]
then
	rm -rf "iicalc-snap"
	mkdir -p "iicalc-snap/snap/gui"
	mkdir -p "iicalc-snap/src/usr/bin"
	mkdir -p "iicalc-snap/src/usr/share"
	systemPath="iicalc-snap/src/usr/share/iicalc/"
	binPath="iicalc-snap/src/usr/bin"
	config=".installer/configDefaults/snap.ini"
	launcher=".installer/launchers/snap.sh"
	iconPath="iicalc-snap/snap/gui/"
	desktopFilePath="iicalc-snap/snap/gui/iicalc.desktop"
	desktopFile=".installer/desktopFiles/iicalc-snap.desktop"
	installDesktopFile="true"
	buildOnly="true"

elif [ "$1" == "--make-brew" ]
then
	rm iicalc.rb 2>/dev/null
	cp .installer/build/macos/iicalc.rb ./

	workingdir=$(pwd)
	cd "$(mktemp -d)"

	python3 -m venv venv
	source venv/bin/activate

	pip3 install -U homebrew-pypi-poet pip
	pip3 install -r "$workingdir/requirements.txt"

	# Format pip list to only include requirements and generate brew formula
	packages=($(pip3 list --format=freeze | grep -v '^\-e' | cut -d = -f 1 | tr '\r\n' ' ' | sed -e 's/homebrew-pypi-poet//g' -e 's/Jinja2//g' -e 's/MarkupSafe//g' -e 's/pip//g' -e 's/setuptools//g' | tr -s " "))
	poet "$(echo "${packages[@]}" | sed -e 's/ / --also /g' | tr -s " ")" > poetry
	sed -i '/{{resources}}/{
	  s/{{resources}}//g
	  r poetry
  }' "$workingdir/iicalc.rb"

	for package in "${packages[@]}"; do
		echo "    resource(\"$package\").stage { system \"python3\", *Language::Python.setup_install_args(libexec/\"vendor\") }" >> installInstructions
	done

	echo "" >> installInstructions

	sed -i '/{{installpypi}}/{
	s/{{installpypi}}//g
	r installInstructions
  }' "$workingdir/iicalc.rb"


	deactivate
	cd "$workingdir"

	exit

#Build AppImage
elif [ "$1" == "--make-appImage" ]
	then
		rm -rf "iicalc-appImage"
		mkdir -p "iicalc-appImage/usr/bin"
		mkdir -p "iicalc-appImage/usr/share"
		cp ".installer/build/appImage/AppRun" "iicalc-appImage"
		chmod +x "iicalc-appImage/AppRun"
		systemPath="iicalc-appImage/usr/share/iicalc/"
		binPath="iicalc-appImage/usr/bin"
		config=".installer/configDefaults/appImage.ini"
		launcher=".installer/launchers/appImage.sh"
		iconPath="iicalc-appImage/"
		desktopFilePath="iicalc-appImage"
		desktopFile=".installer/desktopFiles/iicalc-appImage.desktop"
		cp "iicalc.svg" "iicalc-appImage"
		installDesktopFile="true"
		buildOnly="true"

#Install for MacOS
elif [ "$(uname)" == "Darwin" ]
then
	if [ "$(whoami)" != "root" ]
	then
		echo "Root access is required to install ImaginaryInfinity Calculator."
		sudo "$DIR"/"${0##*/}"
		exit
	fi
	echo "The installer has detected that you are using MacOS, is this correct? (Y/n)"
	read -r yn
	if [ "$yn" == "n" ]
	then
		exit
	fi
	systemPath="/usr/local/share/iicalc/"
	binPath="/usr/local/bin/"
	config=".installer/configDefaults/macos.ini"
	launcher=".installer/launchers/macos.sh"
	iconPath="/usr/local/share/iicalc/"
	desktopFilePath="/Applications/ImaginaryInfinity Calculator.app"
	desktopFile=".installer/desktopFiles/iicalc.app"
	installDesktopFile="true"
	pythonCommand="python3"
	mkdir /usr/local/share

# Build arch pkg
elif [[ "${args[*]}" =~ "--make-pkg" ]]
	then
		cp -f .installer/build/arch/buildscript.sh ./
		bash buildscript.sh "$@"

		rm buildscript.sh

	exit 0

elif [[ "${args[*]}" =~ "--make-rpm" ]]
	then
		realdir="$(cd "$(dirname "$DIR")"; pwd)"

		cp -f .installer/build/rhel/iicalc.spec ./
		sed -i "s:{{maindir}}:$realdir:g" iicalc.spec
		version=$(cat system/version.txt)
		versionarr=("${version//-/ }")
		if [ "${#versionarr[@]}" -eq "1" ]; then
		  versionarr+=('1')
		fi

		sed -i "s/{{pkgver}}/${versionarr[0]}/g" iicalc.spec
		sed -i "s/{{pkgrel}}/${versionarr[1]}/g" iicalc.spec

		rpmpath=$(rpmbuild --target noarch -bb iicalc.spec | awk -F': ' '/Wrote: /{print $2}')

		mv "$rpmpath" ./iicalc.rpm

		exit

#Install for Android
elif [ "$(echo "$PREFIX" | grep -o 'com.termux')" != "" ]
then
	echo "The installer has detected that you are using Android, is this correct? (Y/n)"
	read -r yn
	if [ "$yn" == "n" ]
	then
		exit
	fi
	systemPath="/data/data/com.termux/files/usr/share/iicalc"
	binPath="/data/data/com.termux/files/usr/bin/"
	config=".installer/configDefaults/android.ini"
	launcher=".installer/launchers/android.sh"
	iconPath="/dev/null"
	desktopFilePath="/dev/null"
	desktopFile=".installer/desktopFiles/iicalc.desktop"
	installDesktopFile="false"
	pythonCommand="python3"

#Install for Linux
elif [ "$(uname)" == "Linux" ]
then
	if [ "$(whoami)" != "root" ]
	then
		echo "Root access is required to install ImaginaryInfinity Calculator."
		sudo "$DIR"/"${0##*/}"
		exit
	fi
	echo "The installer has detected that you are using Linux, is this correct? (Y/n)"
	read -r yn
	if [ "$yn" == "n" ]
	then
		exit
	fi
	systemPath="/usr/share/iicalc/"
	binPath="/usr/bin/"
	config=".installer/configDefaults/unix.ini"
	launcher=".installer/launchers/unix.sh"
	iconPath="/usr/share/icons"
	desktopFilePath="/usr/share/applications"
	desktopFile=".installer/desktopFiles/iicalc.desktop"
	installDesktopFile="true"
	pythonCommand="python3"

#Install for Haiku
elif [ "$(uname)" == "Haiku" ]
then
	echo "The installer has detected that you are using Haiku, is this correct? (Y/n)"
	read -r yn
	if [ "$yn" == "n" ]
	then
		exit
	fi
	prevPath=$(pwd)
	cd .installer/launchers/
	unzip -o haiku.sh.zip
	cd ../desktopFiles/haiku
	unzip -o "ImaginaryInfinity Calculator.zip"
	cd "$prevPath"
	systemPath="$HOME/.iicalc/system/"
	binPath="$HOME/config/non-packaged/bin"
	config=".installer/configDefaults/haiku.ini"
	launcher=".installer/launchers/haiku.sh"
	iconPath="$HOME/.iicalc/system/"
	mkdir "$HOME/config/settings/deskbar/menu/Applications"
	desktopFilePath="$HOME/config/settings/deskbar/menu/Applications/"
	desktopFile=".installer/desktopFiles/haiku/ImaginaryInfinity Calculator"
	installDesktopFile="true"
	echo "Would you like to automatially install Dialog from HaikuDepot? (Y/n)"
	read -r yn
	if [ ! "$yn" == "n" ]
	then
		echo "Refreshing repositories..."
		pkgman refresh
		# echo "Installing Python..."
		# echo "y" | pkgman install python39_x86
		# echo "Installing pip..."
		# echo "y" | pkgman install pip_python39
		echo "Installing dialog..."
		echo "y" | pkgman install dialog
		pythonCommand="python3"
	fi

#Install for NetBSD
elif [ "$(uname)" == "NetBSD" ]
then
		if [ "$(whoami)" != "root" ]
		then
			echo "Root access is required to install ImaginaryInfinity Calculator."
			sudo "$DIR"/"${0##*/}"
			exit
		fi
		echo "The installer has detected that you are using NetBSD, is this correct? (Y/n)"
		read -r yn
		if [ "$yn" == "n" ]
		then
			exit
		fi
		echo "Installing required packages from pkgsrc..."
		pkg_add python38
		pkg_add py38-expat
		pkg_add readline
		pkg_add py38-readline
		echo "Installing pip..."
		python3.8 -m ensurepip
		systemPath="/usr/share/iicalc/"
		binPath="/usr/bin/"
		config=".installer/configDefaults/unix.ini"
		launcher=".installer/launchers/netbsd.sh"
		iconPath="/usr/share/icons"
		desktopFilePath="/usr/share/applications"
		desktopFile=".installer/desktopFiles/iicalc.desktop"
		installDesktopFile="true"
		pythonCommand="python3.8"
elif [ "$(uname)" == "OpenBSD" ]
then
	if [ "$(whoami)" != "root" ]
	then
		echo "Root access is required to install ImaginaryInfinity Calculator."
		sudo "$DIR"/"${0##*/}"
		exit
	fi
	echo "The installer has detected that you are using OpenBSD, is this correct? (Y/n)"
	read -r yn
	if [ "$yn" == "n" ]
	then
		exit
	fi
	echo "Installing required packages from pkgsrc..."
	pkg_add -r python

	if ! pip3 > /dev/null;
	then
		pkg_add -r curl
		curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
		python3 get-pip.py
		rm get-pip.py
	fi
	systemPath="/usr/share/iicalc/"
	binPath="/usr/bin/"
	config=".installer/configDefaults/unix.ini"
	launcher=".installer/launchers/unix.sh"
	iconPath="/usr/share/icons"
	desktopFilePath="/usr/share/applications"
	desktopFile=".installer/desktopFiles/iicalc.desktop"
	installDesktopFile="true"
	pythonCommand="python3"
elif [ "$(uname)" == "FreeBSD" ]
then
	if [ "$(whoami)" != "root" ]
	then
		echo "Root access is required to install ImaginaryInfinity Calculator."
		sudo "$DIR"/"${0##*/}"
		exit
	fi
	echo "The installer has detected that you are using FreeBSD, is this correct? (Y/n)"
	read -r yn
	if [ "$yn" == "n" ]
	then
		exit
	fi
	echo "Installing required packages from pkgsrc..."
	pkg install python3

	if ! pip3 > /dev/null;
	then
		pkg install curl
		curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
		python3 get-pip.py
		rm get-pip.py
	fi
	systemPath="/usr/share/iicalc/"
	binPath="/usr/bin/"
	config=".installer/configDefaults/unix.ini"
	launcher=".installer/launchers/unix.sh"
	iconPath="/usr/share/icons"
	desktopFilePath="/usr/share/applications"
	desktopFile=".installer/desktopFiles/iicalc.desktop"
	installDesktopFile="true"
	pythonCommand="python3"
else
	echo "The installer does not currently support your operating system. You can install the calculator by manually specifying the required paths, however this is only recommended for experienced users."
	echo "Would you like to start manual installation (y/N)?"
	read -r yn
	if [ "$yn" != "y" ]
	then
		exit
	fi
	echo "Where should plugins and themes that are installed system wide be stored? (Ex. /usr/share/iicalc/)"
	read -r systemPath
	echo "Where should executable files be stored? (Ex. /usr/bin/)"
	read -r binPath
	echo "Where should icons be stored?"
	read -r iconPath
	echo "What command do you use to start Python 3?"
	read -r pythonCommand
	installDesktopFile="false"
	cp .installer/launchers/unix.sh .installer/launchers/custom.sh
	launcher=".installer/launchers/custom.sh"
	sed -i "s'systemPath=\"/usr/share/iicalc/\"'systemPath=$systemPath'" $launcher

fi
cd "$DIR"
echo "Installing launcher..."
cp -v $launcher "$binPath/iicalc"
mkdir -p "$systemPath"
#Install desktop file if requested
if [ $installDesktopFile == "true" ]
then
	echo "Installing icons..."
	if [ "$(uname)" == "Darwin" ]; then
		iconSource="$DIR/iicalc.png"
		iconDestination="$DIR/$desktopFile"
		icon=/tmp/$(basename "$iconSource")
		rsrc=/tmp/icon.rsrc

		# Create icon from the iconSource
		cp "$iconSource" "$icon"

		# Add icon to image file, meaning use itself as the icon
		sips -i "$icon"

		# Take that icon and put it into a rsrc file
		DeRez -only icns "$icon" > "$rsrc"

		# Apply the rsrc file to
		SetFile -a C "$iconDestination"

		touch "$iconDestination"/$'Icon\r'
		Rez -append "$rsrc" -o "$iconDestination"/Icon?
		SetFile -a V "$iconDestination"/Icon?

		#osascript -e 'tell application "Finder" to quit'
		#osascript -e 'delay 2'
		#osascript -e 'tell application "Finder" to activate'

		rm "$rsrc" "$icon"
	else
		cp iicalc.svg "$iconPath"
	fi

	cp -r "$desktopFile" "$desktopFilePath"
fi
#Warn about missing Python if installing
if [ "$buildOnly" != "true" ]
then
	if ! type "$pythonCommand" > /dev/null; then
		echo "Python 3 not found. You must install Python 3 before attempting to install the calculator."
		echo "On Debian based operating systems (Ubuntu, Raspbian, Debian, etc.) run: sudo apt install python3"
		echo "On Red Hat based operating systems (Fedora, CentOS, Red Hat Enterprise Linux, etc.) run: sudo dnf install python3"
		echo "On Alpine based operating systems (PostmarketOS, Alpine Linux, etc.) run: sudo apk add python3"
		echo "On Arch based operating systems (Arch Linux, Manjaro, TheShellOS) run: sudo pacman -S python"
		echo "On MacOS, download the latest Python installer from https://www.python.org/downloads/mac-osx/"
		echo "On Android based operating systems (In Termux) run: pkg install python"
	fi
fi

#Copy files
mkdir -p "$binPath"
chmod +x "$binPath/iicalc"
echo "Installing builtin plugins..."
mkdir -p "$systemPath/systemPlugins"
cp -r system/systemPlugins/* "$systemPath/systemPlugins"
mkdir -p "$systemPath/themes"
cp -r system/themes/* "$systemPath/themes"
cp -r "templates" "$systemPath"
mkdir -p "$systemPath/docs"
cp -r system/docs/* "$systemPath/docs"
mkdir -p "$systemPath/locale"
cp -r system/locale/* "$systemPath/locale"
cp README.md "$systemPath/docs/iicalc.md"
echo "Installing main Python script..."
cp main.py "$systemPath/iicalc.py"
cp requirements.txt "$systemPath"
cp messages.txt "$systemPath"
cp system/version.txt "$systemPath"
cp README.md "$systemPath"
cp $config "$systemPath/config.ini"
#Add custom URI scheme if supported
#if type xdg-mime > /dev/null; then
	#xdg-mime default iicalc.desktop x-scheme-handler/iicalc
#fi
#Install Python modules if installing
if [ "$buildOnly" != "true" ]
then

	if ! "$pythonCommand" -m pip --version 1> /dev/null 2> /dev/null;
	then
		echo ""
		printf "\033[0;31mPip does not seem to be installed. Before running the calculator, please install pip.\033[0m\n"
		echo "On Debian based operating systems (Ubuntu, Raspbian, Debian, etc.) run: sudo apt install python3-pip"
		echo "On Red Hat based operating systems (Fedora, CentOS, Red Hat Enterprise Linux, etc.) run: sudo dnf install python3-pip"
		echo "On Alpine based operating systems (PostmarketOS, Alpine Linux, etc.) run: sudo apk add py3-pip"
		echo "On Arch based operating systems (Arch Linux, Manjaro, TheShellOS) run: sudo pacman -Syu python-pip"
		printf "On MacOS, download the get-pip.py installer: \033[33mcurl https://bootstrap.pypa.io/get-pip.py -o get-pip.py\033[0m and then run: \033[33mpython3 get-pip.py\033[0m\n"
	else
		echo "Installing Python modules..."
		"$pythonCommand" -m pip install -r requirements.txt
	fi
fi
#Finish building deb
if [ "$1" == "--make-deb" ]
then
	#Calculate MD5 Sums
	cd iicalc-deb
	find . -type f ! -regex '.*.hg.*' ! -regex '.*?debian-binary.*' ! -regex '.*?DEBIAN.*' -printf '%P ' | xargs md5sum > "DEBIAN/md5sums"
	cd ..
	#Calculate Size
	sed -i "s'Installed-Size: 0'Installed-Size: $(du -s iicalc-deb/ | awk '{print $1}')'" "iicalc-deb/DEBIAN/control"
	sed -i "s'Version: 0'Version: $(cat system/version.txt)'" "iicalc-deb/DEBIAN/control"
	#Build DEB
	dpkg -b iicalc-deb iicalc.deb
	exit
fi
if [ "$1" == "--make-ppa-files" ]
then
	cd "${pkg_dir}"
	echo "${pkg_name} (${pkg_ver}) bionic; urgency=medium

  * $(git log -1 --pretty=%B | xargs)

 -- ImaginaryInfinity <tabulatejarl8@gmail.com>  $(date +'%a, %d %b %Y %H:%M:%S -0400' -d @"$(git log -1 --pretty=format:'%ct' | xargs)")

$(cat debian/changelog)" > debian/changelog

	if [ "$beta" == "true" ]; then
		sed -i 's|Source:.*|Source: https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator/-/archive/development/ImaginaryInfinity-Calculator-development.zip|' debian/copyright

		sed -i 's|Source:.*|Source: iicalc-beta|' debian/control
		sed -i 's|Package:.*|Package: iicalc-beta|' debian/control
		sed -i 's|Conflicts:.*|Conflicts: iicalc|' debian/control

		mv debian/iicalc.install debian/iicalc-beta.install
	fi

	exit
fi
#Finish building snap
if [ "$1" == "--make-snap" ]
then
	#Calculate MD5 Sums
	mkdir -p "iicalc-snap/snap"
	cp ".installer/build/snap/snapcraft.yaml" "iicalc-snap/snap/snapcraft.yaml"
	cd iicalc-snap

	if [ "$2" == "--beta" ] || [ "$2" == "-b" ]; then
		beta="true"
	else
		beta="false"
	fi

	# Version
	sed -i "s/{{vernum}}/$(cat ../system/version.txt)/g" snap/snapcraft.yaml

	# Grade
	if [ "$beta" == "true" ]; then
		sed -i "s/{{grade}}/devel/g" snap/snapcraft.yaml
	else
		sed -i "s/{{grade}}/stable/g" snap/snapcraft.yaml
	fi

	snapcraft
	exit
fi
#Finish building AppImage
if [ "$1" == "--make-appImage" ]
then
	if [ -f appimagetool-x86_64.AppImage ]
	then
		echo "Found appimagetool"
	else
		wget https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage
	fi
	chmod +x ./appimagetool-x86_64.AppImage
	./appimagetool-x86_64.AppImage --appimage-extract
	mv squashfs-root appimagetool
	ARCH=x86_64 ./appimagetool/AppRun iicalc-appImage
	exit
fi
if [[ ! "${args[*]}" =~ "--noclear" ]]; then
	clear
fi
if [[ ! "${args[*]}" =~ "--silent" ]]; then
	echo "Installation complete! You can now launch ImaginaryInfinity Calculator by running 'iicalc' in a terminal or by clicking on it in the application menu if available on your system"
fi
