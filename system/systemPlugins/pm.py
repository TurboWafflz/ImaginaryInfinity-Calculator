import requests
import os
import configparser
import shutil
import sys
import subprocess
from systemPlugins.core import theme, config, Locale
from systemPlugins import utils
from systemPlugins import store as store_plugin
import webbrowser
import json
from pkg_resources import Requirement

from rich.progress import (
	BarColumn,
	TextColumn,
	Progress
)
import tempfile
import zipfile
from rich.markdown import Markdown
from rich.console import Console
import traceback


i18n = Locale().i18n
_list_function = list

class OAuthError(Exception):
	pass


def auth():
	webbrowser.open("https://turbowafflz.azurewebsites.net/iicalc/auth?connectCalc=true")
	print(theme["styles"]["output"] + i18n("If your browser does not automatically open, go to this URL: ") + theme["styles"]["link"] + "https://turbowafflz.azurewebsites.net/iicalc/auth?connectCalc=true" + theme["styles"]["normal"])
	token = input(theme["styles"]["input"] + i18n("Please authenticate in your browser and paste the token here: ") + theme["styles"]["normal"])
	user = json.loads(requests.get("https://api.github.com/user", headers={"Authorization": "Bearer " + token}).text)
	if "message" not in user:
		if input("Is this you? " + str(user["login"]) + " [Y/n] ").lower() == "n":
			return
		if not os.path.isdir(config["paths"]["userpath"] + "/.pluginstore"):
			os.mkdir(config["paths"]["userPath"] + "/.pluginstore")
		with open(config["paths"]["userpath"] + "/.pluginstore/.token", "w+") as f:
			f.write(token)
	else:
		print(theme["styles"]["error"] + i18n("Invalid OAuth token") + theme["styles"]["normal"])
		return


def getUserInfo():
	if not os.path.exists(config["paths"]["userpath"] + "/.pluginstore/.token"):
		raise OAuthError(i18n("Invalid OAuth Token. Please run pm.auth() to refresh your token."))

	with open(config["paths"]["userpath"] + "/.pluginstore/.token") as f:
		user = json.loads(requests.get("https://api.github.com/user", headers={"Authorization": "Bearer " + f.read().strip()}).text)
	if "message" in user:
		raise OAuthError(i18n("Invalid OAuth Token. Please run pm.auth() to refresh your token."))
	else:
		return user


# Plugin rating function
def rate(plugin):
	plugin = str(plugin)
	index = configparser.ConfigParser()
	index.read(config["paths"]["userPath"] + "/.pluginstore/index.ini")
	installed = configparser.ConfigParser()
	installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")
	if plugin in installed.sections():
		if os.path.isfile(config["paths"]["userpath"] + "/.pluginstore/.token"):
			rating = 0
			while rating not in ["1", "2"]:
				print("Rating " + plugin)
				rating = input(i18n("Upvote (1) or Downvote (2)? "))
			rating = -1 if rating == "2" else 1
			with open(config["paths"]["userpath"] + "/.pluginstore/.token") as f:
				response = requests.post("https://turbowafflz.azurewebsites.net/iicalc/rate/" + plugin, data={"vote": rating}, cookies={"authToken": f.read().strip()})
				print(response.text)
		else:
			auth()
			print()
			rate(plugin)
	elif plugin in index.sections():
		if input(i18n("You must install a plugin to rate it. Install {plugin}? [Y/n] ").format(plugin=plugin)).lower() != "n":
			install(plugin)
		else:
			print(i18n("Cancelled"))
	else:
		print(i18n("Plugin {plugin} does not exist").format(plugin=plugin))


def getUserPlugins():
	if not os.path.exists(config["paths"]["userpath"] + "/.pluginstore/.token"):
		raise OAuthError(i18n("Invalid OAuth Token. Please run pm.auth() to refresh your token."))

	with open(config["paths"]["userpath"] + "/.pluginstore/.token") as f:
		r = requests.post("https://turbowafflz.azurewebsites.net/iicalc/getplugins", cookies={"authToken": f.read().strip()})
	if "OAuth Error" in r.text or "Invalid OAuth Session" in r.text:
		raise OAuthError(i18n("Invalid OAuth Token. Please run pm.auth() to refresh your token."))
	else:
		return r.text.split(",")


# Download file
def download(url, localFilename):
	# NOTE the stream=True parameter
	r = requests.get(url, stream=True)
	with open(localFilename, 'wb') as f:
		f.write(r.content)
	return localFilename


# Check plugin against hash
def verify(plugin):
	# Load index, if available
	try:
		index = configparser.ConfigParser()
		index.read(config["paths"]["userPath"] + "/.pluginstore/index.ini")
	# If not, suggest running pm.update()
	except Exception:
		print("\n" + i18n("Could not find package list, maybe run pm.update()"))

	if index[plugin]["type"] == "plugins":
		location = config["paths"]["userPath"] + "/plugins/"
	elif index[plugin]["type"] == "themes":
		location = config["paths"]["userPath"] + "/themes/"
	else:

		print(i18n("Error installing plugin: Invalid type"))
		return "error"
	# Load installed list if available
	if os.path.exists(config["paths"]["userPath"] + "/.pluginstore/installed.ini"):
		installed = configparser.ConfigParser()
		installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")
	# If not, create it
	else:
		with open(config["paths"]["userPath"] + "/.pluginstore/installed.ini", "w+") as installedFile:
			installedFile.close()
			installed = configparser.ConfigParser()
			installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")
	if (utils.fileChecksum(location + "/" + index[plugin]["filename"], "sha256") != index[plugin]["hash"]):
		return False
	return bool(os.path.exists(config["paths"]["userPath"] + "/" + index[plugin]["type"] + "/" + index[plugin]["filename"]))


# Update package lists from server
def update(silent=False, theme=theme):
	print(theme["styles"]["error"] + "The plugin store is currently unavailable. We are investigating a solution to this issue but do not currently have any estimate as to when it will be back.")
	# if not os.path.isdir(config["paths"]["userPath"] + "/.pluginstore"):
	# 	os.makedirs(config["paths"]["userPath"] + "/.pluginstore")
	# try:
	# 	utils.progress_download(["https://turbowafflz.azurewebsites.net/iicalc/plugins/index"], config["paths"]["userPath"] + "/.pluginstore/index.ini", isFile=True)
	# except KeyboardInterrupt:
	# 	# done = True
	# 	return
	# with open(config["paths"]["userPath"] + "/.pluginstore/index.ini") as f:
	# 	tmp = f.readlines()
	# if "The service is unavailable." in tmp:
	# 	print(theme["styles"]["error"] + "\n" + i18n("The index is currently unavailable due to a temporary Microsoft Azure outage. Please try again later."))
	# 	# done=True
	# 	return
	# # Load index, if available
	# try:
	# 	index = configparser.ConfigParser()
	# 	index.read(config["paths"]["userPath"] + "/.pluginstore/index.ini")
	# # If not, suggest running pm.update()
	# except Exception:
	# 	print("\n" + i18n("Could not find package list, maybe run pm.update()"))
	# # Load installed list if available
	# if os.path.exists(config["paths"]["userPath"] + "/.pluginstore/installed.ini"):
	# 	installed = configparser.ConfigParser()
	# 	installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")
	# # If not, create it
	# else:
	# 	with open(config["paths"]["userPath"] + "/.pluginstore/installed.ini", "w+") as installedFile:
	# 		installedFile.close()
	# 		installed = configparser.ConfigParser()
	# 		installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")
	# updates = 0
	# reinstall = 0
	# # Iterate through installed plugins
	# for plugin in installed.sections():
	# 	if index[plugin]["type"] == "plugins":
	# 		location = config["paths"]["userPath"] + "/plugins/"
	# 	elif index[plugin]["type"] == "themes":
	# 		location = config["paths"]["userPath"] + "/themes/"
	# 	else:
	# 		print(i18n("Error installing plugin: Invalid type"))
	# 		return "error"
	# 	# Make sure plugin file exists
	# 	if os.path.exists(location + "/" + installed[plugin]["filename"]):
	# 		# Check if an update is available
	# 		if float(index[plugin]["lastUpdate"]) > float(installed[plugin]["lastUpdate"]) and not silent:
	# 			updates = updates + 1
	# 			print("\n" + i18n("An update is available for {plugin}").format(plugin=plugin))
	# 			# Verify plugin against the hash stored in the index
	# 		elif not utils.fileChecksum(location + "/" + index[plugin]["filename"], "sha256") == index[plugin]["hash"]:
	# 			installed[plugin]["verified"] = "false"
	# 			with open(config["paths"]["userPath"] + "/.pluginstore/installed.ini", "w+") as f:
	# 				installed.write(f)
	# 			# Warn if plugin is marked as damaged
	# 			if installed[plugin]["verified"] != "true" and not silent:
	# 				reinstall = reinstall + 1
	# 				print("\n" + i18n("{plugin} is damaged and should be reinstalled").format(plugin=plugin))
	# 	# Plugin missing, mark as unverified if not disabled
	# 	elif not os.path.exists(location + "/" + installed[plugin]["filename"] + ".disabled"):
	# 		print(i18n("File not found: {path}").format(path=location + "/" + installed[plugin]["filename"]))
	# 		print("\n" + i18n("{plugin} is missing and needs to be reinstalled").format(plugin=plugin))
	# 		reinstall = reinstall + 1
	# 		installed[plugin]["verified"] = "false"
	# 		with open(config["paths"]["userPath"] + "/.pluginstore/installed.ini", "w+") as f:
	# 			installed.write(f)
	# # Print summary for user
	# if not silent:
	# 	print("")
	# 	print("\n" + i18n("{n} packages have updates available").format(n=str(updates)))
	# 	print(i18n("{n} packages are damaged and should be reinstalled").format(n=str(reinstall)))
	# if updates > 0 or reinstall > 0 and not silent:
	# 	print(i18n("Run 'pm.upgrade()' to apply these changes"))
	# # done = True


class Plugin:
	def __init__(self, plugin, index, currentDepends=[], rootPlugin=True):
		self.index = index
		self.name = plugin
		self.root = rootPlugin
		self.download = self.index[plugin]['download']
		self.type = self.index[plugin]['type']
		self.hash = self.index[plugin]['hash']
		self.filename = self.index[plugin]['filename']

		# Parse calcversion to find the operator and version
		self.calcversion = self.index[plugin]["calcversion"]
		# Get current calculator version
		with open(config["paths"]["systemPath"] + "/version.txt") as f:
			currentversion = f.read().strip()
		# check to see if the current version of the calculator satisfys plugin required version
		if currentversion not in Requirement.parse("iicalc" + self.calcversion):
			self.isCompatible = False
		else:
			self.isCompatible = True

		# update list of dependencies if not already in the list
		self.dependsList = []
		self.currentDepends = [plugin] if currentDepends == [] else currentDepends
		if plugin not in self.currentDepends:
			self.currentDepends.append(plugin)

		if self.index.has_option(plugin, 'depends'):
			self.pypiDependencies = [
				depend for depend in self.index[plugin]['depends'].split(',')
				if depend.startswith('pypi:') and depend not in self.currentDepends
			]
			self.currentDepends += list(utils.flatten(self.pypiDependencies))
			self.themeDependencies = [
				Plugin(depend, self.index, self.currentDepends, rootPlugin=False)
				for depend in self.index[plugin]['depends'].split(',')
				if not depend.startswith('pypi:') and self.index[depend]["type"] == 'themes' and depend not in self.currentDepends and not self.isInstalled(depend)
			]
			self.currentDepends += list(map(lambda i: str(i), list(utils.flatten(self.themeDependencies))))
			self.pluginDependencies = [
				Plugin(depend, self.index, self.currentDepends, rootPlugin=False)
				for depend in self.index[plugin]['depends'].split(',')
				if not depend.startswith('pypi:') and self.index[depend]["type"] == 'plugins' and depend not in self.currentDepends and not self.isInstalled(depend)
			]
			self.currentDepends += list(map(lambda i: str(i), list(utils.flatten(self.pluginDependencies))))
		else:
			self.pypiDependencies = []
			self.themeDependencies = []
			self.pluginDependencies = []

	def getDependencies(self, currentList=[]):
		if not self.root:
			return [depend.getDependencies(currentList) for depend in self.themeDependencies + self.pluginDependencies] + self.pypiDependencies + [self if not self.root else ""]
		depends = list(utils.flatten([depend.getDependencies() for depend in self.themeDependencies + self.pluginDependencies] + self.pypiDependencies + [str(self) if not self.root else ""]))
		while "" in depends:
			depends.remove('')
		dependsNoDuplicates = []
		[dependsNoDuplicates.append(item) for item in depends if item not in dependsNoDuplicates and not self.isInstalled(item)]
		self.pypiDependencies = [dependency for dependency in dependsNoDuplicates if isinstance(dependency, str)]
		self.pluginDependencies = [dependency for dependency in dependsNoDuplicates if isinstance(dependency, Plugin) and dependency.type == 'plugins']
		self.themeDependencies = [dependency for dependency in dependsNoDuplicates if isinstance(dependency, Plugin) and dependency.type == 'themes']
		self.dependsList = dependsNoDuplicates
		return self

	def isInstalled(self, plugin):
		return True if self.index.has_section(plugin) else False

	def __str__(self):
		return self.name

	def __repr__(self):
		items = ("%s = %r" % (k, v) for k, v in self.__dict__.items())
		return "<%s: {%s}>" % (self.__class__.__name__, ', '.join(items))


def verifyInstalled(newInstalls: dict):
	verified = {}
	installed = configparser.ConfigParser()
	installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")
	for plugin, value in newInstalls.items():
		if (utils.fileChecksum(value['location'], "sha256") != newInstalls[plugin]['hash']):
			# Verification failed
			installed[plugin]["verified"] = "false"
			verified[plugin] = {'verified': 'false'}
		else:
			# Verification passed
			installed[plugin]["verified"] = "true"
			verified[plugin] = {'verified': 'true'}
	with open(config["paths"]["userPath"] + "/.pluginstore/installed.ini", "w+") as f:
		installed.write(f)

	return verified


def resolveDependencies(pluginList, index, quiet=False, debug=False):
	if not quiet:
		print(i18n("Resolving dependencies...") + "\n")

	# Resolve dependencies
	plugins = []
	currentDepends = []
	for plugin in pluginList:
		plugins.append(Plugin(plugin, index, currentDepends).getDependencies())
		currentDepends = plugins[-1].currentDepends

	if debug:
		print(f'Things to install: {list(map(lambda i: str(i), plugins))}')

	pypiDependencies = []
	for plugin in plugins:
		if isinstance(plugin, Plugin):
			for depend in plugin.pypiDependencies:
				if depend not in pypiDependencies:
					pypiDependencies.append(depend)
		elif isinstance(plugin, str):
			if plugin not in pypiDependencies:
				pypiDependencies.append(plugin)

	pluginDependencies = []
	for plugin in plugins:
		if isinstance(plugin, Plugin):
			for depend in plugin.pluginDependencies:
				if str(depend) not in list(map(lambda i: str(i), pluginDependencies)):
					pluginDependencies.append(depend)

	themeDependencies = []
	for plugin in plugins:
		if isinstance(plugin, Plugin):
			for depend in plugin.themeDependencies:
				if str(depend) not in list(map(lambda i: str(i), pluginDependencies)):
					themeDependencies.append(depend)

	# Remove things from dependencies that are explicitely called to be installed
	for element in pluginDependencies:
		if str(element) in list(map(lambda i: str(i), plugins)):
			pluginDependencies.remove(element)
	for element in themeDependencies:
		if str(element) in list(map(lambda i: str(i), plugins)):
			themeDependencies.remove(element)

	if debug:
		print(f'pypiDependencies={pypiDependencies!r}')
		print(f'pluginDependencies={list(map(lambda i: str(i), pluginDependencies))}')
		print(f'Plugin Download Links: {list(map(lambda i: i.download, pluginDependencies))}')
		print(f'themeDependenceis={themeDependencies!r}')

	return pluginDependencies, themeDependencies, pypiDependencies, plugins, index


def downloadPluginsAndDepends(pluginDependencies, themeDependencies, pypiDependencies, plugins, index, quiet=False, debug=False):

	with open(config["paths"]["systemPath"] + "/version.txt") as f:
		currentversion = f.read().strip()

	for plugin in pluginDependencies + themeDependencies + plugins:
		if debug:
			print(str(plugin) + " compatible? " + str(plugin.isCompatible))
		if (not plugin.isCompatible and input(i18n("The plugin {name} is meant for version {version} but you\'re using version {currentversion} of the calculator so it may misbehave. Download anyway? [Y/n] ").format(name=str(plugin), version=plugin.calcversion, currentversion=currentversion)).lower() == "n"):
			return False

	if not quiet:
		print(i18n('Installing dependencies...'))

	# Install PyPI dependencies
	if len(pypiDependencies) > 0:
		progress = Progress(TextColumn("[bold blue]{task.fields[package]}", justify="right"), BarColumn(bar_width=None), "[progress.percentage]{task.percentage:>3.1f}%")
		task_id = progress.add_task("download", package=pypiDependencies[0][5:], start=False, total=len(pypiDependencies))
		os.environ['PYTHONIOENCODING'] = 'UTF-8'
		with progress:
			for package in pypiDependencies:
				progress.update(task_id, package=package[5:])
				try:
					if config['system']['pipinstalluser'] == 'true':
						retcode = subprocess.call([sys.executable, "-m", "pip", "install", package[5:], '--user'], stdout=subprocess.DEVNULL)
					else:
						retcode = subprocess.call([sys.executable, "-m", "pip", "install", package[5:]], stdout=subprocess.DEVNULL)
					if retcode != 0:
						raise Exception
				except Exception:
					if not quiet:
						print("\n" + i18n("Dependency unsatisfiable: {plugin}").format(package[5:]))
					return False
				progress.start_task(task_id)
				progress.advance(task_id)

	print()

	# Set variables
	successfullyInstalled = {}

	# Install plugin dependencies
	for plugin in pluginDependencies:

		success = utils.progress_download([plugin.download], config["paths"]["userPath"] + "/plugins/" + plugin.filename, isFile=True)

		if success:
			successfullyInstalled[str(plugin)] = {
				'hash': plugin.hash,
				'location': config["paths"]["userPath"] + "/plugins/" + plugin.filename,
			}
		else:
			if not quiet:
				print()
				print('\n' + i18n('Dependancy unsatisfiable: {plugin}').format(plugin=list(map(lambda i: str(i), pluginDependencies))[success]))
			return False
	# Install theme dependencies
	for themeInstall in themeDependencies:

		success = utils.progress_download([themeInstall.download], config["paths"]["userPath"] + "/themes/" + plugin.filename, isFile=True)

		if success:
			successfullyInstalled[str(themeInstall)] = {}
			successfullyInstalled[str(themeInstall)]['hash'] = themeInstall.hash
			successfullyInstalled[str(themeInstall)]['location'] = config["paths"]["userPath"] + "/themes/" + plugin.filename

		else:
			if not quiet:
				print()
				print('\n' + i18n('Dependancy unsatisfiable: {plugin}').format(plugin=list(map(lambda i: str(i), themeDependencies))[success]))
			return False
	# Install plugins
	if not quiet:
		print("\n" + i18n("Installing plugins..."))

	if debug:
		print(f'Plugin Download Links: {list(map(lambda i: i.download, plugins))}')

	# Install explicitely installed packages
	for plugin in plugins:
		if plugin.filename[-4:].lower() == ".iiz":
			try:
				installIIZ(plugin)
				success = True
				location = config["paths"]["userPath"] + "/plugins/" + plugin.filename
			except Exception:
				success = False

		else:
			if plugin.type == 'plugins':
				location = config["paths"]["userPath"] + "/plugins/" + plugin.filename

			elif plugin.type == 'themes':
				location = config["paths"]["userPath"] + "/themes/" + plugin.filename
			if debug:
				print(f'Installation location: {location}')

			success = utils.progress_download([plugin.download], location, isFile=True)

		if not success:
			if not quiet:
				print()
				if plugin.type == 'themes':
					print('\n' + i18n('Theme unsatisfiable: {theme}').format(theme=list(map(lambda i: str(i), plugins))[success]))
				elif plugin.type == 'plugins':
					print('\n' + i18n('Plugin unsatisfiable: {plugin}').format(plugin=list(map(lambda i: str(i), plugins))[success]))
			return False
		else:
			successfullyInstalled[str(plugin)] = {}
			successfullyInstalled[str(plugin)]['hash'] = plugin.hash
			successfullyInstalled[str(plugin)]['location'] = location

	# Update installed list
	installed = configparser.ConfigParser()
	installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")
	for plugin in successfullyInstalled:
		installed[plugin] = index[plugin]
		installed[plugin]["source"] = "index"
	with open(config["paths"]["userPath"] + "/.pluginstore/installed.ini", "w+") as f:
		installed.write(f)

	return successfullyInstalled


# Install a plugin
def install(*args, prompt=False, debug=False):

	# TODO: rework plugin system to allow for updating of a single plugin's info
	update(silent=True)
	# Load index, if available
	try:
		index = configparser.ConfigParser()
		index.read(config["paths"]["userPath"] + "/.pluginstore/index.ini")
	# If not, suggest running pm.update()
	except Exception:
		print(i18n("Could not find package list, maybe run pm.update()"))
	# Load installed list if available
	# print(config.sections())
	if os.path.exists(config["paths"]["userPath"] + "/.pluginstore/installed.ini"):
		installed = configparser.ConfigParser()
		installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")
	# If not, create it
	else:
		with open(config["paths"]["userPath"] + "/.pluginstore/installed.ini", "w+") as installedFile:
			installedFile.close()
			installed = configparser.ConfigParser()
			installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")

	# Flatten out lists or tuples contained in args
	args = list(utils.flatten(args))
	args = utils.removeDuplicates(args)

	pluginDependencies, themeDependencies, pypiDependencies, requestedPlugins, index = resolveDependencies(args, index, debug=debug)
	depends = pluginDependencies + themeDependencies + pypiDependencies + requestedPlugins

	for plugin in pluginDependencies + themeDependencies + pypiDependencies + requestedPlugins:

		if not isinstance(plugin, str):

			# Set verified to none if it is not set in the installed list
			try:
				verified = installed[str(plugin)]["verified"]
			except Exception:
				verified = "none"

			# Plugin is already installed
			if installed.has_section(str(plugin)) and not verified == "false":
				# Newer version available, update
				if float(index[str(plugin)]["lastUpdate"]) > float(installed[str(plugin)]["lastUpdate"]):
					if prompt:
						if input(i18n("{plugin} has an update available. Update it? [Y/n] ").format(plugin=str(plugin))).lower() == "n":
							while plugin in depends:
								depends.remove(plugin)
				else:
					# No updates available, nothing to do
					print(i18n("{plugin} is already installed and has no update available").format(plugin=str(plugin)))
					while plugin in depends:
						depends.remove(plugin)

			# Plugin has failed verification, reinstall it
			elif verified != "true" and installed.has_section(str(plugin)):
					if prompt:
						if input(i18n("{plugin} is damaged and should be reinstalled. Reinstall it? [Y/n] ").format(plugin=str(plugin))).lower() == "n":
							while plugin in depends:
								depends.remove(plugin)

			# Plugin is not installed, install it
			elif index.has_section(str(plugin)):
					if prompt:
						if input(i18n("Install {plugin}? [Y/n] ").format(plugin=str(plugin))).lower() == "n":
							while plugin in depends:
								depends.remove(plugin)

			# Plugin could not be found
			else:
				print(theme['styles']['error'] + i18n("Package {plugin} not found.").format(plugin=str(plugin)) + theme['styles']['normal'])
				return

	if not any(isinstance(_, Plugin) for _ in depends):
		print("\n" + i18n("Nothing to do."))
		return

	# Refresh dependencies for new depends list
	pluginDependencies, themeDependencies, pypiDependencies, requestedPlugins, index = resolveDependencies([str(plugin) for plugin in depends if isinstance(plugin, Plugin)], index, quiet=True, debug=debug)

	depends = pluginDependencies + themeDependencies + pypiDependencies + requestedPlugins

	print(theme['styles']['important'] + '\n' + i18n('Packages to install:') + '\n' + theme['styles']['normal'] + ', '.join(list(map(lambda i: str(i), depends))))

	if input('\n' + i18n('Continue? [Y/n] ')).lower() == 'n':
		print(i18n('Transaction cancelled'))
		return
	else:
		print()

	try:
		newInstalls = downloadPluginsAndDepends(pluginDependencies, themeDependencies, pypiDependencies, requestedPlugins, index, debug=debug)
		if not newInstalls:
			raise Exception("")
	except Exception as e:
		print(e)
		return

	try:
		print('\n' + i18n('Verifying transaction...'))
		newInstalls = verifyInstalled(newInstalls)
	except Exception as e:
		print(e)
		traceback.print_exc()
		return

	newInstalls = [i18n('{plugin} failed verification, the package should be reinstalled.').format(plugin=plugin) for plugin in newInstalls if newInstalls[plugin]['verified'] == 'false']
	if newInstalls == []:
		print(i18n("All packages passed verification"))
	else:
		print('\n'.join(newInstalls))
	return


# Install zipped package
def installIIZ(plugin):
	oldPwd = os.getcwd()
	with tempfile.TemporaryDirectory() as td:
		os.chdir(td)
		utils.progress_download([plugin.download], plugin.filename, isFile=True)
		# open(plugin.filename, "wb+").write(requests.get(plugin.download).content)
		zipfile.ZipFile(plugin.filename, 'r').extractall()
		for type in ["plugins", "themes", "docs"]:
			if os.path.exists(type):
				for file in os.listdir(type):
					print(file)
					if not os.path.exists(f"{config['paths']['userPath']}/{type}/{str(plugin)}"):
						os.mkdir(f"{config['paths']['userPath']}/{type}/{str(plugin)}")
					if os.path.isdir(f"{type}/{file}"):
						shutil.copytree(f"{type}/{file}", f"{config['paths']['userPath']}/{type}/{str(plugin)}/{file}")
					else:
						shutil.copyfile(f"{type}/{file}", f"{config['paths']['userPath']}/{type}/{str(plugin)}/{file}")
		os.chdir(oldPwd)


def removeIIZ(plugin):
	installed = configparser.ConfigParser()
	installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")
	plugin = installed[plugin]
	for type in ["plugins", "themes", "docs"]:
		if os.path.exists(f"{config['paths']['userPath']}/{type}/{str(plugin)}"):
			shutil.rmtree(f"{config['paths']['userPath']}/{type}/{str(plugin)}")


# Remove a plugin
def remove(*args):

	args = list(utils.flatten(args))
	args = utils.removeDuplicates(args)

	# Check if installed list exists
	if os.path.exists(config["paths"]["userPath"] + "/.pluginstore/installed.ini"):
		installed = configparser.ConfigParser()
		installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")
	# If not, create it
	else:
		with open(config["paths"]["userPath"] + "/.pluginstore/installed.ini", "w+") as installedFile:
			installedFile.close()
			installed = configparser.ConfigParser()
			installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")

	# Flatten out lists or tuples contained in args
	args = list(utils.flatten(args))

	print(i18n("Removing packages...") + "\n")

	for plugin in args:
		# Check if plugin is marked as installed
		if installed.has_section(plugin):
			if installed[plugin]["filename"][-4:] == ".iiz":
				removeIIZ(plugin)
			else:
				print(plugin)
				# Remove plugin from plugins
				if installed[plugin]["type"] == "plugins":
					location = config["paths"]["userPath"] + "/plugins/"
				elif installed[plugin]["type"] == "themes":
					location = config["paths"]["userPath"] + "/themes/"
				else:
					print(i18n("Error installing plugin: Invalid type"))
					return "error"
				try:
					os.remove(location + "/" + installed[plugin]["filename"])
				except Exception:
					try:
						os.remove(location + "/" + installed[plugin]["filename"] + ".disabled")
					except Exception:
						pass
			# Remove plugin from installed list
			installed.remove_section(plugin)
		else:
			# Plugin is not installed, no need to do anything
			print(i18n("{plugin} is not installed.").format(plugin=plugin))

		# Write installed list to disk
		with open(config["paths"]["userPath"] + "/.pluginstore/installed.ini", "w+") as f:
			installed.write(f)

	print("\n" + i18n("Done"))


# Upgrade all plugins
def upgrade():
	# Read index, if available
	try:
		index = configparser.ConfigParser()
		index.read(config["paths"]["userPath"] + "/.pluginstore/index.ini")
	# Index not found, suggest running pm.update()
	except Exception:
		print(i18n("Could not find packages list, maybe run pm.update()"))
	# Check if installed list exists, if so, load it
	if os.path.exists(config["paths"]["userPath"] + "/.pluginstore/installed.ini"):
		installed = configparser.ConfigParser()
		installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")
	# If not, create it
	else:
		with open(config["paths"]["userPath"] + "/.pluginstore/installed.ini", "w+") as installedFile:
			installedFile.close()
			installed = configparser.ConfigParser()
			installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")
	updates = []
	reinstall = []
	# Iterate through installed plugins
	for plugin in installed.sections():
		# Read installed list
		installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")
		index.read(config["paths"]["userPath"] + "/.pluginstore/index.ini")
		if index[plugin]["type"] == "plugins":
			location = config["paths"]["userPath"] + "/plugins/"
		elif index[plugin]["type"] == "themes":
			location = config["paths"]["userPath"] + "/themes/"
		else:
			print(i18n("Error installing plugin: Invalid type"))
			return "error"
		# Make sure plugin's file exists
		if os.path.exists(location + "/" + installed[plugin]["filename"]):
			# Check plugin against hash
			if (utils.fileChecksum(location + "/" + index[plugin]["filename"], "sha256") != index[plugin]["hash"]):
				installed[plugin]["verified"] = "false"
				with open(config["paths"]["userPath"] + "/.pluginstore/installed.ini", "w+") as f:
					installed.write(f)
			# Check plugin update time against the latest version
			if float(index[plugin]["lastUpdate"]) > float(installed[plugin]["lastUpdate"]):
				# print("Updating " + plugin + "...")
				updates.append(plugin)
			# Plugin is marked as unverified, offer to reinstall it
			elif installed[plugin]["verified"] == "false":
				print("Hash: " + utils.fileChecksum(location + "/" + index[plugin]["filename"], "sha256"))
				print("Expected: " + index[plugin]["hash"])
				if input(i18n("{plugin} appears to be damaged, would you like to reinstall it? (Y/n) ").format(plugin=plugin)).lower() != "n":
					reinstall.append(plugin)
		elif not os.path.exists(location + "/" + installed[plugin]["filename"] + ".disabled"):
			# Plugin file is missing, offer to reinstall it
			print(i18n("File not found: {file}").format(file=location + "/" + installed[plugin]["filename"]))
			if input(i18n("{plugin} appears to be damaged, would you like to reinstall it? (Y/n) ").format(plugin=plugin)).lower() != "n":
				reinstall.append(plugin)

	install(reinstall + updates)

	# print("Done:")
	# print(str(len(updates)) + " packages updated")
	# print(str(len(reinstall)) + " damaged packages reinstalled")


# Search the index for a plugin
def search(term, type="all"):
	# Read index, if available
	try:
		index = configparser.ConfigParser()
		index.read(config["paths"]["userPath"] + "/.pluginstore/index.ini")
	# Index not found, suggest running pm.update()
	except Exception:
		print(i18n("Could not find plugin list, maybe run pm.update()"))
	# Check if installed list exists, if so, load it
	if os.path.exists(config["paths"]["userPath"] + "/.pluginstore/installed.ini"):
		installed = configparser.ConfigParser()
		installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")
	# If not, create it
	else:
		with open(config["paths"]["userPath"] + "/.pluginstore/installed.ini", "w+") as installedFile:
			installedFile.close()
			installed = configparser.ConfigParser()
			installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")

	# Iterate through plugins in index
	for plugin in index.sections():
		# Show plugin if search term is included in the name or description
		if (term in plugin or term in index[plugin]["description"]) and type in ["all", config["paths"]["userPath"] + index[plugin]["type"]]:
			print(plugin + " - " + index[plugin]["description"] + " (" + index[plugin]["type"] + ")")


# List packages
def list(scope="available", type="all"):
	if scope != 'available' and scope != 'installed':
		return _list_function(scope) # fix for overriding list() function

	# Read index, if available
	try:
		index = configparser.ConfigParser()
		index.read(config["paths"]["userPath"] + "/.pluginstore/index.ini")
	# Index not found, suggest running pm.update()
	except Exception:
		print(i18n("Could not find packages list, maybe run pm.update()"))
	# Load installed list if possible
	if os.path.exists(config["paths"]["userPath"] + "/.pluginstore/installed.ini"):
		installed = configparser.ConfigParser()
		installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")
	# If not, create one
	else:
		with open(config["paths"]["userPath"] + "/.pluginstore/installed.ini", "w+") as installedFile:
			installedFile.close()
			installed = configparser.ConfigParser()
			installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")

	# List installed packages
	if scope == "installed":
		# Iterate through installed plugins
		for plugin in installed.sections():
			# Check if plugin has passed hash verification
			if installed[plugin]["verified"] == "true":
				verified = " (" + index[plugin]["type"] + ") " + i18n("Verified") + " | "
			else:
				verified = " (" + index[plugin]["type"] + ") " + i18n("Damaged, should be reinstalled") + " | "
			# Print plugin info
			if type == "all" or installed[plugin]["type"] == type:
				print(verified + plugin + " - " + installed[plugin]["summary"])
	# List plugins in index
	if scope == "available":
		# Iterate through plugins in index
		for plugin in index.sections():
			# Check if plugin is installed
			if installed.has_section(plugin):
				# Check if plugin has passed hash verification
				if installed[plugin]["verified"] == "true":
					status = " (" + index[plugin]["type"] + ") " + i18n("Installed & verified") + " | "
				else:
					status = " (" + index[plugin]["type"] + ") " + i18n("Damaged, should be reinstalled") + " | "
			else:
				status = " (" + index[plugin]["type"] + ") " + i18n("Not installed") + " | "
			# Print plugin info
			if type == "all" or config["paths"]["userPath"] + index[plugin]["type"] == type:
				print(status + plugin + " - " + index[plugin]["summary"])


# Install a package from a file
def installFromFile(file):
	# Copy the file to an ini file so configparser doesn't get mad
	shutil.copyfile(file, config["paths"]["userPath"] + "/.pluginstore/installer.ini")
	# Read index
	try:
		index = configparser.ConfigParser()
		index.read(config["paths"]["userPath"] + "/.pluginstore/index.ini")
	# Index not found, suggest running pm.update()
	except Exception:
		print(i18n("Could not find package list, maybe run pm.update()"))
		return
	# Load locak file
	icpk = configparser.ConfigParser()
	icpk.read(config["paths"]["userPath"] + "/.pluginstore/installer.ini")

	# Check if installed list exists
	if os.path.exists(config["paths"]["userPath"] + "/.pluginstore/installed.ini"):
		installed = configparser.ConfigParser()
		installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")
	# If not, create it
	else:
		with open(config["paths"]["userPath"] + "/.pluginstore/installed.ini", "w+") as installedFile:
			installedFile.close()
			installed = configparser.ConfigParser()
			installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")
	# Iterate through all plugins in the file
	for plugin in icpk.sections():
		print(i18n("Installing dependencies..."))
		# Split dependencies into a list
		dependencies = icpk[plugin]["depends"].split(",")
		for dependency in dependencies:
			# Install dependency from index if available
			if index.has_section(dependency):
				install(dependency)
			# Dependancy already installed, do nothing
			elif installed.has_section(dependency):
				print(i18n("Dependancy already satisfied"))
			# Dependancy could not be found, abort
			elif dependency != "none":
				print(i18n("Dependency unsatisfiable: {dependency}").format(dependency=dependency))
				return
			else:
				pass
		print(i18n("Installing {plugin}...").format(plugin=plugin))
		try:
			if icpk[plugin]["type"] == "plugins":
				location = config["paths"]["userPath"] + "/plugins/"
			elif icpk[plugin]["type"] == "themes":
				location = config["paths"]["userPath"] + "/themes/"
			else:
				print(i18n("Error installing plugin: Invalid type"))
				return "error"
			utils.progress_download([icpk[plugin]["download"]], location + "/" + icpk[plugin]["filename"])
			installed[plugin] = icpk[plugin]
			installed[plugin]["source"] = "icpk"
			print(i18n("Verifying..."))
			if not utils.fileChecksum(location + "/" + icpk[plugin]["filename"], "sha256") == icpk[plugin]["hash"]:
				installed[plugin]["verified"] = "false"
				with open(config["paths"]["userPath"] + "/.pluginstore/installed.ini", "w+") as f:
					installed.write(f)
			else:
				installed[plugin]["verified"] = "true"
				with open(config["paths"]["userPath"] + "/.pluginstore/installed.ini", "w+") as f:
					installed.write(f)
		except Exception as e:
			print(i18n("Unable to download {plugin}: {error}").format(plugin=plugin, error=str(e)))


# Show information about a plugin
def info(plugin):
	try:
		index = configparser.ConfigParser()
		index.read(config["paths"]["userPath"] + "/.pluginstore/index.ini")
	except Exception:
		print(i18n("Could not find packages list, maybe run pm.update()"))
		return

	installed = configparser.ConfigParser()
	installed.read(config["paths"]["userPath"] + "/.pluginstore/installed.ini")

	# Show info from index if available
	if index.has_section(plugin):
		print(i18n("Name: {plugin}").format(plugin=plugin))
		print(i18n("Description: {description}").format(description=index[plugin]['description']))
		print(i18n("Author: {author}").format(author=index[plugin]["maintainer"]))
		print(i18n("Version: {version}").format(version=index[plugin]["version"]))
		print(i18n("Votes: {votes}").format(votes=index[plugin]["rating"]))
		# print("Screened: " + index[plugin]["approved"])
	# Show info from local install file if not available in index
	elif installed.has_section(plugin):
		print(i18n("Name: {plugin}").format(plugin=plugin))
		print(i18n("Description: {description}").format(description=index[plugin]["description"]))
		print(i18n("Author: {author}").format(author=index[plugin]["maintainer"]))
		print(i18n("Version: {version}").format(version=index[plugin]["description"]))
		print(i18n("Votes: {votes}").format(votes=index[plugin]["rating"]))
		# print("Screened: " + index[plugin]["approved"])
	# Couldn't find the plugin from any source
	else:
		print(i18n("Packages not found"))


def store():
	store_plugin.store(no_depr=True)


# Help
def help():
	console = Console()
	md = Markdown("""

# Commands:

 - `pm.update()` - Update the package list, this must be run before packages can be installed or to check for updates
 - `pm.upgrade()` - Install all available updates
 - `pm.install(*args)` - Installs the specified plugins from the plugin index. (Accepts lists) Example: `pm.install(\'algebra\', \'ptable\')`
 - `pm.remove(*args)` - Removes the specified installed plugins. (Accepts lists) Example: `pm.remove(\'algebra\', \'ptable\')`
 - `pm.list(\"<available/installed>\")` - List packages
 - `pm.search(\"<term>\")` - Search the package index
 - `pm.info(\"<plugin>\")` - Show info about a package
 - `pm.rate(\"<plugin>\")` - Rate an installed plugin
 - `pm.auth()` - Connect your installation of iicalc with your GitHub account. Allows for rating of plugins
 - `pm.installFromFile(\"<filename>\")` - Install a packages from a local *.icpk file
 - `pm.store()` - GUI Store (`store.store()` is now deprecated and will be removed in a future version)

	""", inline_code_lexer="python", inline_code_theme=config['appearance']['syntaxhighlight'])
	console.print(md)