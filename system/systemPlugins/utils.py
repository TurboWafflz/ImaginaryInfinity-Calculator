"""A collection of utility functions used throughout the calculator"""
from concurrent.futures import ThreadPoolExecutor
from functools import partial
import os
from typing import Iterable
import urllib.request
import hashlib
import warnings
import argparse
import configparser
import gettext
import ast
import shlex

from rich.progress import (
	BarColumn,
	DownloadColumn,
	TextColumn,
	TransferSpeedColumn,
	TimeRemainingColumn,
	Progress,
	TaskID,
)


#########################
# Initialize utils
#########################

parser = argparse.ArgumentParser()
parser.add_argument("--config", "-c", type=str, help="Optional config file")
# parser.add_argument("--installpmplugin", type=str, help="Prompt to install plugin with pm. For custom iicalc:// URI")
args = parser.parse_args()


class Locale:
	def __init__(self):
		localeDir = os.path.join(config['paths']['systemPath'], 'locale')
		translate = gettext.translation('base', localeDir, languages=config['system']['locale'].replace(' ', '').split(','))
		self.i18n = translate.gettext


def loadConfigFile(args):
	if args.config is not None:
		if os.path.isfile(args.config):
			config = configparser.ConfigParser()
			config.read(args.config)
			configPath = args.config
		else:
			print("Invalid config file location specified: " + args.config)
			exit()
	else:
		# Load config from ~/.iicalc
		home = os.path.expanduser("~")
		if os.path.isfile(os.path.join(home, ".iicalc", "config.ini")):
			config = configparser.ConfigParser()
			config.read(home + "/.iicalc/config.ini")
			configPath = os.path.join(home, ".iicalc", "config.ini")
		# Load config from current directory
		elif os.path.isfile(os.path.abspath("./config.ini")):
			try:
				config = configparser.ConfigParser()
				config.read(os.path.abspath("./config.ini"))
				configPath = os.path.abspath("config.ini")
			except Exception:
				print("Fatal error: Cannot load config")
				exit()

	return config, configPath


config, configPath = loadConfigFile(args)

i18n = Locale().i18n


class ShellParser:
	'''Parses text in a shell-like way and converts it into python function calls.
	Works with arguments.
	'''
	def _init_new_text(self, text):
		self.text = shlex.split(text)
		self.pointer = 2 # skip the first two which are the module and method
		self.args = []
		self.kwargs = {}

	def module_name(self):
		'''Returns the name of the module being called.'''
		return self.text[0]

	def method_name(self):
		'''Returns the method name being called.'''
		return self.text[1]

	def current_token(self):
		'''Returns the current token.'''
		try:
			return self.text[self.pointer]
		except IndexError:
			return ''

	def current_token_name(self):
		'''Return the current token name (without ``--``).'''
		return self.current_token().lstrip('-')

	def next_token(self):
		'''Returns next token in list'''
		try:
			return self.text[self.pointer + 1]
		except IndexError:
			return ''

	def is_flag(self):
		'''Check if the current token is a flag argument or not.'''
		return self.is_keyword_argument() and (self.next_token().startswith('--') or not self.next_token())

	def is_keyword_argument(self):
		'''Check if the current token is a keywork argument. i.e. starts with --'''
		return self.text[self.pointer].startswith('--')

	def incr_token_pointer(self, amount=1):
		'''Increment the token pointer by ``amount``. Defaults to 1'''
		self.pointer += amount

	def eval_arguments(self):
		'''Use ast.literal_eval to safely eval things like integers and booleans'''
		for index, item in enumerate(self.args):
			try:
				self.args[index] = ast.literal_eval(item)
			except (ValueError, SyntaxError):
				pass

		for item in self.kwargs:
			try:
				self.kwargs[item] = ast.literal_eval(self.kwargs[item])
			except (ValueError, SyntaxError):
				pass

		return self

	def parse_input(self, text):
		'''Iterate through all tokens and sort them out into args or kwargs'''

		self._init_new_text(text)

		while self.current_token():
			if self.is_flag():
				self.kwargs[self.current_token_name()] = 'True'
				self.incr_token_pointer()

			elif self.is_keyword_argument():
				self.kwargs[self.current_token_name()] = self.next_token()
				self.incr_token_pointer(2)

			else:
				self.args.append(self.current_token_name())
				self.incr_token_pointer()

		return self


#########################
# Util Functions
#########################


def copy_url(task_id: TaskID, url: str, path: str, progress) -> None:
	req = urllib.request.Request(url, data=None, headers={'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0'})
	"""Copy data from a url to a local file."""
	try:
		response = urllib.request.urlopen(req)
	except Exception:
		return False
	# This will break if the response doesn't contain content length
	progress.update(task_id, total=len(response.read()))
	response = urllib.request.urlopen(req)
	with open(path, "wb") as dest_file:
		progress.start_task(task_id)
		for data in iter(partial(response.read, 32768), b""):
			dest_file.write(data)
			progress.update(task_id, advance=len(data))
	return True


def progress_download(urls: Iterable[str], dest: str, isFile=False):
	"""Download multuple files to the given directory. Shows progress bar"""
	returns = []
	progress = Progress(TextColumn("[bold blue]{task.fields[filename]}", justify="right"), BarColumn(bar_width=None), "[progress.percentage]{task.percentage:>3.1f}%", "•", DownloadColumn(), "•", TransferSpeedColumn(), "•", TimeRemainingColumn())
	with progress:
		with ThreadPoolExecutor(max_workers=4) as pool:
			for url in urls:
				filename = url.split("/")[-1]
				task_id = progress.add_task("download", filename=filename, start=False)
				if isFile:
					returns.append(pool.submit(copy_url, task_id, url, dest, progress).result())
				else:
					returns.append(pool.submit(copy_url, task_id, url, os.path.join(dest, filename), progress).result())
	if False in returns:
		return returns.index(False)
	else:
		return True


def flatten(iterable):
	if isinstance(iterable, (list, tuple, set, range)):
		for sub in iterable:
			yield from flatten(sub)
	else:
		yield iterable


def removeDuplicates(iterable):
	res = []
	[res.append(item) for item in iterable if item not in res]
	return type(iterable)(res)


def loadConfig(config):
	items = []
	for each_section in config.sections():
		for (each_key, each_val) in config.items(each_section):
			items.append((each_section, each_key, each_val))
	return items


def fileChecksum(filename, algorithm='sha1'):
	if algorithm == "sha256":
		hasher = hashlib.sha256()
	elif algorithm == "sha512":
		hasher = hashlib.sha512()
	elif algorithm == "sha1":
		hasher = hashlib.sha1()
	elif algorithm == "md5":
		hasher = hashlib.md5()
	else:
		raise NameError(i18n('{algorithm} is not defined.').format(algorithm=algorithm))

	with open(filename, 'rb') as f:
		while True:
			data = f.read(65536) # 64kb
			if not data:
				break
			hasher.update(data)
	return hasher.hexdigest()


def deprecated(message, category=PendingDeprecationWarning, askQuit=False):
	def deprecated_decorator(func):
		def deprecated_func(*args, **kwargs):
			if kwargs.get('no_depr') is True:
				return func(*args, **kwargs)
			warnings.simplefilter('default', category)
			if category == PendingDeprecationWarning:
				warnings.warn(i18n("{name}.{funcname} will be deprecated in a future release. {message}").format(name=__name__, funcname=func.__name__, message=message), category=category, stacklevel=2)
			else:
				warnings.warn(i18n("{name}.{funcname} is a deprecated function. {message}").format(name=__name__, funcname=func.__name__, message=message), category=category, stacklevel=2)
			if askQuit:
				try:
					input(i18n('[Press any button to continue or Ctrl+C to quit]'))
				except KeyboardInterrupt:
					return
			warnings.simplefilter('default', category)
			return func(*args, **kwargs)
		return deprecated_func
	return deprecated_decorator


def getDefaultBoolMenuOption(configSection):
	if configSection == 'true':
		return i18n('On')
	else:
		return i18n('Off')
