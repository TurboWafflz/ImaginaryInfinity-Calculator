import configparser
from systemPlugins.core import clear, config, configPath, signal, theme, restart, plugins, update, loadConfigFile, Locale
import platform
from plugins import *
import argparse
from dialog import Dialog, ExecutableNotFound
import os
from pygments.styles import get_all_styles
import importlib
from systemPlugins.utils import getDefaultBoolMenuOption

parser = argparse.ArgumentParser()
parser.add_argument("--config", "-c", type=str, help="Optional config file")
# parser.add_argument("--installpmplugin", type=str, help="Prompt to install plugin with pm. For custom iicalc:// URI")
args = parser.parse_args()


i18n = Locale().i18n


# Modify Configuration file
def configMod(section, key, value, config=config):
	config[section][key] = value
	with open(configPath, "w") as configFile:
		config.write(configFile)
	signal("onSettingsSaved")
	print(i18n("Config file updated. Some changes may require a restart to take effect."))


def settingsSignal(sig, config, *args):
	for plugin in plugins(False, True):
		try:
			current_module = importlib.import_module('plugins.' + plugin[:-3])
			if (hasattr(current_module, 'settings') and hasattr(current_module.settings, sig) and callable(getattr(current_module.settings, sig))):
				current_module_settings_dialog = getattr(current_module.settings, sig)
				resp = current_module_settings_dialog(*args, config)
				if isinstance(resp, configparser.ConfigParser):
					return resp
		except Exception:
			pass
	return config


def show():
	for section in config.sections():
		print(theme["styles"]["important"] + section + theme['styles']['normal'])
		for (key, val) in config.items(section):
			print(key + " = " + val)
		print()


def startupTimes():
	if os.path.isfile(os.path.join(config['paths']['userPath'], 'startuptimes.ini')):
		startuptimes = configparser.ConfigParser()
		startuptimes.read(os.path.join(config['paths']['userPath'], 'startuptimes.ini'))
		for section in startuptimes.sections():
			for (key, val) in startuptimes.items(section):
				print(key + " - " + str(round(float(val), 2)) + "ms")
	else:
		print(theme['styles']['error'] + i18n("{path} does not exist.").format(path=os.path.join(config['paths']['userPath'], 'startuptimes.ini')) + theme['styles']['normal'])


# Dialog based settings editor
def editor():
	if platform.system() == "Windows":
		print(i18n("The setting editor does not support Windows. Don't start an issue, support will not be added."))
		return
	try:
		d = Dialog(autowidgetsize=True)
	except ExecutableNotFound:
		print(theme["styles"]["error"] + i18n("Dialog Execeutable Not Found. (Try installing \'dialog\' with your package manager)") + theme["styles"]["normal"])
		return

	# Load config
	config, _ = loadConfigFile(args)
	originalConfig, _ = loadConfigFile(args)

	mainMenuSelection = i18n("Theme")

	while True:

		# Define menu options
		choices = [
			(i18n("Theme"), i18n("The colors the calculator will use")),
			(i18n("Prompt"), i18n("The prompt that will be displayed")),
			(i18n("Update"), i18n("Update to the latest version of ImaginaryInfinity Calculator")),
			(i18n("Plugins"), i18n("Enable/disable plugins")),
			(i18n("Safe mode"), i18n("Disable all plugins except core and settings")),
			# (i18n("Server Wakeup"), i18n("Start the index server on start")),
			(i18n("Debug Mode"), i18n("Enable/disable debug mode")),
			(i18n("Check for Updates"), i18n("Check for Updates on Starup")),
			(i18n("Update Channel"), i18n("Switch which channel you\'re updating from")),
			(i18n("Subtraction from last answer"), i18n("Toggle subtraction from last answer")),
			(i18n('Show branch warning'), i18n('Show unstable branch warning if not on master')),
			(i18n("Documentation Syntax Theme"), i18n("Choose the theme for syntax highlighting")),
			(i18n("Locale"), i18n("Set your language")),
		]

		if config['startup']['safemode'] == 'false':
			for plugin in plugins(False):
				try:
					if not plugin.endswith('.disabled'):
						current_module = importlib.import_module('plugins.' + plugin[:-3])
						if hasattr(current_module, 'settings'):
							if hasattr(current_module.settings, 'choices'):
								choices += current_module.settings.choices
				except Exception:
					pass
				# print(e); import traceback; traceback.print_exc(); import sys; sys.exit(0)
		choices += [(i18n("Save and exit"), i18n("Exit the settings editor")), (i18n("Exit without saving"), i18n("Exit the settings editor without saving changes"))]

		# Display menu
		code, tag = d.menu(i18n("ImaginaryInfinity Calculator Settings"), choices=choices, cancel_label=i18n("Quit"), default_item=mainMenuSelection)
		if code == d.OK:
			clear()
			mainMenuSelection = tag
			# Theme settings
			if tag == i18n("Theme"):
				themeFiles = os.listdir(config["paths"]["userPath"] + "/themes/") + os.listdir(config["paths"]["systemPath"] + "/themes/")
				if ".placeholder" in themeFiles:
					themeFiles.remove(".placeholder")
				if len(themeFiles) == 0:
					d.msgbox(i18n("No themes installed"))
					pass
				else:
					# Handle themes in folders
					for themeFile in themeFiles:
						if os.path.isdir(config["paths"]["userPath"] + "/themes/" + themeFile):
							themeFiles.remove(themeFile)
							themeFilesInDir = os.listdir(config["paths"]["userPath"] + "/themes/" + themeFile)
							pathToThemeFilesInDir = []
							for themeFileInDir in themeFilesInDir:
								pathToThemeFilesInDir.append(themeFile + "/" + themeFileInDir)
							themeFiles.extend(pathToThemeFilesInDir)
					choices = []

					# Users currently selected theme
					currentTheme = ''

					for themeFile in themeFiles:
						themeInfo = configparser.ConfigParser()
						if os.path.exists(config["paths"]["userPath"] + "/themes/" + themeFile):
							themeInfo.read(config["paths"]["userPath"] + "/themes/" + themeFile)
						else:
							themeInfo.read(config["paths"]["systemPath"] + "/themes/" + themeFile)
						try:
							print(themeInfo["theme"]["name"])
							if themeFile == config['appearance']['theme']:
								currentTheme = themeInfo['theme']['name']
							choices.append((themeInfo["theme"]["name"], themeInfo["theme"]["description"]))
						except Exception:
							print(i18n("Invalid theme"))

					tcode, ttag = d.menu(i18n("ImaginaryInfinity Calculator Theme Settings"), choices=choices, default_item=currentTheme)
					if tcode == d.OK:
						themeFiles = os.listdir(config["paths"]["userPath"] + "/themes/") + os.listdir(config["paths"]["systemPath"] + "/themes/")
						# Handle themes in folders
						for themeFile in themeFiles:
							if os.path.isdir(config["paths"]["userPath"] + "/themes/" + themeFile):
								themeFiles.remove(themeFile)
								themeFilesInDir = os.listdir(config["paths"]["userPath"] + "/themes/" + themeFile)
								pathToThemeFilesInDir = []
								for themeFileInDir in themeFilesInDir:
									pathToThemeFilesInDir.append(themeFile + "/" + themeFileInDir)
								themeFiles.extend(pathToThemeFilesInDir)
						if ".placeholder" in themeFiles:
							themeFiles.remove(".placeholder")
						for themeFile in themeFiles:
							themeInfo = configparser.ConfigParser()
							if os.path.exists(config["paths"]["userPath"] + "/themes/" + themeFile):
								themeInfo.read(config["paths"]["userPath"] + "/themes/" + themeFile)
							else:
								themeInfo.read(config["paths"]["systemPath"] + "/themes/" + themeFile)
							if themeInfo["theme"]["name"] == ttag:
								config["appearance"]["theme"] = themeFile
					else:
						clear()
			# Prompt settings
			elif tag == i18n("Prompt"):
				# print(config)
				# import sys
				# sys.exit(0)
				pcode, pstring = d.inputbox(i18n("ImaginaryInfinity Calculator Prompt Settings"), init=config["appearance"]["prompt"])
				if pcode == d.OK:
					config["appearance"]["prompt"] = pstring
				else:
					clear()
			elif tag == i18n("Update"):
				update()

			elif tag == i18n("Plugins"):
				pluginslist = plugins(False)
				i = 0
				if len(pluginslist) > 0:
					for plugin in pluginslist:
						if plugin[-3:] == ".py":
							pluginslist[i] = (plugin, plugin, True)
						if plugin[-9:] == ".disabled":
							pluginslist[i] = (plugin, plugin, False)
						i += 1
					pcode, ptags = d.checklist(i18n("Plugins"), choices=pluginslist)
					i = 0
					# print(ptags)
					for plugin in pluginslist:
						if not plugin[0][-9:] == ".disabled" and not plugin[0] in ptags:
							os.rename(config["paths"]["userPath"] + "/plugins/" + plugin[0], config["paths"]["userPath"] + "/plugins/" + plugin[0] + ".disabled")
						if plugin[0][-9:] == ".disabled" and plugin[0] in ptags:
							os.rename(config["paths"]["userPath"] + "/plugins/" + plugin[0], config["paths"]["userPath"] + "/plugins/" + plugin[0][:-9])
				else:
					d.msgbox(i18n("You have not installed any plugins."))
			# Safe mode settings
			elif tag == i18n("Safe mode"):
				default = getDefaultBoolMenuOption(config['startup']['safemode'])

				scode, stag = d.menu(i18n("ImaginaryInfinity Calculator Safe Mode Settings"), choices=[(i18n("On"), i18n("Enable safe mode")), (i18n("Off"), i18n("Disable safe mode"))], default_item=default)
				if scode == d.OK:
					if stag == i18n("On"):
						config["startup"]["safemode"] = "true"
					if stag == i18n("Off"):
						config["startup"]["safemode"] = "false"
			# Server wakeup settings
			# elif tag == i18n("Server Wakeup"):
			# 	default = getDefaultBoolMenuOption(config['startup']['startserver'])
			#
			# 	startserver = d.menu(i18n("ImaginaryInfinity Calculator Server Wakeup"), choices=[(i18n("On"), i18n("Enable starting server on start")), (i18n("Off"), i18n("Disable starting server on start"))], default_item=default)
			# 	if startserver[0] == d.OK:
			# 		if startserver[1] == i18n("On"):
			# 			config["startup"]["startserver"] = "true"
			# 		else:
			# 			config["startup"]["startserver"] = "false"

			# Debug mode settings
			elif tag == i18n("Debug Mode"):
				default = getDefaultBoolMenuOption(config['dev']['debug'])

				debugmode = d.menu(i18n("ImaginaryInfinity Calculator Debug Settings"), choices=[(i18n("On"), i18n("Enable debug mode")), (i18n("Off"), i18n("Disable debug mode"))], default_item=default)
				if debugmode[0] == d.OK:
					if debugmode[1] == "On":
						config["dev"]["debug"] = "true"
					else:
						config["dev"]["debug"] = "false"

			# Check for updates settings
			elif tag == i18n("Check for Updates"):
				default = getDefaultBoolMenuOption(config['startup']['checkupdates'])

				checkupdates = d.menu(i18n("ImaginaryInfinity Calculator Update Checker Settings"), choices=[(i18n("On"), i18n("Enable checking for updates")), (i18n("Off"), i18n("Disable checking for updates"))], default_item=default)
				if checkupdates[0] == d.OK:
					if checkupdates[1] == "On":
						config["startup"]["checkupdates"] = "true"
					else:
						config["startup"]["checkupdates"] = "false"

			# Branch settings
			elif tag == i18n("Update Channel"):
				default = config['updates']['branch'].capitalize()

				updatechannel = d.menu(i18n("ImaginaryInfinity Calculator Update Channel Settings"), choices=[(i18n("Master"), i18n("This channel is the default stable channel")), (i18n("Development"), i18n("This channel may have unstable beta features"))], default_item=default)
				if updatechannel[0] == d.OK:
					if updatechannel[1] == "Master":
						config["updates"]["branch"] = "master"
					else:
						config["updates"]["branch"] = "development"

			# Subtraction settings
			elif tag == i18n("Subtraction from last answer"):
				default = getDefaultBoolMenuOption(config['system']['subtractfromlast'])

				subtract = d.menu(i18n("ImaginaryInfinity Calculator Subtraction Settings"), choices=[(i18n("On"), i18n("Calculator will subtract if \'-\' is first")), (i18n("Off"), i18n("Calculator won\'t subtract if \'-\' is first"))], default_item=default)
				if subtract[0] == d.OK:
					if subtract[1] == "On":
						config["system"]["subtractfromlast"] = "true"
					else:
						config["system"]["subtractfromlast"] = "false"

			elif tag == i18n('Show branch warning'):
				default = getDefaultBoolMenuOption(config['system']['showbranchwarning'])

				warning = d.menu(i18n('ImaginaryInfinity Calculator Branch Warning'), choices=[(i18n('On'), i18n('Show branch warning on startup if not on the master branch')), (i18n('Off'), i18n('Don\'t show branch warning on startup'))], default_item=default)
				if warning[0] == d.OK:
					if warning[1] == 'On':
						config['system']['showbranchwarning'] = 'true'
					else:
						config['system']['showbranchwarning'] = 'false'
			elif tag == i18n('Documentation Syntax Theme'):
				default = config['appearance']['syntaxhighlight']

				syntaxtheme = d.menu(i18n('ImaginaryInfinity Calculator Documentation Syntax Theme'), choices=[(style, i18n('Not selected')) if style != config['appearance']['syntaxhighlight'] else (style, i18n('Selected')) for style in list(get_all_styles())], default_item=default)
				if syntaxtheme[0] == d.OK:
					config['appearance']['syntaxhighlight'] = syntaxtheme[1]

			# elif tag == i18n('Locale'):
			# 	default = config['system']['locale']
			#
			# 	locale = d.menu(i18n('ImaginaryInfinity Calculator Locale'), choices=[('', '')], default_item=default)
			# 	if locale[0] == d.OK:
			# 		config['system']['locale'] = locale[1]

			# Close settings and write changes to config
			elif tag == i18n("Save and exit"):
				with open(configPath, "w") as configFile:
					config.write(configFile)
					configFile.close()
				break
			# Close settings without modifying config
			elif tag == i18n("Exit without saving"):
				break
			else:
				config = settingsSignal("settingsPopup", config, tag)

		else:
			if config != originalConfig:
				# Check if config has changed
				code = d.yesno(i18n("Save changes?"))
				if code == d.OK:
					tag = i18n("Save and exit")
					with open(configPath, "w") as configFile:
						config.write(configFile)
				else:
					tag = ""
			break
	# Prompt to restart to apply settings
	if tag == i18n("Save and exit"):
		if config != originalConfig:
			signal("onSettingsSaved")
			restartbox = Dialog(autowidgetsize=True).yesno(i18n("Your settings have been saved. Some settings may require a restart to take effect. Would you like to restart?"))
			if restartbox == "ok":
				clear()
				restart()
			else:
				clear()
		else:
			# Config hasn't changed; don't save
			clear()
	else:
		clear()
