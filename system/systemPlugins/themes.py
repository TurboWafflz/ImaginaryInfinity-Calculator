import configparser
import colorama
from systemPlugins.core import theme, clear, config, Locale
from dialog import Dialog
import platform


i18n = Locale().i18n


def editor():
	print(theme["styles"]["important"] + i18n("ImaginaryInfinity Calculator Theme Editor") + theme["styles"]["normal"])
	foreColors = dir(colorama.Fore)
	backColors = dir(colorama.Back)
	styles = dir(colorama.Style)
	remove = ['__delattr__', '__dir__', '__eq__', '__ge__', '__gt__', '__init__', '__le__', '__module__', '__new__', '__reduce_ex__', '__setattr__', '__str__', '__weakref__', '__delattr__', '__dir__', '__eq__', '__ge__', '__gt__', '__init__', '__le__', '__module__', '__new__', '__reduce_ex__', '__setattr__', '__str__', '__weakref__', '__dict__', '__format__', '__hash__', '__lt__', '__reduce__', '__sizeof__', '__doc__', '__init_subclass__', '__repr__', '__getattribute__', '__subclasshook__', '__ne__']
	for thing in remove:
		try:
			foreColors.remove(thing)
		except Exception:
			pass
		try:
			backColors.remove(thing)
		except Exception:
			pass
		try:
			styles.remove(thing)
		except Exception:
			pass
	for color in foreColors:
		if "__" in color:
			try:
				foreColors.remove(color)
			except Exception:
				print("weird")

	for color in backColors:
		if "__" in color:
			try:
				backColors.remove(color)
			except Exception:
				print("weird")
	for style in styles:
		if "__" in style:
			try:
				styles.remove(style)
			except Exception:
				print("weird")
	openTheme = configparser.ConfigParser()
	try:
		openTheme.read(config["paths"]["systemPath"] + "/templates/.emptytheme.iitheme")
	except Exception:
		try:
			openTheme.read(config["paths"]["userPath"] + "/templates/.emptytheme.iitheme")
		except Exception:
			print(i18n("Could not find theme template"))
			return
	themeName = input(i18n("Theme file: "))
	clear()
	print(i18n("Editing: {themeName}").format(themeName=themeName))
	print(i18n("Available colors:"))
	print("Fore: " + str(foreColors))
	print("Back: " + str(backColors))
	print("Styles: " + str(styles))
	openTheme["theme"]["name"] = input(i18n("Theme name: "))
	openTheme["theme"]["description"] = input(i18n("Theme description: "))
	openTheme["theme"]["ansi"] = "false"
	Fore = input(i18n("Normal foreground: "))
	Back = input(i18n("Normal background: "))
	Style = input(i18n("Normal style: "))
	openTheme["styles"]["normal"] = "colorama.Fore." + Fore.upper() + " + colorama.Back." + Back.upper() + " + colorama.Style." + Style.upper()
	Fore = input(i18n("Error foreground: "))
	Back = input(i18n("Error background: "))
	Style = input(i18n("Error style: "))
	openTheme["styles"]["error"] = "colorama.Fore." + Fore.upper() + " + colorama.Back." + Back.upper() + " + colorama.Style." + Style.upper()
	Fore = input(i18n("Warning foreground: "))
	Back = input(i18n("Warning background: "))
	Style = input(i18n("Warning style: "))
	openTheme["styles"]["warning"] = "colorama.Fore." + Fore.upper() + " + colorama.Back." + Back.upper() + " + colorama.Style." + Style.upper()
	Fore = input(i18n("Important foreground: "))
	Back = input(i18n("Important background: "))
	Style = input(i18n("Important style: "))
	openTheme["styles"]["important"] = "colorama.Fore." + Fore.upper() + " + colorama.Back." + Back.upper() + " + colorama.Style." + Style.upper()
	Fore = input(i18n("Startup message foreground: "))
	Back = input(i18n("Startup message background: "))
	Style = input(i18n("Startup message style: "))
	openTheme["styles"]["startupmessage"] = "colorama.Fore." + Fore.upper() + " + colorama.Back." + Back.upper() + " + colorama.Style." + Style.upper()
	Fore = input(i18n("Prompt foreground: "))
	Back = input(i18n("Prompt background: "))
	Style = input(i18n("Prompt style: "))
	openTheme["styles"]["prompt"] = "colorama.Fore." + Fore.upper() + " + colorama.Back." + Back.upper() + " + colorama.Style." + Style.upper()
	Fore = input(i18n("Link foreground: "))
	Back = input(i18n("Link background: "))
	Style = input(i18n("Link style: "))
	openTheme["styles"]["link"] = "colorama.Fore." + Fore.upper() + " + colorama.Back." + Back.upper() + " + colorama.Style." + Style.upper()
	Fore = input(i18n("Answer foreground: "))
	Back = input(i18n("Answer background: "))
	Style = input(i18n("Answer style: "))
	openTheme["styles"]["answer"] = "colorama.Fore." + Fore.upper() + " + colorama.Back." + Back.upper() + " + colorama.Style." + Style.upper()
	Fore = input(i18n("Input foreground: "))
	Back = input(i18n("Input background: "))
	Style = input(i18n("Input style: "))
	openTheme["styles"]["input"] = "colorama.Fore." + Fore.upper() + " + colorama.Back." + Back.upper() + " + colorama.Style." + Style.upper()
	Fore = input(i18n("Output foreground: "))
	Back = input(i18n("Output background: "))
	Style = input(i18n("Output style: "))
	openTheme["styles"]["output"] = "colorama.Fore." + Fore.upper() + " + colorama.Back." + Back.upper() + " + colorama.Style." + Style.upper()
	print(i18n("Saving theme..."))
	if themeName[-8:] != ".iitheme":
		themeName += ".iitheme"
	with open(config["paths"]["userPath"] + "/themes/" + themeName, "w") as themeFile:
		openTheme.write(themeFile)
