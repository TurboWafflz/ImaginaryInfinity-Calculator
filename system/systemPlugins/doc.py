import rich
from systemPlugins.core import config, theme, Locale
import shutil
import os
import pydoc


i18n = Locale().i18n


def findDoc(article):
	try:
		if os.path.exists(config["paths"]["userPath"] + f"/docs/{article}.md"):
			if not os.path.isdir(config["paths"]["userPath"] + f"/docs/{article}.md"):
				return config["paths"]["userPath"] + f"/docs/{article}.md"
			else:
				return None
	except Exception:
		pass
	try:
		if os.path.exists(config["paths"]["systemPath"] + f"/docs/{article}.md"):
			if not os.path.isdir(config["paths"]["systemPath"] + f"/docs/{article}.md"):
				return config["paths"]["systemPath"] + f"/docs/{article}.md"
			else:
				return None
	except Exception:
		pass
	return None


def view(article):
	path = findDoc(article)
	if path is None:
		print(theme["styles"]["error"] + i18n("Error: Article does not exist"))
		return

	md = rich.markdown.Markdown(open(path, "r").read(), inline_code_lexer="python", inline_code_theme=config['appearance']['syntaxhighlight'], hyperlinks=False)
	console = rich.console.Console(color_system=config["appearance"]["colorsystem"])

	if shutil.which('less') is None:
		console.print(md)

	else:
		with console.capture() as capture:
			console.print(md)
		str_output = capture.get()
		pydoc.pipepager(str_output, cmd='less -R --prompt \"' + i18n('Press [up] and [down] to scroll, press [q] to quit.') + '\"')


def list():
	articles = []
	try:
		for file in os.listdir(config["paths"]["userPath"] + "/docs/"):
			if file[-3:] == ".md":
				articles.append(file[:-3])
			if os.path.isdir(config["paths"]["userPath"] + f"/docs/{file}"):
				for fileInDir in os.listdir(config["paths"]["userPath"] + f"/docs/{file}"):
					if fileInDir[-3:] == ".md":
						articles.append(file + "/" + fileInDir[:-3])
	except Exception:
		pass
	try:
		for file in os.listdir(config["paths"]["systemPath"] + "/docs/"):
			if file[-3:] == ".md":
				articles.append(file[:-3])
			if os.path.isdir(config["paths"]["systemPath"] + f"/docs/{file}"):
				for fileInDir in os.listdir(config["paths"]["systemPath"] + f"/docs/{file}"):
					if fileInDir[-3:] == ".md":
						articles.append(file + "/" + fileInDir[:-3])
	except Exception:
		pass

	print(theme["styles"]["normal"] + i18n("Available articles:"))
	for article in sorted(articles):
		print(theme["styles"]["normal"] + article)
