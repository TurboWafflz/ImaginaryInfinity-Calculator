# The dev plugin contains advanced functions not intended for most users
import os
from systemPlugins.core import theme, pluginPath, plugins, clear, Locale
from systemPlugins import utils
from dialog import Dialog
import time
import shutil
import subprocess
import colorama
import sys

i18n = Locale().i18n


def showPalette():
	print(theme["styles"]["normal"] + i18n("Normal"))
	print(theme["styles"]["error"] + i18n("Error"))
	print(theme['styles']['warning'] + i18n("Warning"))
	print(theme["styles"]["important"] + i18n("Important"))
	print(theme["styles"]["startupmessage"] + i18n("Startup Message"))
	print(theme["styles"]["prompt"] + i18n("Prompt"))
	print(theme["styles"]["link"] + i18n("Link"))
	print(theme["styles"]["answer"] + i18n("Answer"))
	print(theme["styles"]["input"] + i18n("Input"))
	print(theme["styles"]["output"] + i18n("Output"))


# Show all color combinations for theme making
def showColors():
	colors = [
		color for color in dir(colorama.Fore)
		if not color.startswith('_') and 'RESET' not in color
	]
	styles = [
		style for style in dir(colorama.Style)
		if not style.startswith('_') and 'RESET' not in style
	]
	combinations = []
	for color in colors:
		for style in styles:
			combinations.append((color, style))

	for color in colors:
		sys.stdout.write(getattr(colorama.Fore, color) + color + colorama.Fore.RESET + ' ')

	print()
	sys.stdout.flush()

	for i in range(0, len(combinations), 3):
		sys.stdout.write(getattr(colorama.Fore, combinations[i][0]) + getattr(colorama.Style, combinations[i][1]) + 'X' + colorama.Fore.RESET + colorama.Style.RESET_ALL)
		sys.stdout.write(getattr(colorama.Fore, combinations[i][0]) + getattr(colorama.Style, combinations[i + 1][1]) + 'X' + colorama.Fore.RESET + colorama.Style.RESET_ALL)
		sys.stdout.write(getattr(colorama.Fore, combinations[i][0]) + getattr(colorama.Style, combinations[i + 2][1]) + 'X' + colorama.Fore.RESET + colorama.Style.RESET_ALL)
		sys.stdout.write(' ' * (len(combinations[i][0]) - 2))

	sys.stdout.flush()


def getReqs(filename):
	if shutil.which("pipreqs") is None:
		return ""
	if not os.path.isdir(pluginPath + "/.reqs"):
		os.mkdir(pluginPath + "/.reqs")
	shutil.move(pluginPath + "/" + filename, pluginPath + "/.reqs/" + filename)
	subprocess.call(["pipreqs", "--force", pluginPath + "/.reqs"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
	shutil.move(pluginPath + "/.reqs/" + filename, pluginPath + "/" + filename)
	with open(pluginPath + "/.reqs/requirements.txt") as f:
		reqs = [line.rstrip().split("==")[0] for line in f.readlines()]
	reqstr = ""
	if len(reqs) == 1 and reqs[0] == "":
		return ""
	else:
		for i in range(len(reqs)):
			reqstr += "pypi:" + reqs[i] + "\n"
		return reqstr


def generateStoreInfo(plugin):
	if os.path.exists(plugin):
		name = input(i18n("Plugin name (No spaces): "))
		type = input(i18n("Plugin Type (plugin/theme): ")).lower()
		description = input(i18n("Plugin description: "))
		version = input(i18n("Plugin version: "))
		maintainer = input(i18n("Maintainer email address: "))
		link = input(i18n("Direct download link (Please use GitHub or GitLab for hosting): "))
		summary = input(i18n("Description summary: "))
		hash = utils.fileChecksum256(type + "s/" + plugin, "sha256")
		print()
		print(i18n("Plugin listing information:"))
		print()
		print("[" + name + "]")
		print("description = " + description)
		print("maintainer = " + maintainer)
		print("version = " + version)
		print("download = " + link)
		print("hash = " + hash)
		print("lastupdate = " + str(time.time()))
		print("summary = " + summary)
		print("filename = " + plugin)
		print("rating = 5")
		print("ratings = 0")
	else:
		print(i18n("File not found: {path}").format(path="plugins/" + plugin))


def guiStoreInfo():
	d = Dialog()
	d.add_persistent_args(["--title", i18n("Generate Store Info")])
	pluginlist = plugins(False, True)
	# Get plugin
	choices = [(pluginlist[i], "") for i in range(len(pluginlist))]
	if not choices:
		choices = [(i18n("No Plugins Are Installed"), "")]
	resp = d.menu(i18n("Choose plugin"), choices=choices)
	if (resp[0] == d.OK and resp[1] == i18n("No Plugins Are Installed") or resp[0] != d.OK):
		clear()
		return
	else:
		# Continue Asking
		name = ""
		while name == "":
			name = d.inputbox(i18n("Plugin Name (No Spaces)"))[1].replace(" ", "_")

		if resp[1].endswith(".iitheme"):
			type = "themes"
		elif resp[1].endswith(".py"):
			type = "plugins"

		description = "\n"
		while description == "\n":
			description = d.editbox_str("", title=i18n("Plugin Description"))[1].rstrip()

		version = ""
		while version == "":
			version = d.inputbox(i18n("Plugin Version"))[1]

		maintainer = ""
		while maintainer == "":
			maintainer = d.inputbox(i18n("Maintainer Email Address"))[1]

		link = ""
		while link == "":
			link = d.inputbox(i18n("Direct Download Link (Please use GitHub or GitLab for hosting)"))[1]

		summary = ""
		while summary == "":
			summary = d.inputbox(i18n("Plugin Summary"))[1]

		if type == "plugins":
			reqs = getReqs(resp[1])
			depends = d.editbox_str(reqs, title=i18n("Dependencies separated by line breaks. Start PyPI dependancies with \'pypi:\'"))[1]
		depends = depends.replace("\n", ",")
		depends = depends.rstrip(",")

		hash = utils.fileChecksum(type + "/" + resp[1], "sha256")

		clear()

		print("[" + name + "]")
		print("description = " + description)
		print("maintainer = " + maintainer)
		print("version = " + version)
		print("download = " + link)
		print("hash = " + hash)
		print("lastupdate = " + str(time.time()))
		print("summary = " + summary)
		print("filename = " + resp[1])
		if depends != "":
			print("depends = " + depends)
		print("rating = 5")
		print("ratings = 0")
		print("type = " + type)
