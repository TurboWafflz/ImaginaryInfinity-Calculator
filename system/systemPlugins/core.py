# ImaginaryInfinity Calculator Core Plugin v2.3
# Copyright 2020 Finian Wright
import os
import platform
from colorama import Fore
from colorama import Back
from colorama import Style
import colorama
import sys
import zipfile
import requests
import shutil
import configparser
import re
import argparse
import tempfile
from systemPlugins import utils
from rich.console import Console
from rich.markdown import Markdown
import pydoc
import importlib
import gettext
import traceback
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument("--config", "-c", type=str, help="Optional config file")
# parser.add_argument("--installpmplugin", type=str, help="Prompt to install plugin with pm. For custom iicalc:// URI")
args = parser.parse_args()


# Check if config manually specified
def loadConfigFile(args):
	if args.config is not None:
		if os.path.isfile(args.config):
			config = configparser.ConfigParser()
			config.read(args.config)
			configPath = args.config
		else:
			print("Invalid config file location specified: " + args.config)
			exit()
	else:
		# Load config from ~/.iicalc
		home = os.path.expanduser("~")
		if os.path.isfile(os.path.join(home, ".iicalc", "config.ini")):
			config = configparser.ConfigParser()
			config.read(home + "/.iicalc/config.ini")
			configPath = os.path.join(home, ".iicalc", "config.ini")
		# Load config from current directory
		elif os.path.isfile(os.path.abspath("./config.ini")):
			try:
				config = configparser.ConfigParser()
				config.read(os.path.abspath("./config.ini"))
				configPath = os.path.abspath("config.ini")
			except Exception:
				print("Fatal error: Cannot load config")
				exit()

	return config, configPath


config, configPath = loadConfigFile(args)

# Get paths
themePath = config["paths"]["userPath"] + "/themes/"
pluginPath = config["paths"]["userPath"] + "/plugins/"
sys.path.insert(1, config["paths"]["userPath"])


class Locale:
	def __init__(self):
		localeDir = os.path.join(config['paths']['systemPath'], 'locale')
		translate = gettext.translation('base', localeDir, languages=config['system']['locale'].replace(' ', '').split(','))
		self.i18n = translate.gettext


i18n = Locale().i18n


class ThemeLoader:
	def __init__(self, themePath, config, quiet=False):
		self.theme = configparser.ConfigParser()
		self.ansi = False
		self.quiet = quiet
		self.valid_colorama_colors = re.compile(r'(' + '|'.join(['colorama.Back.' + item for item in dir(colorama.Back) if not item.startswith("_")] + ['colorama.Fore.' + item for item in dir(colorama.Fore) if not item.startswith("_")] + ['colorama.Style.' + item for item in dir(colorama.Style) if not item.startswith("_")]) + r"|\+|\s)+")

	def loadTheme(self, loadDark=False):
		try:
			if loadDark:
				raise Exception()

			# Try to load user theme
			if not self.quiet:
				print(i18n("Attempting to load user theme..."))

			self.theme.read(os.path.join(themePath, config['appearance']['theme']))

			if self.theme['theme']['ansi'] == 'true':
				self.ansi = True
		except Exception:
			try:

				if loadDark:
					raise Exception(i18n('Malicious code in theme'))

				# Try to load system theme
				if not self.quiet:
					print(i18n("Attempting to load system theme..."))

				self.theme.read(os.path.join(config["paths"]["systemPath"], "themes", config["appearance"]["theme"]))

				if self.theme['theme']['ansi'] == 'true':
					self.ansi = True
			except Exception as e:
				try:
					# Try to load dark theme
					self.theme.read(os.path.join(config["paths"]["systemPath"], "themes", "dark.iitheme"))

					if not self.quiet:
						if config['dev']['debug'] == 'true':
							traceback.print_exc()
						print(i18n("Failed to load selected theme. Loading dark instead."))
						print(i18n("Error: ") + str(e))
						input(i18n("[Press enter to continue]"))
				except Exception:
					if config['dev']['debug'] == 'true':
						traceback.print_exc()
					print(i18n("Fatal error: unable to find a useable theme"))
					exit()

		return self

	def validateColorama(self, string):
		# See if string is same length as valid regex match
		return len(self.valid_colorama_colors.search(string).group()) == len(string)

	def evaluateANSI(self):
		if self.ansi:
			for s in self.theme["styles"]:
				self.theme["styles"][str(s)] = self.theme["styles"][str(s)].encode("utf-8").decode("unicode_escape")
		else:
			# Validate colorama
			for s in self.theme["styles"]:
				if self.validateColorama(self.theme["styles"][str(s)]):
					self.theme["styles"][str(s)] = str(eval(self.theme["styles"][str(s)]))
				else:
					print(i18n('Part of the theme \'{themename}\' seems to be malicious. If this is a mistake, please report this as an issue at https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator').format(themename=self.theme['theme']['name']))
					print(i18n('Malicious code: ') + self.theme["styles"][str(s)])
					if input(i18n('Would you like to load this style anyways? [y/N] ')).lower() == 'y':
						self.theme["styles"][str(s)] = str(eval(self.theme["styles"][str(s)]))
					else:
						self.loadTheme(loadDark=True)

		return self

	def autoInit(self):
		self.loadTheme().evaluateANSI()
		return self


theme = ThemeLoader(themePath, config).autoInit().theme


# List Plugins
def plugins(printval=True, hidedisabled=False):
	plugins = os.listdir(config["paths"]["userPath"] + "/plugins/")
	if hidedisabled:
		for plugin in plugins:
			if plugin.endswith(".disabled") or not plugin.endswith('.py'):
				plugins.remove(plugin)
	try:
		plugins.remove("__pycache__")
	except ValueError:
		pass
	try:
		plugins.remove("__init__.py")
	except ValueError:
		pass
	try:
		plugins.remove(".reqs")
	except ValueError:
		pass
	if printval:
		for i in range(len(plugins)):
			print(Fore.GREEN + plugins[i])
	else:
		return plugins


# import plugins for signal function
if config['startup']['safemode'] == 'false':
	for plugin in plugins(False, True):
		try:
			globals()[plugin[:-3]] = importlib.import_module("plugins." + plugin[:-3])
		except KeyboardInterrupt:
			print(i18n("Cancelled loading of {plugin}").format(plugin=plugin))
		except Exception:
			pass


# Restart
def restart():
	signal("onRestart")
	print("\u001b[0m" + theme["styles"]["normal"])
	if platform.system() == "Windows":
		os.execl(sys.executable, sys.executable, * ['"' + item + '"' for item in sys.argv])
	else:
		os.execl(sys.executable, sys.executable, * sys.argv)


# Wake up server to decrease wait times when accessing store
def pingServer():
	try:
		requests.get("http://turbowafflz.azurewebsites.net", timeout=1)
	except requests.ConnectionError:
		pass
	except requests.exceptions.ReadTimeout:
		pass


# Help
def chelp():
	console = Console(color_system=config["appearance"]["colorsystem"])

	md = Markdown(open(f"{config['paths']['systemPath']}/docs/commands.md").read(), inline_code_lexer="python", inline_code_theme=config['appearance']['syntaxhighlight'])

	if shutil.which('less') is None:
		console.print(md)

	else:
		with console.capture() as capture:
			console.print(md)
		str_output = capture.get()
		pydoc.pipepager(str_output, cmd='less -R --prompt \"' + i18n('Press [up] and [down] to scroll, press [q] to quit.') + '\"')


# AllWillPerish
def AllWillPerish():
	return(i18n("Cheat mode active"))


# Check for Internet Connection
def hasInternet():
	try:
		requests.head('https://google.com', timeout=1)
		return True
	except Exception:
		return False


# Clear
def clear():
	_ = os.system("clear")
	if _ != 0:
		_ = os.system("cls")
		if _ != 0:
			print(i18n("This command is not currently supported on your OS, start an issue on the GitLab repository and support may be added."))


# Dec2Frac
def dec2frac(dec):
	# Convert int to float
	dec = float(dec)
	# Convert float to integer ratio
	frac = dec.as_integer_ratio()
	# Display integer ratio as fraction
	print(str(frac[0]) + "/" + str(frac[1]))


# Eqn2Table
def eqn2table(eqn, lowerBound, upperBound):
	x = lowerBound
	print(" x | y")
	while x <= upperBound:
		print("{0:0=2d}".format(x), "|", "{0:0=2d}".format(eval(eqn)))
		x = x + 1


# Factor
def factor(num):

	# Positive number
	if(num > 0):
		i = 1
		while(i <= num):
			isFactor = num % i
			# Print factor pair if remainder is 0
			if(isFactor == 0):
				print(i, "*", int(num / i))
			i = i + 1
	# Negative number
	if(num < 0):
		i = -1
		while(i >= num):
			isFactor = num % i
			# Print factor pair if remainder is 0
			if(isFactor == 0):
				print(i, "*", int(num / i))
			i = i - 1


# Factor List
def factorList(num, printResult=True):
	factors = []
	# Positive number
	if(num > 0):
		i = 1
		while(i <= num):
			isFactor = num % i
			# Append factor pair if remainder is 0
			if(isFactor == 0):
				factors.append(i)
			i = i + 1
	# Negative number
	if(num < 0):
		i = -1
		while(i >= num):
			isFactor = num % i
			# Append factor pair if remainder is 0
			if(isFactor == 0):
				factors.append(i)
			i = i - 1
	if(printResult):
		print(factors)
	return(factors)


# FancyFactor
def fancyFactor(num):
	# Positive number
	if(num > 0):
		i = 1
		while(i <= num):
			isFactor = num % i
			# Print factor pair, sums, and differences if remainder is 0
			if(isFactor == 0):
				print(i, "*", int(num / i))
				print(i, "+", int(num / i), "=", i + num / i)
				print(i, "-", int(num / i), "=", i - num / i)
				print("")
			i = i + 1
	# Negative number
	if(num < 0):
		i = -1
		while(i >= num):
			isFactor = num % i
			# Print factor pair, sums, and differences if remainder is 0
			if(isFactor == 0):
				print(i, "*", int(num / i))
				print(i, "+", int(num / i), "=", i + num / i)
				print(i, "-", int(num / i), "=", i - num / i)
				print("")
			i = i - 1


# isPerfect
def isPerfect(num, printResult=True):
	factorsSum = sum(factorList(num, False))
	if(factorsSum == num * 2):
		if(printResult):
			print(i18n("True"))
		return(True)
	else:
		if(printResult):
			print(i18n("False"))
		return(False)


# isPrime
def isPrime(num, printResult=True):
	# Get number of factors
	factors = len(factorList(num))
	# If only 2 factors then number is prime else false
	if(factors == 2):
		if(printResult):
			print(i18n("True"))
		return(True)
	else:
		if(printResult):
			print(i18n("False"))
		return(False)


def toStd(value, roundVal=None, printResult=True):
	value = str(value).lower()
	try:
		nums = list(re.findall("[0-9]+?(?=e)", value)[0])
	except IndexError:
		print(i18n("Not in e notation."))
		return
	enotlist = re.findall("[^e]*$", value)[0]
	negative = "-" in enotlist
	enot = "".join(enotlist[i] for i in range(len(enotlist)))
	enot = int(enot)
	if roundVal is None:
		roundVal = len(nums)
		if negative:
			roundVal += abs(enot)
	if printResult:
		print(("{:." + str(roundVal) + "f}").format(float(value)))
	else:
		return ("{:." + str(roundVal) + "f}").format(float(value))


# Quit
def quit():
	print(theme["styles"]["important"] + i18n("Goodbye") + "\n" + Fore.RESET + Back.RESET + Style.NORMAL)
	sys.exit()


# README (Linux only)
def readme():
	if shutil.which('less') is None:
		return(i18n("Sorry, this command only works with less installed"))
	console = Console(color_system=config["appearance"]["colorsystem"])
	if config["installation"]["installtype"] == "portable":
		readmePath = "README.md"
	else:
		readmePath = config['paths']['systemPath'] + '/README.md'

	with open(readmePath) as f:
		md = Markdown(f.read(), hyperlinks=False, code_theme=config['appearance']['syntaxhighlight'], inline_code_lexer="python", inline_code_theme=config['appearance']['syntaxhighlight'])
	with console.capture() as capture:
		console.print(md)
	str_output = capture.get()
	pydoc.pipepager(str_output, cmd='less -R')


# Root
def root(n, num):
	return(num ** (1 / n))


# Sh
def sh(cmd):
	os.system(cmd)


# Shell
def shell():
	while True:
		cmd = input("> ")
		if(cmd == "exit"):
			break
		print(os.system(cmd))


# Signals to trigger functions in plugins
def signal(sig, *args):
	if config['startup']['safemode'] == 'false':
		try:
			for plugin in plugins(False, True):
				current_module = importlib.import_module('plugins.' + plugin[:-3])
				if hasattr(current_module, sig) and callable(getattr(current_module, sig)):
					current_module_signal_function = getattr(current_module, sig)
					current_module_signal_function(*args)
		except Exception:
			pass

# Function for plugins to set variable
# def setCoreVariable(varname, value):
# 	valid var name
# 	if varname.isidentifier():
# 		try:
# 			safety to not overwrite plugins
# 			if varname not in inspect.stack()[1][0].f_globals:
# 				vars(sys.modules["__main__"])[varname] = value
# 				return 0
# 			else:
# 				return "Module exists"
# 		except Exception as e:
# 			return "Error: " + str(e)
# 	else:
# 		return "Invalid variable name"


def doUpdate(branch="master", theme=theme, gui=False):
	if config["installation"]["installtype"] == "portable":
		console = Console(color_system=config["appearance"]["colorsystem"])
		console.print(Markdown(i18n('Portable installation detected. Run `git pull` in the project directory to update the calculator.'), hyperlinks=False, code_theme=config['appearance']['syntaxhighlight'], inline_code_lexer="sh", inline_code_theme=config['appearance']['syntaxhighlight']))
	else:
		with tempfile.TemporaryDirectory() as td:
			os.chdir(td)
			try:
				utils.progress_download(['https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator/-/archive/' + branch + '/ImaginaryInfinity-Calculator-' + branch + '.zip'], 'newcalc.zip', isFile=True)
			except Exception as e:
				clear()
				print(e)
				sys.exit(i18n("Fatal Error"))

			print(i18n('Unzipping...'))
			with zipfile.ZipFile("newcalc.zip", 'r') as z:
				z.extractall()

			os.chdir("ImaginaryInfinity-Calculator-" + branch)

			print(i18n('Updating...'))

			# detect sudo
			if shutil.which("sudo") is None:
				os.system('bash uninstaller.sh --silent --noclear')
				os.system('bash installer.sh --noclear')
			else:
				os.system('sudo bash uninstaller.sh --silent --noclear')
				os.system('sudo bash installer.sh --noclear')

			x = input(theme["styles"]["important"] + i18n("Update Complete. Would you like to restart? [Y/n] "))
			if x != "n":
				restart()


def cmdUpdate(theme=theme, config=config):
	if input(theme["styles"]["normal"] + i18n("Would you like to update? You are currently on the {branch} branch. [y/N] ").format(branch=config['updates']['branch'])).lower() == "y":
		branch = config["updates"]["branch"]
		doUpdate(branch)
	else:
		print(i18n('Cancelled'))


def update():
	if config["installation"]["installtype"] == "debian":
		# Download
		deb = requests.get("https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator/-/jobs/artifacts/" + config["updates"]["branch"] + "/raw/iicalc.deb?job=debian%20packager")
		if deb.status_code == 404:
			print(theme["styles"]["normal"] + i18n("The {branch} branch is not currently creating a deb for new releases.").format(branch=config['updates']['branch']))
			return
		elif deb.status_code != 200:
			print(theme["styles"]["normal"] + i18n("Error code {code}").format(code=str(deb.status_code)))
			return
		else:
			with tempfile.NamedTemporaryFile() as f:
				f.write(deb.content)
				# Update
				print(theme["styles"]["normal"] + i18n("Updating the Debian version of ImaginaryInfinity Calculator requires root privileges"))
				if os.system("sudo dpkg -i " + f.name) != 0:
					print(theme["styles"]["error"] + i18n("Fatal Error, exiting.") + theme["styles"]["normal"])
					return
			x = input(theme["styles"]["important"] + i18n("Update Complete. Would you like to restart? [Y/n] "))
			if x != "n":
				restart()
	elif config['installation']['installtype'] == 'ppa':
		console = Console()
		console.print(Markdown(i18n('PPA installation detected. Run `sudo apt update` followed by `sudo apt upgrade` to update iiCalc. Note: the PPA version usually takes a bit longer than the rest of the calculator to update.'), hyperlinks=False, code_theme=config['appearance']['syntaxhighlight'], inline_code_lexer="sh", inline_code_theme=config['appearance']['syntaxhighlight']))
		return
	elif config["installation"]["installtype"] == "arch":
		archpkg = requests.get("https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator/-/jobs/artifacts/" + config["updates"]["branch"] + "/raw/iicalc-any.pkg.tar.zst?job=buildpkg")
		if archpkg.status_code == 404:
			print(i18n("The {branch} branch is not currently creating an arch package for new releases.").format(branch=config['updates']['branch']))
			return
		elif archpkg.status_code != 200:
			print(i18n("Error code {code}").format(code=str(archpkg.status_code)))
			return
		else:
			with tempfile.NamedTemporaryFile() as f:
				f.write(archpkg.content)
				# Update
				print(theme["styles"]["normal"] + i18n("Updating the Arch Linux version of ImaginaryInfinity Calculator requires root privileges"))
				if os.system("sudo pacman -U " + f.name) != 0:
					print(theme["styles"]["error"] + i18n("Fatal Error, exiting.") + theme["styles"]["normal"])
					return
			x = input(theme["styles"]["important"] + i18n("Update Complete. Would you like to restart? [Y/n] "))
			if x != "n":
				restart()
	elif config["installation"]["installtype"] == "redhat":
		rpm = requests.get("https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator/-/jobs/artifacts/" + config["updates"]["branch"] + "/raw/iicalc.rpm?job=buildrpm")
		if rpm.status_code == 404:
			print(theme["styles"]["normal"] + i18n("The {branch} branch is not currently creating an rpm package for new releases.").format(branch=config['updates']['branch']))
			return
		elif rpm.status_code != 200:
			print(theme["styles"]["normal"] + i18n("Error code {code}").format(str(rpm.status_code)))
			return
		else:
			with tempfile.NamedTemporaryFile() as f:
				f.write(rpm.content)
				# Update
				print(theme["styles"]["normal"] + i18n("Updating the RedHat/Fedora version of ImaginaryInfinity Calculator requires root privileges"))
				if os.system("sudo rpm -Uhv " + f.name) != 0:
					print(theme["styles"]["error"] + i18n("Fatal Error, exiting.") + theme["styles"]["normal"])
					return

			x = input(theme["styles"]["important"] + i18n("Update Complete. Would you like to restart? [Y/n] "))
			if x != "n":
				restart()
	elif config["installation"]["installtype"] == "aur":
		if config["updates"]["branch"] == "master":
			cloneCommand = "git clone https://aur.archlinux.org/iicalc.git"
		elif config["updates"]["branch"] == "development":
			cloneCommand = "git clone https://aur.archlinux.org/iicalc-beta.git iicalc"
		else:
			print(theme["styles"]["normal"] + i18n("Branch is not \'master\' or \'development\', terminating..."))
			return
		with tempfile.TemporaryDirectory() as td:
			os.chdir(td)
			if os.system(cloneCommand) != 0:
				print(theme["styles"]["error"] + i18n("Fatal Error, exiting.") + theme["styles"]["normal"])
				return
			os.chdir("iicalc")
			if os.system("makepkg -s") != 0:
				print(theme["styles"]["error"] + i18n("Fatal Error, exiting.") + theme["styles"]["normal"])
				return
			print(theme["styles"]["normal"] + i18n("Updating the Arch Linux version of ImaginaryInfinity Calculator requires root privileges"))
			if os.system("sudo pacman -U *.pkg*") != 0:
				print(theme["styles"]["error"] + i18n("Fatal Error, exiting.") + theme["styles"]["normal"])
				return
		print(theme["styles"]["important"] + i18n("Update Complete. Please restart the calculator to apply changes."))

	elif config['installation']['installtype'] == 'windows':
		installer_location = os.path.join(config['paths']['userpath'], 'iicalc.exe')
		if os.path.isfile(installer_location):
			os.remove(installer_location)

		exe = requests.get("https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator/-/jobs/artifacts/" + config["updates"]["branch"] + "/raw/iicalc.exe?job=buildexe")
		if exe.status_code == 404:
			print(theme["styles"]["normal"] + i18n("The {branch} branch is not currently creating an exe installer for new releases.").format(branch=config['updates']['branch']))
			return
		elif exe.status_code != 200:
			print(theme["styles"]["normal"] + i18n("Error code {code}").format(str(exe.status_code)))
			return
		else:
			print('Downloading Windows installer...')
			with open(installer_location, 'wb') as f:
				f.write(exe.content)

			print('Launching Windows installer and exiting...')
			subprocess.Popen([installer_location], start_new_session=True)
			sys.exit()

	elif config["installation"]["installtype"] == "AppImage":
		print(theme["styles"]["normal"] + i18n("Please download the latest AppImage for your branch here: https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator/-/jobs/artifacts/{branch}/raw/ImaginaryInfinity_Calculator-x86_64.AppImage?job=AppImage%20packager").format(branch=config['updates']['branch']))
	elif config['installation']['installtype'] == 'snap':
		print(theme["styles"]["normal"] + i18n('Snaps update themselves automatically. See https://snapcraft.io/docs/keeping-snaps-up-to-date for more details.'))
	else:
		if platform.system() == 'Windows':
			print(theme["styles"]["normal"] + i18n("The updater does not support Windows. Do not start an issue as this is a problem with Windows and not a problem with iicalc."))
			return
		else:
			cmdUpdate()
