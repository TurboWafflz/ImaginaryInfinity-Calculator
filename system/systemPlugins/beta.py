import re
from functools import reduce
from math import gcd
import colorama
from systemPlugins.core import Locale

i18n = Locale().i18n


class style:
	normal = colorama.Fore.RESET + colorama.Back.RESET + colorama.Style.NORMAL
	error = colorama.Fore.RED + colorama.Back.RESET + colorama.Style.BRIGHT
	important = colorama.Fore.MAGENTA + colorama.Back.RESET + colorama.Style.BRIGHT


def expFactor(exp):
	# Find monomials
	monomials = re.split(r'\+|-', exp)
	print(i18n("Monomials: {monomials}").format(monomials=str(monomials)))
	# Find variables
	variablesRaw = re.findall(r"([a-zA-Z]+)", exp)
	variables = []
	# Remove duplicates
	for variable in variablesRaw:
		if variable not in variables:
			variables.append(variable)
	# Put variables in alphabetical order
	variables.sort()
	print(i18n("Variables are: {variables}").format(variables=str(variables)))
	coefficients = []
	# Find coefficients for variables
	for variable in variables:
		coefficientsForVar = re.findall(r"(\d+)" + variable + r"(?:\+|\-|\*|\/|\b|$)", exp)
		coefficients.append(coefficientsForVar)
	for i in range(len(variables)):
		print(i18n("Coefficients for {variable} are {coefficient}").format(variable=str(variables[i - 1]), coefficient=str(coefficients[i - 1])))
	coefficientsInt = []
	# Concatenate coefficients and convert to ints
	for coefficientsForVar in coefficients:
		for coefficient in coefficientsForVar:
			coefficientsInt.append(int(coefficient))
	# Find gcf of coefficients
	gcf = reduce(gcd, coefficientsInt)
	if gcf > 1 or gcf < 1:
		print(i18n("Numerical GCF is: {gcf}").format(gcf=gcf))
	else:
		print(i18n("No numerical GCF"))


def help():
	print(i18n("Beta Plugin"))
	print("-----------")
	print(i18n("This plugin contains beta functions that are not yet ready for use."))
	print(style.important + i18n("Data from functions in this plugin should not be used except for testing purposes") + style.normal)
