# Commands:

## Calculations:

- `factor(<number>)` - Shows factor pairs for a number

- `isPrime(<number>)` - Checks whether or not a number is prime

- `toStd("<value>", [roundVal], [printResult])` - Convert e notation number to standard notation


## Settings:

- `settings.configMod('<section>', '<key>', '<value>')` - Changes a value in the config file.

- `settings.editor()` - Settings editor (Not supported on all platforms)

- `settings.show()` - Show the entire config file

- `settings.startupTimes()` - Show the time (in milliseconds) that plugins add to startup

## Utility:

- `iprt('<library name>')` - Installs and imports a Python moule from PyPI

- `readme()` - Shows the README file (Online/Linux only)

- `sh('<command>')` - Run a command directly on your computer

- `update()` - Update ImaginaryInfinity Calculator. *NOTE* updating the calculator via this command will delete any changes you may have made to the files. This command will save your plugins

- `quit()` - Quit ImaginaryInfinity Calculator


## Package manager:

- `plugins()` - Lists all plugins

- `pm.help()` - Package Manager Help

- `pm.store()` - Plugin GUI Store (`store.store()` is now deprecated and will be removed in a future version)

## Documentation:

- `doc.view("<article>")` - View a help article
- `doc.list()` - Show a list of all available articles
