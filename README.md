# ImaginaryInfinity Calculator

## NOTICE: ImaginaryInfinity Calculator has reached end of life and will no longer receive any updates. We are now working on a new, actually functional calculator, which you can find here: [https://gitlab.com/ImaginaryInfinity/squiid-calculator/squiid](https://gitlab.com/ImaginaryInfinity/squiid-calculator/squiid)

ImaginaryInfinity Calculator is a lightweight, but expandable calculator. Its
command line interface is designed to resemble that of some graphing
calculators. To perform another operation on a result, simply type the operation you wish to perform (eg. "/5" to divide the previous result by 5). To use the previous result in a new calculation, just specify the operation after the new calculation (eg. "5-" to subtract the previous result from 5), or by using the `ans` or `_` variables, like `_ + 3`. Note: commits on the master branch are far between, since we try to do all development on the development branch. If you're interested in testing new features, check out the development branch!

[![Documentation Status](https://readthedocs.org/projects/imaginaryinfinity-calculator/badge/?version=master)](https://imaginaryinfinity-calculator.readthedocs.io/en/stable/?badge=master)
The documentation can also be read at [readthedocs](https://imaginaryinfinity-calculator.readthedocs.io/en/master/).

![Screenshot](screenshots/iicalcscreenshot.png)
<a href="screenshots.md">More screenshots</a>
## Packages
We provide a packaged version of ImaginaryInfinity Calculator for some Linux distributions. They can be found below.

![Pipeline](https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator/badges/master/pipeline.svg)

### Debian/Ubuntu
iiCalc can be installed on Ubuntu based systems by adding the PPA and installing it via apt, or on Debian/Ubuntu based systems by manually downloading and installing the deb.

#### PPA:
```sh
sudo add-apt-repository ppa:imaginaryinfinity/iicalc
sudo apt update

sudo apt install iicalc # master branch
sudo apt install iicalc-beta # development branch
```

#### Manual installation:

[Download](https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator/-/jobs/artifacts/master/raw/iicalc.deb?job=debian%20packager) (iicalc.deb)

This package can be installed with apt, gdebi, qapt, etc.

### Arch Linux
You can install the AUR package or download the package below. To install from the AUR:

```bash
git clone https://aur.archlinux.org/iicalc.git && cd iicalc
makepkg -si
```

Or you can install it with the below package:

[Download](https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator/-/jobs/artifacts/master/raw/iicalc-any.pkg.tar.zst?job=buildpkg) (iicalc-any.pkg.tar.zst)

This package can be installed using pacman, with `sudo pacman -U iicalc-any.pkg.tar.zst`

### Red Hat/openSUSE
[Download](https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator/-/jobs/artifacts/master/raw/iicalc.rpm?job=buildrpm) (iicalc.rpm)

This package can be installed or updated on Red Hat based systems, like Fedora and CentOS, using rpm, with `sudo dnf install iicalc.rpm` and on openSUSE systems with `sudo zypper install iicalc.rpm`. You can also install it by opening it with a graphical package manager like YaST Software, Discover, GNOME Software, etc.

### Windows Installer
[Download](https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator/-/jobs/artifacts/master/raw/iicalc.exe?job=buildexe) (iicalc.exe)

Windows may show a window saying "Windows protected your PC." To install the package anyway, click "More Info" and then "Install anyway"

### Snap
[![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-black.svg)](https://snapcraft.io/iicalc)

The iicalc snap can be installed via `snap install iicalc`. To install the development branch/beta channel version, run `snap install --beta iicalc`

### AppImage
[Download](https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator/-/jobs/artifacts/master/raw/ImaginaryInfinity_Calculator-x86_64.AppImage?job=AppImage%20packager) (ImaginaryInfinity_Calculator-x86_64.AppImage)

Just make executable and run on many Linux distributions

----

## Installation

The advantages of using an installed version of the calculator are that you can launch it from anywhere in a terminal, just by running the `iicalc` command. Installing it also adds a shortcut to your application menu, and if you're on Windows, you have the option to add a desktop shortcut.

### Portable Mode
The calculator can be run without any installation on any device that supports Python 3. Just clone the repository and in the directory that  you cloned it to, run `python3 main.py`, or replace `python3` with your operating system's python command. (Windows is `py`, Android is `python`, etc.)

### Linux/MacOS/Android installation
ImaginaryInfinity Calculator can be installed on Linux by installing the package for your operating system. If your operating system doesn't have a package or you don't want to install a package, you can use the installer script. Just go to the directory where you downloaded the calculator and run `bash installer.sh` in your terminal to install the calculator. The calculator can be easily uninstalled by running `bash uninstaller.sh` in the same directory.

### Windows Installation
The calculator can be installed on Windows by downloading and running the windows exe installer or by using the provided powershell script. To install via the powershell script, first search `powershell` in the windows start menu, right click it, and run it as administrator. Then run `Set-ExecutionPolicy Unrestricted -Scope CurrentUser`, and type "Y" to say yes. This makes it so that the installer script can run. Now, navigate to the directory where you downloaded the calculator, and run `.\installer.ps1`. The calculator can be easily uninstalled by opening a powershell session and running `.\uninstaller.ps1` in the same directory.

----

## Command usage

```sh
usage: iicalc [-h] [--config CONFIG] [--ensurereqs] [--version]

optional arguments:
  -h, --help            show this help message and exit
  --config CONFIG, -c CONFIG
                        Specify a file path to use as the config file
  --ensurereqs, -e      Ensure that all requirements are satisfied
  --version, -V         Print version and exit
```

----

## Supported platforms
**ImaginaryInfinity Calculator fully supports the following platforms:**
- **Linux**
	- Primary development and testing OS. Should have the best support
- **Android**
	- Termux support covers all or most features including installation.

**ImaginaryInfinity Calculator has partial support for the following platforms:**
- **Haiku**
	- **Command history and line navigation are not currently available in Haiku due to the removal of the readline module**
	- Haiku has fairly good support now and is supported by the installer. There are however a few bugs such as syntax highlighting in documentation not working right
- **MacOS**
	- MacOS 10.15.7 has been tested to work
- **Various BSD OSs**
	- Extensive testing has not been performed, but the calculator should be mostly working on OpenBSD, NetBSD, and FreeBSD
- **Windows**
	- Windows support receives much less testing than other platforms
- **Any other OS that can run Python 3**
	- Start an issue on GitHub and we may improve support for your OS

**ImaginaryInfinity Calculator has been tested to work on these platforms:**
- Debian/Ubuntu
- Arch Linux
- OpenBSD
- FreeBSD
- NetBSD
- Fedora
- Alpine Linux
- Android (Termux)
- Void Linux
- MacOS Catalina 10.15.7
- Windows (With some issues in the installer; may be fixed in the future)
	- Windows 11
	- Windows 10
	- Windows 8
	- Windows 7 (Python 3.6.9)
- Haiku R1 Beta 2

**Python version:**

ImaginaryInfinity Calculator has been tested to work on Python 3.6 and above, but support in the future is not guaranteed.

<hr style="border: 1px solid white">

## Plugins
New functionality can easily be added by placing Python files with additional functions in the plugins directory or by downloading plugins from the store. To access a function added by a plugin, type `[plugin].[function]()`. For example, if you wanted to run the function `egg` from the plugin `food`, you would type `food.egg()`. Arguments placed in the parentheses will be passed to the function.
### Plugin Documentation
- [Making a standardized plugin or theme](documentation/makingStandardPlugin.md)
- [Adding custom config settings](documentation/addSettings.md)
- [Plugin Guidelines](documentation/guidelines.md)

**Note:**
Functions in the `core` plugin can be accessed without specifying `core`.
ex. `factor(7)` instead of `core.factor(7)`

<hr style="border: 1px solid white">

## Themes
The colors used by the calculator can be modified by themes. Themes are ini files that define the colors the calculator will use and are stored in the `themes` folder. To change the theme used by the calculator, run `settings.configMod("appearance", "theme", "<theme name>")`, or select a theme in the settings editor. One theme is included by default, `dark` for use on terminals with a dark background. If you use a terminal with a light background, you can download our official `light` theme from the store.

<hr style="border: 1px solid white">

## Built in commands:
The following commands are built in to the calculator or added by the "core" plugin:

- `settings.configMod("<section>", "<key>", "<value>")` - Changes a value in the config file.

- `settings.editor()` - Settings editor, not supported on all platforms

- `factor(<number>)` - Shows factor pairs for a number

- `factorList(<number>)` - Returns a list of the factors of a number

- `fancyFactor(<number>)` - Shows factor pairs and their sums and differences for
a number

- `iprt('<module>')` - Installs and imports a python module from PyPi

- `isPrime(<number>)` - Checks whether or not a number is is prime

- `isPerfect(<number>)` - Checks whether or not a number's factors add up to twice the
starting number

- `restart()` - Restarts iiCalc

- `clear()` or `clear` - Clears the screen

- `sh('<command>')` - Runs a command directly on your computer

- `update()` - Updates the calculator

- `quit()` - Quit ImaginaryInfinity Calculator

<hr style="border: 1px solid white">

## Plugin store

### CLI Store

- `pm.update()` - Update the package list, this must be run before plugins can be installed or to check for updates

- `pm.install(*args)` - Installs the specified plugins from the plugin index. (Accepts lists) Example: `pm.install('algebra', 'ptable')`

- `pm.show("<available/installed>")` - List plugins

- `pm.search("<term>")` - Search the plugin index

- `pm.info("<plugin>")` - Show info about a plugin

- `pm.upgrade()` - Install all available updates

- `pm.remove(*args)` - Removes the specified installed plugins. (Accepts lists) Example: `pm.remove('algebra', 'ptable')`

- `pm.installFromFile("<filename>")` - Install a plugin from a local \*.icpk file

### GUI Store

- `pm.store()` - Runs the GUI version of the plugin store (no longer functional) (`store.store()` is now deprecated and will be removed in a future version)

**Note:** In the GUI search box, you can specify `type:<type>` anywhere in the query to search for types of plugins. You can add a subquery by specifying it after the type. Example: `type:plugins discord` to search for only plugins with the keyword of discord or `light type:theme` to search for only themes with the keyword of light. Types of plugins include:

- plugin(s)
- theme(s)

### Submitting a plugin
The plugin store has been discontinued. You can no longer submit plugins

<hr style="border: 1px solid white">

**The following commands accept a second argument to prevent the result from being
printed. This is useful when they are used in another function so they don't
all get shown to the user:**

- `factorList(<number>, [printResult])`
- `isPrime(<number>, [printResult])`
- `isPerfect(<number>, [printResult])`
- `toStd("<value>", [roundVal], [printResult]) - Convert e notation number to standard notation`

printResult can be set to `True` or `False`, and defaults to `True` if not specified

## In-app documentation

Documentation for the calculator and supported plugins can be viewed by running `doc.view("<article>")`. You can also list all articles with `doc.list()`.
