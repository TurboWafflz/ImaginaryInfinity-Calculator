# Security Policy

## Supported Versions

The latest versions in both the master and development branches are supported.

## Reporting a Vulnerability

Email \<imaginaryinfinity at googlegroups.com\> to report a security vulnerability.