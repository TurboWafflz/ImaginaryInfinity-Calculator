## ImaginaryInfinity Calculator
## Copyright 2020-2021 Finian Wright and Connor Sample
## https://turbowafflz.gitlab.io/iicalc.html
import sys
if "-V" not in sys.argv and "--version" not in sys.argv and '--ensurereqs' not in sys.argv and '-e' not in sys.argv:
	print("Loading...")

from random import *
import time
from math import *
from cmath import *
import platform
import os
import shutil
import configparser
import subprocess
from threading import Thread
import decimal
import argparse
import atexit
import json
import importlib
import traceback
import gettext
import tokenize
import io

if '--ensurereqs' not in sys.argv:
	from colorama import Fore, Back, Style
	import colorama
	from mpmath import * # trigonometric functions
	import requests
	from packaging import version

parser = argparse.ArgumentParser()
parser.add_argument("--config", "-c", type=str, help="Specify a file path to use as the config file")
parser.add_argument("--ensurereqs", "-e", action="store_true", help="Ensure that all requirements are satisfied")
parser.add_argument("--version", "-V", action="store_true", help="Print version and exit")
# parser.add_argument("--installpmplugin", type=str, help="Prompt to install plugin with pm. For custom iicalc:// URI")
args = parser.parse_args()

# Make sure math is real and Python is not completely insane
if not 1 == 1:
	print("Mathmatical impossibility detected. Answers may not be correct")
	input("[Press enter to continue]")
if False:
	print("There is literally no way for this message to appear unless someone tampered with the source code")
	input("[Press enter to continue]")

if args.version is False and args.ensurereqs is False:
	print("Importing plugins...")
	print("Plugin failing to start? You can cancel loading the current plugin by pressing Ctrl-C.")


#########################################
# Class for initializing the config file
#########################################

class ConfigLoader:
	def __init__(self, manualPath=None):
		self.config = configparser.ConfigParser()
		self.configPath = manualPath
		self.configBroken = False

	def setConfigPath(self):
		if self.configPath is None: # No manual config path specified; set config path
			if os.path.isfile(os.path.join(os.path.expanduser("~"), ".iicalc", "config.ini")):
				self.configPath = os.path.join(os.path.expanduser("~"), ".iicalc", "config.ini")
			elif os.path.isfile("./config.ini"):
				self.configPath = os.path.abspath("./config.ini")
			else:
				raise FileNotFoundError(f"Config file not found in {os.path.join(os.path.expanduser('~'), '.iicalc', 'config.ini')} or {os.path.abspath('./config.ini')}")

		return self

	def readUserConfig(self, throwError=False):
		try:
			self.config.read(self.configPath)
		except Exception as e:

			if throwError:
				print("Error in config file at " + self.configPath + ": " + str(e) + ". Exiting")
				exit(1)

			# Config is broken
			if os.path.isfile(os.path.join(os.path.dirname(self.configPath), "config.bak")):
				if input("The config at " + self.configPath + " is broken. Restore the last backup? (" + time.ctime(os.stat(os.path.join(os.path.dirname(self.configPath), "config.bak")).st_mtime) + ") [Y/n] ").lower() != "n":
					try:
						os.remove(self.configPath)
					except Exception:
						pass

					shutil.copyfile(os.path.join(os.path.dirname(self.configPath), "config.bak"), self.configPath)

					self.readUserConfig(throwError=True)

				else:
					print("Warning: " + str(e))
					self.configBroken = True
			else:
				print("Warning: " + str(e))
				print("[" + os.path.join(os.path.dirname(self.configPath), "config.bak") + " does not exist to the config file cannot be restored to a previous state")
				input("[Press enter to continue]")
				self.configBroken = True

		return self

	def updateUserConfig(self):
		# Update user config from system-wide config if applicable
		with open(self.config['paths']['systemPath'] + "/version.txt") as f:
			systemVersion = f.read().strip()
		if os.path.isfile(self.config['paths']['systemPath'] + "/config.ini") and self.config['system']['userVersion'].strip() != systemVersion:
			systemPath = self.config['paths']['systemPath']
			oldConfig = []

			# Load old config into memory
			for each_section in self.config.sections():
				for (each_key, each_val) in self.config.items(each_section):
					oldConfig.append((each_section, each_key, each_val))

			# Read new config
			self.config = configparser.ConfigParser()
			self.config.read(systemPath + "/config.ini")

			# Restore user preferences
			for i in range(len(oldConfig)):
				if not (oldConfig[i][0] == "installation" and oldConfig[i][1] == "installtype"): # never keep user installtype value
					if oldConfig[i][0] == "system" and oldConfig[i][1] == "userversion":
						self.config[oldConfig[i][0]][oldConfig[i][1]] = systemVersion
					else:
						try:
							if not self.config.has_section(oldConfig[i][0]):
								self.config.add_section(oldConfig[i][0])
							self.config[oldConfig[i][0]][oldConfig[i][1]] = oldConfig[i][2]
						except Exception as e:
							print("Warning: " + str(e))

			# Write new config
			with open(self.configPath, "w") as cf:
				self.config.write(cf)

		return self

	def formatUserConfig(self):
		# Format user config if it's new
		formatted = False
		if self.config['installation']['installtype'] == 'snap':
			if '$SNAP' in self.config['appearance']['messagefile']:
				self.config['appearance']['messagefile'] = os.path.expandvars(self.config['appearance']['messagefile'])
				formatted = True
			if '$SNAP' in self.config['paths']['systemPath']:
				self.config['paths']['systemPath'] = os.path.expandvars(self.config['paths']['systemPath'])
				formatted = True
		if "{}" in self.config['paths']['userPath']:
			self.config["paths"]["userPath"] = self.config["paths"]["userPath"].format(os.path.expanduser("~"))
			formatted = True
		if "{}" in self.config['system']['userVersion']:
			with open(self.config['paths']['systemPath'] + "/version.txt") as f:
				self.config['system']['userVersion'] = self.config['system']['userVersion'].format(f.read().strip())
			formatted = True

		if formatted:
			with open(self.configPath, "w") as configFile:
				self.config.write(configFile)

		return self

	def verifyConfigIntegrity(self):
		# Verify that config has correct sections
		if not (self.config.has_section("paths") and self.config.has_section("dev") and self.config.has_section("startup") and self.config.has_section("updates") and self.config.has_section("appearance") and self.config.has_section("installation") and self.config.has_section("system")):
			if input("The config at " + self.configPath + " is broken. Restore the last backup? (" + time.ctime(os.stat(os.path.join(os.path.dirname(self.configPath), "config.bak")).st_mtime) + ") [Y/n] ").lower() != "n":
				# Restore config
				try:
					os.remove(self.configPath)
				except Exception:
					pass

				shutil.copyfile(os.path.join(os.path.dirname(self.configPath), "config.bak"), self.configPath)

				self.readUserConfig(throwError=True)
			else:
				print("Warning: config does not contain all needed sections.")
				self.configBroken = True

		return self

	def autoInit(self):
		self.setConfigPath().readUserConfig().formatUserConfig().updateUserConfig().verifyConfigIntegrity().backup()
		return self

	def backup(self):
		# Backup config file
		if not self.configBroken:
			with open(os.path.join(os.path.dirname(self.configPath), "config.bak"), "w") as f:
				self.config.write(f)


#########################################
# Load config
#########################################

if args.version is False and args.ensurereqs is False:
	print("Loading config...")
configInit = ConfigLoader(args.config).autoInit()

# Set variables
config = configInit.config
configPath = configInit.configPath

#########################################
# Load ansicon on Windows
#########################################
if platform.system() == 'Windows' and config['appearance']['ansicon_windows'] == 'true':
	if '--ensurereqs' not in sys.argv:
		import ansicon # To fix colors on Windows inputs
		ansicon.load()


#########################################
# Locale
#########################################

localeDir = os.path.join(config['paths']['systemPath'], 'locale')
translate = gettext.translation('base', localeDir, languages=config['system']['locale'].replace(' ', '').split(','))
i18n = translate.gettext


if args.version is True:
	if os.path.isfile(config["paths"]["systemPath"] + "/version.txt"):
		with open(config["paths"]["systemPath"] + "/version.txt") as f:
			print(Fore.MAGENTA + i18n("Version {version}").format(version=f.read().strip()) + Fore.RESET)
	else:
		print(i18n("Version file not found: {filePath}").format(filePath=config["paths"]["systemPath"] + "/version.txt"))
	exit()
if args.ensurereqs is True:
	if config['installation']['installtype'] == 'portable':
		if os.path.isfile('requirements.txt'):
			subprocess.call([sys.executable, '-m', 'pip', 'install', '-r', 'requirements.txt'])
			if platform.system() == 'Windows':
				subprocess.call([sys.executable, '-m', 'pip', 'install', 'ansicon']) # To fix colors on Windows inputs
			exit()
		else:
			print(i18n('requirements.txt not found in current directory'))
			exit(1)
	else:
		if os.path.isfile(config['paths']['systempath'] + '/requirements.txt'):
			subprocess.call([sys.executable, '-m', 'pip', 'install', '-r', f'{config["paths"]["systemPath"]}/requirements.txt'])
			if platform.system() == 'Windows':
				subprocess.call([sys.executable, '-m', 'pip', 'install', 'ansicon']) # To fix colors on Windows inputs
			exit()
		else:
			print(i18n('requirements.txt not found in {location}').format(location=config['paths']['systemPath']))
			exit(1)

pluginPath = config["paths"]["userPath"] + "/plugins/"
# Add system path to path to load built in plugins
sys.path.insert(1, config["paths"]["userPath"])

# Load system plugins
if config["paths"]["systemPath"] != "none":
	sys.path.insert(2, config["paths"]["systemPath"])
	plugins = os.listdir(config["paths"]["systemPath"] + "/systemPlugins")
	try:
		plugins.remove("core.py")
		plugins.remove("settings.py")
		plugins.remove("__init__.py")
	except Exception:
		pass
	for plugin in plugins:
		if plugin[-3:] == ".py":
			print(plugin)
			try:
				globals()[plugin[:-3]] = importlib.import_module('systemPlugins.' + plugin[:-3])
			except KeyboardInterrupt:
				print(i18n("Cancelled loading of {plugin}").format(plugin=plugin))
			except Exception as e:
				print(i18n("Error importing {plugin}, you might want to disable or remove it.").format(plugin=plugin))
				print(e)
				input("[Press enter to continue]")
		elif plugin[-9:] == ".disabled":
			print(i18n("Not loading {plugin} as it has been disabled in settings.").format(plugin=plugin[:-9]))
		else:
			print(i18n("Not loading {plugin} as it is not a valid plugin.").format(plugin=plugin))
# Load plugins

starttimes = configparser.ConfigParser()
starttimes.add_section('data')

if config["startup"]["safemode"] == "false":
	plugins = os.listdir(pluginPath)
	badPlugins = ["core.py", "settings.py", "__init__.py", "__pycache__"]
	for plugin in plugins:
		if plugin in badPlugins or plugin[:2] == "__":
			print(i18n("Not loading {plugin} as it is not a valid plugin.").format(plugin=plugin))
			continue
		if os.path.isdir(f"{pluginPath}/{plugin}"):
			pluginFiles = os.listdir(f"{pluginPath}/{plugin}")
			for pluginFile in pluginFiles:
				if pluginFile[-3:] == ".py" and not pluginFile.startswith("_"):

					# Start plugin load timer
					starttime = time.perf_counter()

					# Load plugin
					# TODO: try, except for bad things
					globals()[pluginFile[:-3]] = importlib.import_module(f'plugins.{plugin}.{pluginFile[:-3]}')

					# End plugin load timer
					endtime = time.perf_counter()
					starttimes['data'][plugin[:-3]] = f"{(endtime - starttime)*1000:0.8f}"
		elif plugin[-3:] == ".py":
			print(plugin)
			try:
				# Start plugin load timer
				starttime = time.perf_counter()

				# Load plugin
				globals()[plugin[:-3]] = importlib.import_module(f'plugins.{plugin[:-3]}')

				# End plugin load timer
				endtime = time.perf_counter()
				starttimes['data'][plugin[:-3]] = f"{(endtime - starttime)*1000:0.8f}"
			except KeyboardInterrupt:
				print(i18n("Cancelled loading of {plugin}").format(plugin=plugin))
			except Exception as e:
				print(i18n("Error importing {plugin}, you might want to disable or remove it.").format(plugin=plugin))
				print(e)
				input(i18n("[Press enter to continue]"))
		elif plugin[-9:] == ".disabled":
			print(i18n("Not loading {plugin} as it has been disabled in settings.").format(plugin=plugin[:-9]))
		else:
			print(i18n("Not loading {plugin} as it is not a valid plugin.").format(plugin=plugin))

		# Write startup times to file
		with open(os.path.join(config['paths']['userPath'], 'startuptimes.ini'), 'w') as f:
			starttimes.write(f)
else:
	print(i18n("Safe mode, only loading system plugins."))
# from plugins import *
from systemPlugins.core import *
from systemPlugins import settings
signal("onPluginsLoaded")

# Wake Server

# Ask to start server
# if config["startup"]["startserver"] == "ask":
# 	print()
# 	print()
# 	## Set color system to standard on Haiku portable first start
# 	## This should probably be moved to it's own thing instead of piggybacking on the startserver ask value, but this works for now
# 	if config["installation"]["installtype"] == "portable" and platform.system() == "Haiku":
# 		config["appearance"]["colorsystem"] = "standard"
#
# 	if input(theme["styles"]["important"] + i18n("Would you like to ping the server at startup to have faster access times to the plugin store?") + " [Y/n] " + theme["styles"]["normal"]).lower() == "n":
# 		config["startup"]["startserver"] = "false"
# 	else:
# 		config["startup"]["startserver"] = "true"
#
# 	with open(configPath, "w+") as f:
# 		config.write(f)
#
# if config["startup"]["startserver"] == "true":
# 	warmupThread = Thread(target=pingServer)
# 	warmupThread.start()
# else:
# 	warmupThread = None
warmupThread = None

# Check if up to date
if hasInternet():
	if config["startup"]["checkupdates"] == "true":
		try:
			print(i18n("Checking for update... (Press Ctrl-C to cancel)"))

			versionnum = requests.get("https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator/-/raw/" + config["updates"]["branch"] + "/system/version.txt", timeout=5)

			gitlab_check = True
			gitlab_ci = json.loads(requests.get('https://gitlab.com/api/v4/projects/21065211/pipelines?ref=' + config['updates']['branch'], timeout=5).text)

			if versionnum.status_code == 404:
				print(i18n("Not on branch with version.txt"))
				upToDate = True
			if len(gitlab_ci) == 0:
				print(i18n('GitLab CI does not run on the current branch'))
				gitlab_check = False
			else:
				versionnum = versionnum.text
				with open(config["paths"]["systemPath"] + "/version.txt") as f:
					if version.parse(versionnum) > version.parse(f.read().rstrip("\n")):

						# Check that GitLab CI for the current branch is finished
						upToDate = gitlab_ci[0]['status'] != 'success' if gitlab_check else False
					else:
						upToDate = True
		except KeyboardInterrupt:
			upToDate = True
			print(i18n("Cancelled"))
		except requests.exceptions.ConnectTimeout:
			print(i18n("Connection timed out"))
			upToDate = True

	else:
		# updates disabled
		upToDate = True

else:
	upToDate = True


# Import/install
def iprt(lib):
	try:
		globals()[lib] = importlib.import_module(lib)
	except ModuleNotFoundError:
		os.system("pip3 install " + lib)
		try:
			globals()[lib] = importlib.import_module(lib)
		except ModuleNotFoundError:
			pass


# Substitute Decimals for floats in a string of statements.
def decistmt(s):
	result = []
	tokens = tokenize.tokenize(io.BytesIO(s.encode('utf-8')).readline)
	for toknum, tokval, _, _, _ in tokens:
		if toknum == tokenize.NUMBER and '.' in tokval: # is float
			result.extend([
				(tokenize.NAME, 'decimal'),
				(tokenize.OP, '.'),
				(tokenize.NAME, 'Decimal'),
				(tokenize.OP, '('),
				(tokenize.STRING, repr(tokval)),
				(tokenize.OP, ')')
			])
		else:
			result.append((toknum, tokval))
	return tokenize.untokenize(result).decode('utf-8')


# Calculator itself
def main(config=config, warmupThread=warmupThread):

	oldcalc = " "

	shell_parser = utils.ShellParser()

	try:
		# Send signal and clear screen for different OSs
		try:
			import readline
		except ModuleNotFoundError:
			pass

		clear()

		if(platform.system() == "Linux" or "BSD" in platform.system()):
			signal("onLinuxStart")
		elif(platform.system() == "Haiku"):
			signal("onHaikuStart")
		elif(platform.system() == "Windows"):
			signal("onWindowsStart")
			colorama.init(convert=True)
		elif(platform.system() == "Darwin"):
			signal("onMacStart")
			## Remove empty history file to fix weird bug in MacOS if history is empty and calculator is exited with Ctrl-D
			try:
				if readline.get_current_history_length() == 0:
					os.remove(config["paths"]["userPath"] + ".history")
			except Exception:
				pass
		else:
			signal("onUnknownStart")
			print(i18n("Unknown OS, command history and line navigation not available."))

		# Load line history
		if "readline" in sys.modules:
			try:
				readline.read_history_file(config["paths"]["userPath"] + "/.history")
			except FileNotFoundError:
				pass
			readline.set_history_length(10000)
			atexit.register(readline.write_history_file, config["paths"]["userPath"] + "/.history")

		# Display start up stuff
		with open(config['paths']['systemPath'] + '/version.txt') as f:
			print(Fore.BLACK + Back.WHITE + i18n("ImaginaryInfinity Calculator v{versionNum}").format(versionNum=f.read().rstrip('\n')))
		if not upToDate:
			print(Fore.WHITE + Back.MAGENTA + i18n("Update available!"))
		print(theme["styles"]["normal"] + i18n("Copyright 2020-2021 Finian Wright and Connor Sample"))
		print(theme["styles"]["link"] + i18n("https://turbowafflz.gitlab.io/iicalc.html") + theme["styles"]["normal"])
		print(i18n("Type 'chelp()' for a list of commands"))
		print(i18n("Read README"))
		if config["updates"]["branch"] != "master" and config['system']['showbranchwarning'] == 'true' and platform.system() != "Windows":
			print(theme["styles"]["important"] + i18n("You are currently using an unstable update channel, you can switch back to the master channel in settings. (This message can be turned off in settings)"))
		# Display startupmessage
		try:
			with open(config["appearance"]["messageFile"]) as messagesFile:
				messages = messagesFile.readlines()
				msg = messages[randint(0, len(messages) - 1)]
				print(theme["styles"]["startupmessage"] + msg + theme["styles"]["normal"])
		except Exception:
			traceback.print_exc()
			print(i18n("Could not find messages file"))
		# if(platform.system()=="Windows"):
		# 	print(style.important + "Eww, Windows")

		ans = 0
		print('')
		calc = ''
		# if args.installpmplugin != None:
		# 	pm.update()
		# 	pm.install(args.installpmplugin, prompt=True)
		signal("onStarted")
		# Main loop
		while True:
			print_result = True
			print('')
			signal("onReady")
			# Take command from user
			try:
				calc = input(theme["styles"]["prompt"] + config["appearance"]["prompt"] + theme["styles"]["input"] + " ")
			except KeyboardInterrupt:
				calc = ' '
				if platform.system() != 'Windows':
					print(theme["styles"]["normal"] + "\n" + i18n("Press Ctrl-D or type quit() or exit() to exit"))
				else:
					print(theme["styles"]["normal"] + "\n" + i18n("Press Ctrl-Z plus Return or type quit() or exit() to exit"))
			signal("onInput", "'" + calc + "'")
			print('')
			print(theme["styles"]["output"])
			# Calculate/execute command
			try:
				cl = list(calc)
				# Special cases
				if calc == 'AllWillPerish':
					print_result = 0
					print(theme["styles"]["important"] + i18n("Cheat mode active") + theme["styles"]["normal"])
				if calc.lower() == 'exit' or calc.lower() == 'quit':
					print(theme["styles"]["important"] + i18n("Goodbye") + "\n" + Fore.RESET + Back.RESET + Style.NORMAL)
					break
				if calc == '':
					calc = oldcalc
					cl = list(calc)
				if calc == ' ':
					print_result = 0
				if calc == 'clear':
					clear()
					print_result = 0
				if calc == "restart()" or calc == "settings.editor()":
					if "readline" in sys.modules:
						readline.set_history_length(10000)
						readline.write_history_file(config["paths"]["userPath"] + "/.history")
				eqn = calc
				# Ability to disable subtraction from last answer
				if config["system"]["subtractfromlast"] == "true":
					if cl[0] == "+" or cl[0] == "-" or cl[0] == "*" or cl[0] == "/" or cl[0] == "^":
						eqn = str(ans) + str(calc)
					if cl[-1] == "+" or cl[-1] == "-" or cl[-1] == "*" or cl[-1] == "/" or cl[-1] == "^":
						eqn = str(calc) + str(ans)
				else:
					if cl[0] == "+" or cl[0] == "*" or cl[0] == "/" or cl[0] == "^":
						eqn = str(ans) + str(calc)
					if cl[-1] == "+" or cl[-1] == "*" or cl[-1] == "/" or cl[-1] == "^":
						eqn = str(calc) + str(ans)

				oldcalc = calc
				# Evaluate command
				# Test if eqn is not a function
				if len(re.findall(r"[a-zA-Z]+\([^\)]*\)(\.[^\)]*\))?", eqn)) == 0:
					# Is an equation
					try:
						# Surround every number that appears to be a decimal or integer with `decimal.Decimal('<value>')` to fix floating point arithmetic errors
						decimaleqn = decistmt(eqn)
						try:
							ans = eval(str(decimaleqn))
						except decimal.Overflow:
							# If decimal value is too big for the decimal to handle, fall back to normal eval
							ans = eval(str(eqn))

					except Exception:
						try:
							ans = eval(str(eqn))
						except KeyboardInterrupt:
							ans = None
				else:
					# Isn't an equation
					try:
						ans = eval(str(eqn))
					except KeyboardInterrupt:
						ans = None
			except Exception as e:
				try:
					# Exec if eval failed
					try:
						exec(str(calc))
					except KeyboardInterrupt:
						pass
					print_result = 0
				except Exception:
					# try parsing into python calls as a last resort
					try:
						shell_parser.parse_input(str(calc)).eval_arguments()

						if shell_parser.module_name() in globals():
							module_object = globals()[shell_parser.module_name()]

							if hasattr(module_object, shell_parser.method_name()):
								module_attribute = getattr(module_object, shell_parser.method_name())
								if callable(module_attribute):
									ans = module_attribute(*shell_parser.args, **shell_parser.kwargs)
								else:
									ans = module_attribute
							else:
								raise AttributeError(f"module '{shell_parser.module_name()}' has no attribute '{shell_parser.method_name()}'")
						else:
							raise ModuleNotFoundError(f"No module named '{shell_parser.module_name()}'")
					except Exception as e:
						# Couldn't execute, display error
						signal("onError", type(e).__name__)
						if print_result:
							print(theme["styles"]["error"] + type(e).__name__  + ': ' + str(e) + theme["styles"]["normal"])
							if config["dev"]["debug"] == "true":
								traceback.print_exc()
							print_result = 0

			_ = ans # refer back to the previous answer as `_`

			# Print answer
			if(print_result == 1 and ans is not None):
				signal("onPrintAnswer")

				try:
					if(ans.imag == 0):
						print(theme["styles"]["answer"] + str(ans.real))
					else:
						print(theme["styles"]["answer"] + str(ans) + theme["styles"]["normal"])
				except Exception:
					print(theme["styles"]["answer"] + str(ans) + theme["styles"]["normal"])

	# Exit cleanly on Ctrl-D
	except EOFError:
		signal("onEofExit")
		print(theme["styles"]["important"] + "\n" + i18n("EOF, exiting..."))
		print(Fore.RESET + Back.RESET + Style.NORMAL)
		exit()
	# Catch errors and display nice message
	except Exception as e:
		signal("onFatalError", type(e).__name__)
		print(theme["styles"]["error"])
		print("==============")
		print("= " + i18n("Fatal error") + "=")
		print("==============")
		print(Style.NORMAL + i18n("The calculator has encountered an error and cannot continue."))
		print(Style.BRIGHT + i18n("Error: ") + str(e) + theme["styles"]["normal"])
		print(i18n("Please start an issue on the GitLab repository at https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator/-/issues"))

		print(i18n("Traceback:"))
		traceback.print_exc()


main()
