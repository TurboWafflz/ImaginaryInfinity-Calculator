Version 3.0.0
----
Unreleased

 - Migrate repository to GitLab
 - Made a new logo. This is an svg so that it can be used basically anywhere
 - Themes are now previewed when viewing them on [https://turbowafflz.azurewebsites.net/iicalc/browse](https://turbowafflz.azurewebsites.net/iicalc/browse)
 - Backend now uses the `azure-storage-file-share` module instead of installing `smbclient` with apt
 - Rewrote some things in `main.py`, like theme loading and config loading, so that they're more understandable
 - Improve security in loading themes, in signals, and in importing plugins
 - Fixed bug where history didn't save when the calculator was restarted via the settings editor
 - Fixed updater again (#35)
 - Added the `--ensurereqs` argument to reinstall the requirements
 - Fixed portable version of the calculator
 - Gave `pm.install()` an overhaul
 - `pm.remove()` and `pm.install()` now accept `*args`
 - Fixed bug where error was thrown if the config was broken and `config.bak` didn't exist
 - Organize `.installer` folder
 - Start building a snap and a `.exe`
 - Now installable on Haiku
 - Fix bugs with installing the calculator
 - GitLab CI/CD now checks that version was bumped before deploying
 - Import everything from the `mpmath` module into the calculator
 - Add more screenshots
 - Help functions are now just markdown and are rendered in the terminal
 - Add warning color to styles
 - Add PPA for Ubuntu based distros (ppa:imaginaryinfinity/iicalc)
 - Add deprecation system
 - Lint calculator
 - Fix safe mode
 - Assign previous answer to `_`
 - Put documentation on Readthedocs
 - Fix ANSI on Windows by installing `ansicon` package on Windows
 - Set terminal titles on macOS and Windows
 - Make floating-point arithmetic fixes better
 - Removed dialogEditor
 - Disable readline module in Haiku due to it not being included with Python there
 - Ability to update on Windows through `update()` by separating the child process of the installer
 - Add shell-like parsing to user input. For example, `pm install algebra --silent` will be turned into `pm.install('algebra', silent=True)`


Version 2.4.15
----
Released June 6, 2020

 - Added package manager
 - Fixed the updater
 - Switched from `config.json` to `config.ini`
 - Added ability to be installed cross-platform
 - Added some packaged linux versions
 - Added documentation on making plugins
 - Unified functions into `core.py`
 - Added triggers that happen on certain events
 - Added settings menu
 - Many, many more features