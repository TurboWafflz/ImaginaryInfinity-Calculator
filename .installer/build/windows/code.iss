[Code]
// Declare variables
var
  DownloadPage: TDownloadWizardPage;
  ProgressPage: TOutputProgressWizardPage;
  ProgressListBox: TNewListBox;
  ErrorCode: Integer;
  PythonInstalled: Boolean;
  Result1: Boolean;
  Python: string;


//////////////////////
// Function to check if Python is installed
/////////////////////

function CheckPython() : Boolean;
var
  PythonFileName : string;
begin
  PythonFileName := FileSearch('py.exe', GetEnv('PATH'));
   Result := (PythonFileName <> '');
end;


//////////////////////
// Function to show download process of python installer
/////////////////////

function OnDownloadProgress(const Url, FileName: String; const Progress, ProgressMax: Int64): Boolean;
begin
  if Progress = ProgressMax then
    Log(Format('Successfully downloaded file to {tmp}: %s', [FileName]));
  Result := True;
end;


//////////////////////
// Create a download and progress page to show status of Python installer
// if python needs to be installed
/////////////////////

procedure InitializeWizard;
begin
  DownloadPage := CreateDownloadPage(SetupMessage(msgWizardPreparing), SetupMessage(msgPreparingDesc), @OnDownloadProgress);
  ProgressPage := CreateOutputProgressPage('I','');
end;


//////////////////////
// Prepare to install; check for python
/////////////////////

function PrepareToInstall(var NeedsRestart: Boolean): String;
begin
  Result := '';
  PythonInstalled := CheckPython(); // is py.exe in the PATH
  if not PythonInstalled then
  begin
	  // prompt to install python
    Result1 := MsgBox('This tool requires Python 3 to run, but it was not detected. Do you want to install it now ?', mbConfirmation, MB_YESNO) = IDYES;
    if Result1 then
    begin
      // download and install python 3.9.2
      DownloadPage.Clear;
      DownloadPage.Add('https://www.python.org/ftp/python/3.9.2/python-3.9.2-amd64.exe', 'python.exe', '');
      DownloadPage.Show;
      try
        try
          DownloadPage.Download;
        except
          SuppressibleMsgBox(AddPeriod(GetExceptionMessage), mbCriticalError, MB_OK, IDOK);
          // download failed, terminate
          Result := 'Error, Terminating...';
        end;
      finally
        DownloadPage.Hide;
      end;
      // launch python installer and exit if exit code is not 0
      Python:='"'+Expandconstant('{tmp}\python.exe')+'"'
      Exec('cmd.exe ','/c '+Python+' InstallAllUsers=0 PrependPath=1 Include_test=0','', SW_HIDE,ewWaituntilterminated, Errorcode);
      if not (Errorcode = 0) then begin
        Result := 'Error installing python. Exit code ' + IntToStr(Errorcode);
      end;
    end;
  end;
end;


//////////////////////
// i have no clue what these funcitons do, but the program breaks
// if you remove them
/////////////////////

function SetTimer(
  Wnd: LongWord; IDEvent, Elapse: LongWord; TimerFunc: LongWord): LongWord;
  external 'SetTimer@user32.dll stdcall';
function KillTimer(hWnd: LongWord; uIDEvent: LongWord): BOOL;
  external 'KillTimer@user32.dll stdcall';

var
  ProgressFileName: string;


//////////////////////
// convert text buffer to ANSI for printing
/////////////////////

function BufferToAnsi(const Buffer: string): AnsiString;
var
  W: Word;
  I: Integer;
begin
  SetLength(Result, Length(Buffer) * 2);
  for I := 1 to Length(Buffer) do
  begin
    W := Ord(Buffer[I]);
    Result[(I * 2)] := Chr(W shr 8); { high byte }
    Result[(I * 2) - 1] := Chr(Byte(W)); { low byte }
  end;
end;


//////////////////////
// update text in progress box
/////////////////////

procedure UpdateProgress;
var
  S: AnsiString;
  I, L, Max: Integer;
  Buffer: string;
  Stream: TFileStream;
  Lines: TStringList;
begin
  if not FileExists(ProgressFileName) then
  begin
    Log(Format('Progress file %s does not exist', [ProgressFileName]));
  end
    else
  begin
    try
      { Need shared read as the output file is locked for writting, }
      { so we cannot use LoadStringFromFile }
      Stream := TFileStream.Create(ProgressFileName, fmOpenRead or fmShareDenyNone);
      try
        L := Stream.Size;
        Max := 100*2014;
        if L > Max then
        begin
          Stream.Position := L - Max;
          L := Max;
        end;
        SetLength(Buffer, (L div 2) + (L mod 2));
        Stream.ReadBuffer(Buffer, L);
        S := BufferToAnsi(Buffer);
      finally
        Stream.Free;
      end;
    except
      Log(Format('Failed to read progress from file %s - %s', [
                 ProgressFileName, GetExceptionMessage]));
    end;
  end;

  if S <> '' then
  begin
    Log('Progress len = ' + IntToStr(Length(S)));
    Lines := TStringList.Create();
    Lines.Text := S;
    for I := 0 to Lines.Count - 1 do
    begin
      if I < ProgressListBox.Items.Count then
      begin
        ProgressListBox.Items[I] := Lines[I];
      end
        else
      begin
        ProgressListBox.Items.Add(Lines[I]);
      end
    end;
    ProgressListBox.ItemIndex := ProgressListBox.Items.Count - 1;
    ProgressListBox.Selected[ProgressListBox.ItemIndex] := False;
    Lines.Free;
  end;

  { Just to pump a Windows message queue (maybe not be needed) }
  ProgressPage.SetProgress(0, 1);
end;


//////////////////////
// wrapper procedure for UpdateProgress
/////////////////////

procedure UpdateProgressProc(
  H: LongWord; Msg: LongWord; Event: LongWord; Time: LongWord);
begin
  UpdateProgress;
end;


//////////////////////
// function ran on ssPostInstall which installs PyPI requirements
/////////////////////

procedure InstallPyPIRequirements;
var
  ResultCode: Integer;
  Timer: LongWord;
  AppPath: string;
  AppError: string;
  Command: string;
begin
  ProgressPage :=
    CreateOutputProgressPage(
      'Installing PyPI requirements', 'Please wait until this finishes...');
  ProgressPage.Show();
  ProgressListBox := TNewListBox.Create(WizardForm);
  ProgressListBox.Parent := ProgressPage.Surface;
  ProgressListBox.Top := 0;
  ProgressListBox.Left := 0;
  ProgressListBox.Width := ProgressPage.SurfaceWidth;
  ProgressListBox.Height := ProgressPage.SurfaceHeight;

  { Fake SetProgress call in UpdateProgressProc will show it, }
  { make sure that user won't see it }
  ProgressPage.ProgressBar.Top := -100;

  try
    Timer := SetTimer(0, 0, 250, CreateCallback(@UpdateProgressProc));

    AppPath := 'C:\Windows\iicalc.bat';
    ProgressFileName := ExpandConstant('{tmp}\progress.txt');
    Log(Format('Expecting progress in %s', [ProgressFileName]));
    Command := Format('""%s" --ensurereqs > "%s""', [AppPath, ProgressFileName]);
    if not Exec(ExpandConstant('{cmd}'), '/c ' + Command, '', SW_HIDE,
         ewWaitUntilTerminated, ResultCode) then
    begin
      AppError := 'Cannot start app';
    end
      else
    if ResultCode <> 0 then
    begin
      AppError := Format('App failed with code %d', [ResultCode]);
    end;
    UpdateProgress;
  finally
    { Clean up }
    KillTimer(0, Timer);
    ProgressPage.Hide;
    DeleteFile(ProgressFileName);
    ProgressPage.Free();
  end;

  if AppError <> '' then
  begin
    { RaiseException does not work properly while TOutputProgressWizardPage is shown }
    RaiseException(AppError);
  end;
end;


//////////////////////
// on step changed
/////////////////////

procedure CurStepChanged(CurStep: TSetupStep);
begin
  if (CurStep = ssPostInstall) then
  begin
    // installation is finished; install requirements
    InstallPyPIRequirements;
  end;
end;