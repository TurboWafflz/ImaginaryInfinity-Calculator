class Iicalc < Formula
  # include Language::Python::Virtualenv

  desc "Extensible calculator written in Python"
  homepage "https://turbowafflz.gitlab.io/iicalc.html"
  url "https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator/-/archive/development/ImaginaryInfinity-Calculator-development.tar.gz"
  version "2.10.3"
  sha256 "17ca333013c83762d84e2fd4b22d66a37a1ffecaebcc4796daa9ac97a88023b2"
  license "GPL-3.0-only"
  head "https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator.git"

  bottle :unneeded

  depends_on "dialog"
  depends_on "python@3.9"

  {{resources}}

  def install
    {{installpypi}}
	system "cp", ".installer/build/macos/brewinstall.sh", "./"
    system "bash", "brewinstall.sh"
  end

  test do
    system "iicalc", "-V"
  end
end