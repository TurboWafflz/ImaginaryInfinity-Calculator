class Iicalc < Formula
  # include Language::Python::Virtualenv

  desc "Extensible calculator written in Python"
  homepage "https://turbowafflz.gitlab.io/iicalc.html"
  url "https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator/-/archive/development/ImaginaryInfinity-Calculator-development.tar.gz"
  version "2.10.3"
  sha256 "17ca333013c83762d84e2fd4b22d66a37a1ffecaebcc4796daa9ac97a88023b2"
  license "GPL-3.0-only"
  head "https://gitlab.com/TurboWafflz/ImaginaryInfinity-Calculator.git"

  bottle :unneeded

  depends_on "dialog"
  depends_on "python@3.9"


  resource "certifi" do
    url "https://files.pythonhosted.org/packages/06/a9/cd1fd8ee13f73a4d4f491ee219deeeae20afefa914dfb4c130cfc9dc397a/certifi-2020.12.5.tar.gz"
    sha256 "1a4995114262bffbc2413b159f2a1a480c969de6e6eb13ee966d470af86af59c"
  end

  resource "chardet" do
    url "https://files.pythonhosted.org/packages/ee/2d/9cdc2b527e127b4c9db64b86647d567985940ac3698eeabc7ffaccb4ea61/chardet-4.0.0.tar.gz"
    sha256 "0d6f53a15db4120f2b08c94f11e7d93d2c911ee118b6b30a04ec3ee8310179fa"
  end

  resource "colorama" do
    url "https://files.pythonhosted.org/packages/1f/bb/5d3246097ab77fa083a61bd8d3d527b7ae063c7d8e8671b1cf8c4ec10cbe/colorama-0.4.4.tar.gz"
    sha256 "5941b2b48a20143d2267e95b1c2a7603ce057ee39fd88e7329b0c292aa16869b"
  end

  resource "commonmark" do
    url "https://files.pythonhosted.org/packages/60/48/a60f593447e8f0894ebb7f6e6c1f25dafc5e89c5879fdc9360ae93ff83f0/commonmark-0.9.1.tar.gz"
    sha256 "452f9dc859be7f06631ddcb328b6919c67984aca654e5fefb3914d54691aed60"
  end

  resource "fuzzywuzzy" do
    url "https://files.pythonhosted.org/packages/11/4b/0a002eea91be6048a2b5d53c5f1b4dafd57ba2e36eea961d05086d7c28ce/fuzzywuzzy-0.18.0.tar.gz"
    sha256 "45016e92264780e58972dca1b3d939ac864b78437422beecebb3095f8efd00e8"
  end

  resource "idna" do
    url "https://files.pythonhosted.org/packages/ea/b7/e0e3c1c467636186c39925827be42f16fee389dc404ac29e930e9136be70/idna-2.10.tar.gz"
    sha256 "b307872f855b18632ce0c21c5e45be78c0ea7ae4c15c828c20788b26921eb3f6"
  end

  resource "mpmath" do
    url "https://files.pythonhosted.org/packages/95/ba/7384cb4db4ed474d4582944053549e02ec25da630810e4a23454bc9fa617/mpmath-1.2.1.tar.gz"
    sha256 "79ffb45cf9f4b101a807595bcb3e72e0396202e0b1d25d689134b48c4216a81a"
  end

  resource "packaging" do
    url "https://files.pythonhosted.org/packages/86/3c/bcd09ec5df7123abcf695009221a52f90438d877a2f1499453c6938f5728/packaging-20.9.tar.gz"
    sha256 "5b327ac1320dc863dca72f4514ecc086f31186744b84a230374cc1fd776feae5"
  end

  resource "Pygments" do
    url "https://files.pythonhosted.org/packages/15/9d/bc9047ca1eee944cc245f3649feea6eecde3f38011ee9b8a6a64fb7088cd/Pygments-2.8.1.tar.gz"
    sha256 "2656e1a6edcdabf4275f9a3640db59fd5de107d88e8663c5d4e9a0fa62f77f94"
  end

  resource "pyparsing" do
    url "https://files.pythonhosted.org/packages/c1/47/dfc9c342c9842bbe0036c7f763d2d6686bcf5eb1808ba3e170afdb282210/pyparsing-2.4.7.tar.gz"
    sha256 "c203ec8783bf771a155b207279b9bccb8dea02d8f0c9e5f8ead507bc3246ecc1"
  end

  resource "pythondialog" do
    url "https://files.pythonhosted.org/packages/72/3c/26ed0db035f97196704d0197d8b2254b8a6ca93a2d132430b0b0d597aa79/pythondialog-3.5.1.tar.gz"
    sha256 "34a0687290571f37d7d297514cc36bd4cd044a3a4355271549f91490d3e7ece8"
  end

  resource "requests" do
    url "https://files.pythonhosted.org/packages/6b/47/c14abc08432ab22dc18b9892252efaf005ab44066de871e72a38d6af464b/requests-2.25.1.tar.gz"
    sha256 "27973dd4a904a4f13b263a19c866c13b92a39ed1c964655f025f3f8d3d75b804"
  end

  resource "rich" do
    url "https://files.pythonhosted.org/packages/12/3c/e4e2b356057f3ce557fcda8a2b9bf114b06f71ade88dac8a0883ae800e28/rich-10.1.0.tar.gz"
    sha256 "8f05431091601888c50341697cfc421dc398ce37b12bca0237388ef9c7e2c9e9"
  end

  resource "typing-extensions" do
    url "https://files.pythonhosted.org/packages/16/06/0f7367eafb692f73158e5c5cbca1aec798cdf78be5167f6415dd4205fa32/typing_extensions-3.7.4.3.tar.gz"
    sha256 "99d4073b617d30288f569d3f13d2bd7548c3a7e4c8de87db09a9d29bb3a4a60c"
  end

  resource "urllib3" do
    url "https://files.pythonhosted.org/packages/cb/cf/871177f1fc795c6c10787bc0e1f27bb6cf7b81dbde399fd35860472cecbc/urllib3-1.26.4.tar.gz"
    sha256 "e7b021f7241115872f92f43c6508082facffbd1c048e3c6e2bb9c2a157e28937"
  end

  def install

    resource("certifi").stage { system "python3", *Language::Python.setup_install_args(libexec/"vendor") }
    resource("chardet").stage { system "python3", *Language::Python.setup_install_args(libexec/"vendor") }
    resource("colorama").stage { system "python3", *Language::Python.setup_install_args(libexec/"vendor") }
    resource("commonmark").stage { system "python3", *Language::Python.setup_install_args(libexec/"vendor") }
    resource("fuzzywuzzy").stage { system "python3", *Language::Python.setup_install_args(libexec/"vendor") }
    resource("idna").stage { system "python3", *Language::Python.setup_install_args(libexec/"vendor") }
    resource("mpmath").stage { system "python3", *Language::Python.setup_install_args(libexec/"vendor") }
    resource("packaging").stage { system "python3", *Language::Python.setup_install_args(libexec/"vendor") }
    resource("Pygments").stage { system "python3", *Language::Python.setup_install_args(libexec/"vendor") }
    resource("pyparsing").stage { system "python3", *Language::Python.setup_install_args(libexec/"vendor") }
    resource("pythondialog").stage { system "python3", *Language::Python.setup_install_args(libexec/"vendor") }
    resource("requests").stage { system "python3", *Language::Python.setup_install_args(libexec/"vendor") }
    resource("rich").stage { system "python3", *Language::Python.setup_install_args(libexec/"vendor") }
    resource("typing-extensions").stage { system "python3", *Language::Python.setup_install_args(libexec/"vendor") }
    resource("urllib3").stage { system "python3", *Language::Python.setup_install_args(libexec/"vendor") }

	system "cp", ".installer/build/macos/brewinstall.sh", "./"
    system "bash", "brewinstall.sh"
  end

  test do
    system "iicalc", "-V"
  end
end