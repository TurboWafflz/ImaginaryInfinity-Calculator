#!/usr/bin/env bash
DIR=$(dirname "$0")

systemPath="$DESTDIR/usr/local/share/iicalc/"
binPath="$DESTDIR/usr/local/bin/"
config=".installer/configDefaults/macos.ini"
launcher=".installer/launchers/macos.sh"
iconPath="$DESTDIR/usr/local/share/iicalc/"
desktopFilePath="$DESTDIR/Applications/ImaginaryInfinity Calculator.app"
desktopFile=".installer/desktopFiles/iicalc.app"

mkdir "$DESTDIR"/usr/local/share

cp -v $launcher "$binPath/iicalc"

iconSource="$DIR/iicalc.png"
iconDestination="$DIR/$desktopFile"
icon=/tmp/$(basename "$iconSource")
rsrc=/tmp/icon.rsrc
# Create icon from the iconSource
cp "$iconSource" "$icon"

# Add icon to image file, meaning use itself as the icon
sips -i "$icon"

# Take that icon and put it into a rsrc file
DeRez -only icns "$icon" > "$rsrc"

# Apply the rsrc file to
SetFile -a C "$iconDestination"

touch "$iconDestination"/$'Icon\r'
Rez -append "$rsrc" -o "$iconDestination"/Icon?
SetFile -a V "$iconDestination"/Icon?

rm "$rsrc" "$icon"

cp -r "$desktopFile" "$desktopFilePath"

#Copy files
mkdir -p "$binPath"
chmod +x "$binPath/iicalc"
echo "Installing builtin plugins..."
mkdir -p "$systemPath"
mkdir -p "$systemPath/systemPlugins"
cp -r system/systemPlugins/* "$systemPath/systemPlugins"
mkdir -p "$systemPath/themes"
cp -r system/themes/* "$systemPath/themes"
cp -r "templates" "$systemPath"
mkdir -p "$systemPath/docs"
cp -r system/docs/* "$systemPath/docs"
cp README.md "$systemPath/docs/iicalc.md"
echo "Installing main Python script..."
cp main.py "$systemPath/iicalc.py"
cp requirements.txt "$systemPath"
cp messages.txt "$systemPath"
cp system/version.txt "$systemPath"
cp README.md "$systemPath"
cp $config "$systemPath/config.ini"